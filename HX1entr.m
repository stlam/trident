function F1a = HX1entr(var1a, Dp, DX, k_masspipe_T2, k_HenryT2_mod, k_sievert, Moles_firstelement, HX1_element_sa1a, DT, Hx_mesh, CoeffA, mt1_T2_HXin, v_dot)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This is an optional function to use if necessary when T2 diffusivity in
%the salt is greater than the T2 diffusivity in the metal.  This function
%is not required if flibe is the salt because the T2 diffusivity in flibe
%is lower than that in the metal.  This sets up a system equations which is
%solved with "fsolve" in matlab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

F1a = [k_masspipe_T2*(var1a(4) - var1a(3)) - (2*Dp/DX)*(var1a(2) - var1a(1));  %var1a(1) = Avg1, var1a(2) = Cm1, var1a(3) = Csw, var1a(4) = Csb
      var1a(3) - k_HenryT2_mod*(var1a(2)/k_sievert)^2;  
      var1a(1) - (Moles_firstelement + k_masspipe_T2*(var1a(4) - var1a(3))*(HX1_element_sa1a/Hx_mesh)*DT)/CoeffA;  %CoeffA is the volume of the first element
      var1a(4) - (mt1_T2_HXin - (2*Dp/DX)*(var1a(2) - var1a(1))*(HX1_element_sa1a/Hx_mesh)*DT)/(v_dot*DT)]; 
