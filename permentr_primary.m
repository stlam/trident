function FP = permentr_primary(var1p, Dpermp, PPX, k_masspipe_T2_perm1, k_HenryT2mod, k_perm_sievert, Moles_firstpelement, Perm1_element_sa, DT, Permeator_number_p, Permp_element_volume, mt1_T2_Perm_in, v_dot)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function sets up a system of non-linear equations for tritum 
% transport from the salt into the metal at the upstream side of the 
% primary permeator which can be solved by the MATLAB function 'fsolve'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


FP = [k_masspipe_T2_perm1*(var1p(4) - var1p(3)) - (2*Dpermp/PPX)*(var1p(2) - var1p(1));  %var1a(1) = Avg1, var1a(2) = Cm1, var1a(3) = Csw, var1a(4) = Csb
       var1p(3) - k_HenryT2mod*(var1p(2)/k_perm_sievert)^2;
       var1p(1) - (Moles_firstpelement + k_masspipe_T2_perm1*(var1p(4) - var1p(3))*(Perm1_element_sa)*(DT/Permeator_number_p))/Permp_element_volume;  %CoeffA is the volume of the first element
       var1p(4) - (mt1_T2_Perm_in - (2*Dpermp/PPX)*(var1p(2) - var1p(1))*(Perm1_element_sa)*(DT/Permeator_number_p))/(v_dot*(DT/Permeator_number_p))];
