function [mt1_TF_HXout, mt1_T2_HXout, Cr_dissolved, C_Cr_coolantHX, TF_consumed, Cr_coolant, T2_generated, Cr_profile, mt1_TF, mt1_T2, ...
          Conc_slice, grad, Crperslice, initial, Vol_surf, Totmatomsurf, surface, Crcorrodedaxial] = CorrosionModuleHX(j, initial, surface, Conc_TF, k_masspipe_TF, Vol_surf, ...
          pipe_sa, pipe_d, pipe_l, pipe_d2, pipe_l2, Hx_tube_od, VolHX, Vol_1, Thick, DT, Slice_vol, Crperslice, ...
          Cr_dissolvedHX, TF_consumed, Cr_profile, slices1, slices2, slice_thick1, slice_thick2, Conc_slice, ...
          Conc_bulk, Totmatomsurf, grad, Lattice_param, mt1_TF, mt1_T2, T, Core_mesh, Hot_mesh, Hx_mesh, Cold_mesh, ...
          Length1_tot, GBflag, Crcorrodedaxial, Cr_dissolved,  mt1_T2_HXout, mt1_TF_HXin, t, Surf_area_gb)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model for corrosion of Cr from base metal in heat exchanger.  Accounts 
% for early corrosion limited by TF transport/concentration.  Accounts for 
% long-term corrosion limited by Cr diffusion rate in the base metal.  The 
% corrosion module for the hot and cold legs is in 'CorrosionModule.m'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Set Physical Constants:
R_univ = 8.31446;       %Universal gas constant [J/mol-K]

D_Cr = (2.84E-5)*exp(-90.6/(R_univ*T/1000));  %[m2/s] Cr Grain Boundary Diffusion coefficient in 316L from Mizouchi, 2004 eqn 4

D_Cr_bulk = (3E-32)*exp(0.0283*T);  %[m2/s] Cr Bulk Diffusion coefficient in 316L from fit to data in Mizouchi 2004 Figure 2
%     D_Cr_bulk = 0.27*exp(-246/(0.008314*T))*(1/10000);  %[m2/s] Cr vol diffusion in 310 stainless, Williams, 1987
%     D_Cr_bulk = (1/10000)*(3.06*exp(-67536/(1.987*T)));  %[m2/s] bulk Cr diffusion in 304 from Daruvala, 1979
%     D_Cr_bulk = (1.745E-30)*exp((2.863E-2)*T); %Cr "overall diffusion coefficient" for Hastelloy N from Evans, 1961 ORNL-2982


%i=0;  %counter for J_Cr_history
%"surface" Selects the surface point. If the initial surface point becomes depleted in Cr, then surface will be set to 2 or higher in the calcs below
%in order to reflect the fact that the surface for Cr has moved.

%"initial" = 1 for the initial period of time where corrosion is controlled by the flux of TF to the surface.
%Then, initial is set to 2 once the TF-limited period has
%ended and the Cr diffusion-limited corrosion has begun


if initial(j,1) == 1  %Assume mass transfer limited due to arrival of TF at surface up until initial layer of surface atoms has been depleted.  Check C_Cr_w_t at beginning of timestep
    J_TF = k_masspipe_TF*Conc_TF;  %flux of TF to the surface assuming concentration of TF is zero at the pipe wall due to instant reaction with Cr [mole/m2-s]
%     ratio = ((Totmatomsurf(j,1)-Cr_dissolvedHX)/(Totmatomsurf(j,1)-Cr_dissolvedHX)); %Ratio of surface Cr atoms to other atoms on surface
%     J_TF = J_TF*ratio;  %[mole TF/m2-s] Adjust flux so that it is the effective flux to Cr atoms on the surface  ****Re-think if this is desirable***
    Totmatomsurf_old = Totmatomsurf(j,1);
    Totmatomsurf(j,1) = Totmatomsurf(j,1) - 0.5*J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;  %[moles Cr] at surface after corrosion with TF
    Crperslice_old = Crperslice(j,1);   %[moles Cr] in outermost slice
    Crperslice(j,1) = Crperslice(j,1) - (0.5*J_TF*DT*pipe_sa(j,1));%*Surf_area_gb);  % [mole Cr in slice 1] moles of Cr surface atoms in first slice
    Crcorrodedaxial_old = Crcorrodedaxial(j,1);
    Crcorrodedaxial(j,1) = Crcorrodedaxial(j,1) + (0.5*J_TF*DT*pipe_sa(j,1));%*Surf_area_gb);  %[mole Cr] total moles Cr corroded from each axial segment
    Cr_dissolvedHX_old = Cr_dissolvedHX;  %Record the old moles of Cr dissolved from the previous step.  This is used under certain criteria below. 
    Cr_dissolved_old = Cr_dissolved;
    Cr_dissolved = Cr_dissolved + 0.5*J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;  %[moles] moles of Cr dissolved by TF in TF-limited corrosion
    Cr_coolant = Cr_dissolved/Vol_1; %[mole/m3] concentration of Cr in the primary coolant
    Cr_dissolvedHX = Cr_dissolvedHX + 0.5*J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;  %[moles] moles of Cr dissolved by TF in TF-limited corrosion
    C_Cr_coolantHX = Cr_dissolvedHX/VolHX; %[mole/m3] concentration of Cr in the HX volume element Vol = v_dot*DT
    TF_consumed = TF_consumed + J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;  %[mole]moles of TF consumed in reaction Cr + 2TF = CrF2 + T2
    T2_generated = 0.5*TF_consumed;  %[mole] moles of T2 generated via consumption of TF in reaction with Cr
    mt1_T2_old = mt1_T2;  %Record the old moles of T2 dissolved from the previous step.  This is used under certain criteria below. 
    mt1_T2_HXout_old = mt1_T2_HXout;  %Record the old moles of T2 dissolved in the HX coolant element from the previous step.  This is used under certain criteria below.
    mt1_TF_old = mt1_TF;  %Record the old moles of TF dissolved from the previous step.  This is used under certain criteria below. 
    mt1_TF_HXin_old = mt1_TF_HXin;  %Record the old moles of TF dissolved in the HX coolant element from the previous step.  This is used under certain criteria below. 
    mt1_T2 = mt1_T2 + 0.5*J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;  %Moles of T2 in coolant after some T2 generated via corrosion of Cr to CrF2.
    mt1_T2_HXout = mt1_T2_HXout + 0.5*J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;
    mt1_TF = mt1_TF - J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;      %Moles of TF in coolant after some TF consumed via corrosion of Cr to CrF2. 
    mt1_TF_HXout = mt1_TF_HXin - J_TF*DT*pipe_sa(j,1);%*Surf_area_gb;
    if Totmatomsurf(j,1)<=0  %Check amount of Cr corroded after timestep.  If all surface atoms have been corroded, assume mass transfer limited corrosion has ended.  Then calc distribution to use in Cr-diffusion limited regime.
        Totmatomsurf(j,1) = 0;  %[mole Cr] That surface has been depleted
%         Crperslice(j,1) = Crperslice_old - Totmatomsurf_old;  %The surface atoms have been depleted, update the moles of Cr in the slice.
        Crcorrodedaxial(j,1) = Crcorrodedaxial_old + Totmatomsurf_old;  %[mole Cr] mole Cr corroded from each axial segment
        Cr_dissolvedHX = Cr_dissolvedHX_old + Totmatomsurf_old;  %Set to moles of Cr initially on surface.  This indicates that all surface Cr atoms have been dissolved.
        Cr_dissolved = Cr_dissolved_old + Totmatomsurf_old;  
        mt1_T2 = mt1_T2_old + Totmatomsurf_old;  %Update amount of T2 in solution. One T2 is made for each Cr dissolved.
        mt1_TF = mt1_TF_old - 2*Totmatomsurf_old;  %Update amount of TF in solution.  Two TF are used up for each Cr dissolved.
        mt1_T2_HXout = mt1_T2_HXout_old + Totmatomsurf_old;
        mt1_TF_HXout = mt1_TF_HXin_old - 2*Totmatomsurf_old;
        C_Cr_coolantHX = Cr_dissolvedHX/VolHX;  %[moles/m3] concentration of Cr in HX coolant element
        Cr_coolant = Cr_dissolved/Vol_1;
        TF_consumed = TF_consumed + 2*Totmatomsurf_old;  %set to 2*moles of surface Cr
        T2_generated = 0.5*TF_consumed;  %[mole] moles of T2 generated via consumption of TF in reaction with Cr
        initial(j,1) = 2;  %Marks the end of TF transport-controlled corrosion and the beginning of Cr diffusion-limited corrosion
        %C_Cr_w_t = 0;  %Set surface moles of Cr [mol Cr] equal to 0, which is the same as saying 0 [mol/m3] at the surface. Don't do this it makes things strange
%         for n = 0:1:slices1
%             slice = n*slice_thick1;    %Distance into metal wall from the surface [m]
%             C_Cr_x = (Crperslice(j,1)/Slice_vol(j,1) - Conc_bulk(j,1))*(1-erf(slice/(2*sqrt(D_Cr*DT))))+Conc_bulk(j,1);    %Calculate profile of Cr in 316 pipe wall
%             Cr_profile (j,n+1) = C_Cr_x;   %[mole Cr/m3] Array of Cr profile in 316, then pick out the value that is 99% of maximum and move on to next step
%         end
%         max1 = max(Cr_profile(j,:));  %[mole Cr/m3] finds the maximum concentration of the Cr profile
%         for n = 1:1:slices1+1
%             if (Cr_profile(j,n)-Cr_profile(j,1))/(max1-Cr_profile(j,1)) >= 0.99  %Pick the point that is 99% of the maximum
%                 Cr_99 = Cr_profile(j,n);
%                 dx = n*slice_thick1 - slice_thick1;   %distance from Cr min to 99% of Cr max1 [m]
%                 grad(j,1) = (Cr_99 - Cr_profile(j,1))/dx; %calculate gradient from the point of 99 % Cr to the surface
%                 break
%             else
%             end
%         end
        Cr_99 = 0.99*Conc_bulk(j,1);  %Cr concentration that is 99% of the bulk concentration
        dx = 2*sqrt(D_Cr*t)*erfinv(0.99);   %distance from Cr min to 99% of Cr max1 [m]
        grad(j,1) = (Cr_99 - 0)/dx; %calculate gradient from the point of 99 % Cr to the surface assuming surface is at 0
    else
    end
else  %Now corosion is limited by Cr diffusion in the metal once the surface atoms have been removed in the TF limited regime.
    if j >= 1 && j <= Core_mesh  %In reactor core.  These values get updated as the surface for Cr changes.  The initial values for surface = 1 are set in TRIDENT main program
        Vol_surf(j,1) = 0;       %Currently not accounting for any corrosion in reactor core
        pipe_sa(j,1)=0;
    elseif j >= Core_mesh +1 && j <= Core_mesh + Hot_mesh  %In hot leg
        Vol_surf(j,1) = pi*(pipe_l/Hot_mesh)*(((pipe_d/2)+(surface(j,1)-1)*slice_thick2+Lattice_param)^2 - ((pipe_d/2)+(surface(j,1)-1)*slice_thick2)^2);  %Vol_surf is updated as the surface moves.
        pipe_sa(j,1)=pi*2*((pipe_d/2)+(surface(j,1)-1)*slice_thick2)*pipe_l/Hot_mesh;  %[m2] Update pipe surface area as surface changes
    elseif j >= Core_mesh + Hot_mesh +1 && j <= Core_mesh + Hot_mesh + Hx_mesh  %%% PRIMARY HEAT EXCHANGER %%%
        Hx_tube_ir = (Hx_tube_od - 2*Thick)/2;  %Hx tube inner radius [m]
        Vol_surf(j,1) = pi*(Length1_tot/Hx_mesh)*((Hx_tube_ir+(surface(j,1)-1)*slice_thick2+Lattice_param)^2 - (Hx_tube_ir+(surface(j,1)-1)*slice_thick2)^2);
        pipe_sa(j,1)=pi*2*((Hx_tube_ir)+(surface(j,1)-1)*slice_thick2)*Length1_tot/Hx_mesh;  %[m2] Update pipe surface area as surface changes
    elseif   j >= Core_mesh + Hot_mesh + Hx_mesh +1 && j<= Core_mesh + Hot_mesh + Hx_mesh + Cold_mesh   %Cold Leg
        Vol_surf(j,1) = pi*(pipe_l2/Cold_mesh)*(((pipe_d2/2)+(surface(j,1)-1)*slice_thick2+Lattice_param)^2 - ((pipe_d2/2)+(surface(j,1)-1)*slice_thick2)^2);
        pipe_sa(j,1)=pi*2*((pipe_d2/2)+(surface(j,1)-1)*slice_thick2)*pipe_l2/Cold_mesh;  %[m2] Update pipe surface area as surface changes
    end
    
    %The ratio could be used here.  Should this be used in regime 1 since 1 is supposed to have the higher corrosion rate?
    Conc_slice(j,1) = Crperslice(j,surface(j,1))/Slice_vol(j,surface(j,1));  %[mol/m3] adjust the concentration of Cr in the slice.  Conc_slice is initally set in main program.
    %ratio = ((Crperslice(j,surface(j,1))/Slice_vol(j,surface(j,1)))*Vol_surf(j,surface(j,1)))/(sum(Conc_slice(j,:))*Vol_surf(j,surface(j,1)));  %[moles/moles] ratio of surface Cr atoms to total surface atoms
    J_TF = k_masspipe_TF*Conc_TF;  %[mole TF/m2-sec] flux of TF to pipe wall
    J_Cr = D_Cr*grad(j,1);  %Cr diffusion-controlled flux [mol Cr/m^2-s]
    if J_Cr > 0.5*J_TF; %*ratio; %This is used every time (2/6/2014)  That can't be right because it means that the corrosion is being limited by TF for a much longer time than was thought previously
        %i=i+1;             %counter
        %J_Cr_history(i,1)=J_Cr; %history of J_Cr to see where, if ever, it crosses to below J_TF
        J_Cr = 0.5*J_TF;  %*ratio;   %The flux of Cr from the surface cannot exceed 0.5*J_TF
    else
    end
 
    mt1_T2_old = mt1_T2;  %Record the old moles of T2 dissolved from the previous step.  This is used under certain criteria below. 
    mt1_T2_HXout_old = mt1_T2_HXout;  %Record the old moles of T2 dissolved in the HX coolant element from the previous step.  This is used under certain criteria below.
    mt1_TF_old = mt1_TF;  %Record the old moles of TF dissolved from the previous step.  This is used under certain criteria below. 
    mt1_TF_HXin_old = mt1_TF_HXin;  %Record the old moles of TF dissolved in the HX coolant element from the previous step.  This is used under certain criteria below. 

    Crpersliceold = Crperslice(j,surface(j,1));  %[mole Cr in current slice] prior to dissolution in the current time step.  Used in the following if-statement.
    Crperslice(j,surface(j,1)) = Crperslice(j,surface(j,1)) - J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;  %[mole Cr in slice = #surface] Update Cr concentration at the surface [mol/m3] from current time step based on diffusion to coolant.  This must not be greater than the flux of TF to the surface!!!!
    Crcorrodedaxial_old = Crcorrodedaxial(j,1);
    Crcorrodedaxial(j,1) = Crcorrodedaxial(j,1) + J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;  %[mole Cr] mole Cr corroded from each axial segment  
    mt1_T2 = mt1_T2 + J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;   %Moles of T2 in coolant after some T2 generated via corrosion of Cr to CrF2 + T2.
    mt1_TF = mt1_TF - 2*J_Cr*DT*pipe_sa(j,1)*Surf_area_gb; %Moles of TF in coolant after some TF consumed via corrosion of Cr to CrF2. 
    mt1_T2_HXout = mt1_T2_HXout + J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;
    mt1_TF_HXout = mt1_TF_HXin - 2*J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;
    
    %No longer tracking surface recession
%     if Crperslice(j,surface(j,1)) <= 1E-20 %Criteria for allowing surface depletion of Cr and the movement of the surface.  Need to make sure this criterium is sufficient.
%         Crperslice(j,surface(j,1)) = 0;   %[moles Cr in current slice] Set moles in the slice to zero
%         Cr_profile(j,surface(j,1)) = 0;   %[moles Cr/m3] Set current surface concentration = 0
%         surface(j,1) = surface(j,1) + 1;         %Define new surface.  Change surface indicator to next slice
%         Crcorrodedaxial(j,1) = Crcorrodedaxial_old + Crpersliceold;  %Moles of Cr corrded from the surface at the current axial position          
%         Cr_dissolvedHX = Cr_dissolvedHX + Crpersliceold;   %[mole Cr] amount of Cr dissolved is equal to that which was already dissolved plus the remaining Cr in the slice.
%         C_Cr_coolantHX = Cr_dissolvedHX/VolHX; %[moles/m3] concentration of Cr dissolved in the primary coolant
%         Cr_dissolved = Cr_dissolved + Crpersliceold;
%         Cr_coolant = Cr_dissolved/Vol_1; %[moles/m3] concentration of Cr dissolved in the primary coolant
%         TF_consumed = TF_consumed + Crpersliceold*2;  %Moles of TF consumed via reaction with Cr during Cr diffusion-limited regime [moles]
%         T2_generated = 0.5*TF_consumed;   %[moles T2] generated via 2TF + Cr = CrF2 + T2
%         mt1_T2 = mt1_T2_old + Crpersliceold;  %[moles T2 in primary coolant] after Cr + 2TF = CrF2 + T2
%         mt1_T2_HXout = mt1_T2_HXout_old + Crpersliceold;
%         mt1_TF = mt1_TF_old - 2*Crpersliceold; %[moles TF in primary coolant] after Cr + 2TF = CrF2 + T2
%         mt1_TF_HXout = mt1_TF_HXin_old - 2*Crpersliceold; 
%         for n = surface(j,1)-1:1:slices2   %Calculate profile
%             slice = n*slice_thick2;   %Distance into metal wall from the surface
%             C_Cr_x2 = (Cr_profile(j,surface(j,1)-1) - Conc_bulk(j,1))*(1-erf(slice/(2*sqrt(D_Cr*DT))))+Conc_bulk(j,1);    %Give some profile of Cr in 316 pipe wall
%             Cr_profile (j,n+1) = C_Cr_x2;   %Array of Cr concentration pofile in 316 under Cr diffusion limited corrosion
%         end
%     else  %The surface has not been depleted yet, calculate the new profile as usual.
%         Cr_profile(j,surface(j,1)) = Crperslice(j,surface(j,1))/Slice_vol(j,surface(j,1));  %[mol/m3] Update surface concentration in the array
        Cr_dissolved = Cr_dissolved + J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;  %moles of Cr dissolved in coolant
        Cr_coolant = Cr_dissolved/Vol_1;  %[moles/m3] concentration of Cr in primary coolant
        Cr_dissolvedHX = Cr_dissolvedHX + J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;  %moles of Cr dissolved in coolant
        C_Cr_coolantHX = Cr_dissolvedHX/VolHX;  %[moles/m3] concentration of Cr in primary coolant
        TF_consumed = TF_consumed + 2*J_Cr*DT*pipe_sa(j,1)*Surf_area_gb;  %Moles of TF consumed via reaction with Cr during Cr diffusion-limited regime [moles]
        T2_generated = 0.5*TF_consumed;   %[moles T2] generated via 2TF + Cr = CrF2 + T2
%         for n = 0:1:slices2-surface(j,1)
%             slice = (n+1)*slice_thick2;    %Distance into metal wall from the surface
%             C_Cr_x2 = (Cr_profile(j,surface(j,1)) - Conc_bulk(j,1))*(1-erf(slice/(2*sqrt(D_Cr*DT))))+Conc_bulk(j,1);    %Give some profile of Cr in 316 pipe wall
%             Cr_profile (j,n+surface(j,1)+1) = C_Cr_x2;   %Array of Cr pofile in 316 under Cr diffusion limited corrosion
%         end
%     end
    %Calc 99% to get new gradient for use in next time step
%     max2 = max(Cr_profile(j,:));
%     min2 = Cr_profile(j,surface(j,1));
%     for n = 1:1:slices2+1
%         if (Cr_profile(j,n)-min2)/(max2-min2) >= 0.99  %pick the point that is 99% of the maximum
%             Cr_99 = Cr_profile(j,n);
%             dx = n*slice_thick2-surface(j,1)*slice_thick2;     %thickness from Cr min to 99% of Cr max2
%             grad(j,1) = (Cr_99 - Cr_profile(j,surface(j,1)))/dx;   %calculate slope from the point of 99 % Cr to the surface
%             break
%         else
%         end
%     end

%Cr corroded from bulk diffusion
        Cr_99_bulk = 0.99*Conc_bulk(j,1);
        dx_bulk = 2*sqrt(D_Cr_bulk*t)*erfinv(0.99);   %distance from Cr min to 99% of Cr max1 [m]
        grad_bulk = (Cr_99_bulk - 0)/dx_bulk;
        J_Cr_bulk = D_Cr_bulk*grad_bulk;  %Cr diffusion-controlled flux [mol Cr/m^2-s]
        Cr_dissolved = Cr_dissolved + J_Cr_bulk*DT*pipe_sa(j,1);  %moles of Cr dissolved in coolant
        Cr_coolant = Cr_dissolved/Vol_1;  %[moles/m3] concentration of Cr in primary coolant
        Cr_dissolvedHX = Cr_dissolvedHX + J_Cr_bulk*DT*pipe_sa(j,1);  %moles of Cr dissolved in coolant
        C_Cr_coolantHX = Cr_dissolvedHX/VolHX;  %[moles/m3] concentration of Cr in primary coolant
        TF_consumed = TF_consumed + 2*J_Cr_bulk*DT*pipe_sa(j,1);  %Moles of TF consumed via reaction with Cr during Cr diffusion-limited regime [moles]
        T2_generated = 0.5*TF_consumed;   %[moles T2] generated via 2TF + Cr = CrF2 + T2

        Crperslice(j,surface(j,1)) = Crperslice(j,surface(j,1)) - J_Cr_bulk*DT*pipe_sa(j,1);  %[mole Cr in slice = #surface] Update Cr concentration at the surface [mol/m3] from current time step based on diffusion to coolant.  This must not be greater than the flux of TF to the surface!!!!
        Crcorrodedaxial(j,1) = Crcorrodedaxial(j,1) + J_Cr_bulk*DT*pipe_sa(j,1);  %[mole Cr] mole Cr corroded from each axial segment
        mt1_T2 = mt1_T2 + J_Cr_bulk*DT*pipe_sa(j,1);  %Moles of T2 in coolant after some T2 generated via corrosion of Cr to CrF2 + T2
        mt1_TF = mt1_TF - 2*J_Cr_bulk*DT*pipe_sa(j,1);    %Moles of TF in coolant after some TF consumed via corrosion of Cr to CrF2
        
        mt1_T2_HXout = mt1_T2_HXout + J_Cr_bulk*DT*pipe_sa(j,1);
        mt1_TF_HXout = mt1_TF_HXin - 2*J_Cr_bulk*DT*pipe_sa(j,1);
end
end

