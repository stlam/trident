function F2 = HX2entr(var2, Ds, DX, k_transp2, k_henryT22, k_sievert, Moles_finalelement, HX2_element_sa1, DT, CoeffA2, mt2_involel, v_dot2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This is a function to used to set up a system equations which is
%solved with "fsolve" in matlab.  This is used for the upstream side of the
%secondary heat exchanger.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


F2 = [k_transp2*(var2(4) - var2(3)) - (2*Ds/DX)*(var2(2) - var2(1));  %var1(1) = Avg2, var1(2) = Csm2, var1(3) = Csw2, var1(4) = Csb2
      var2(3) - k_henryT22*(var2(2)/k_sievert)^2;  
      var2(1) - (Moles_finalelement + k_transp2*(var2(4) - var2(3))*(HX2_element_sa1)*DT)/CoeffA2;
      var2(4) - (mt2_involel - (2*Ds/DX)*(var2(2) - var2(1))*(HX2_element_sa1)*DT)/(v_dot2*DT)]; 

     
      
   
   