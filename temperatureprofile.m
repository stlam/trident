function [T_core, T_hot, T_Hx, T_cold, Temp_Profile] = temperatureprofile(T_in, qo, m_dot, cp_flibe, Core_height, Core_mesh, T_out, Hx_height1, Hot_mesh, Cold_mesh, Hx_mesh)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Builds the temperature profile throughout the loop based on user
% specifications from the input file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dx_core = Core_height/Core_mesh;
dx_Hx1 = Hx_height1/Hx_mesh;
    for z = 1:1:4  %z is the index used to delineate the four zones: core, hot leg, Hx, and cold leg.  TEMPERATURE UNITS ARE KELVIN
        if z == 1  %Build temperature profile through the core
            for j = 1:1:Core_mesh+1
                T_core_temp(j,1) = T_in+((qo*10^6)/(m_dot*cp_flibe))*(Core_height/pi)*(sin(pi*((-Core_height/2)+(j-1)*dx_core)/Core_height)+1);  %Temperature at each meshpoint
            end
            for j = 1:1:Core_mesh
                T_core(j,1) = (T_core_temp(j,1)+T_core_temp(j+1,1))/2;   %Average temperature in the region between meshpoints
            end
        elseif z == 2  %Build temperature profile in the hot leg (Currently assuming isothermal)
            for j = 1:1:Hot_mesh
                T_hot(j,1) = T_out;
            end
        elseif z == 3  %Build temperature profile in the Hx (assuming constant cooling rate)
            Slope_Hx = (T_in-T_out)/Hx_height1;
            for j = 1:1:Hx_mesh+1
                T_Hx_temp(j,1) = Slope_Hx*(j-1)*dx_Hx1 + T_out;  %Temperature at each meshpoint
            end
            for j = 1:1:Hx_mesh
                T_Hx(j,1) = (T_Hx_temp(j,1)+T_Hx_temp(j+1,1))/2;  %Average temperature in the region between meshpoints
            end
        elseif z == 4  %Build temperature profile in the cold leg (currently assuming isothermal)
            for j = 1:1:Cold_mesh
                T_cold(j,1) = T_in;
            end
        end
    end
    Temp_Profile = T_core;
    Temp_Profile = [Temp_Profile;T_hot;T_Hx;T_cold];  %Puts all of the temperatures into a single vector
end
