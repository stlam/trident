function  [TF_bed_limit, T2_bed_limit, CP, CS, CPC, c1, c2, mt1, mt2, mt3,   j12, j23, mtg_TF, mtg_T2, mt1_TF, mt1_T2, mt1_total, tperg, Moles_Cr_ppt, GramsCr_persa, C_Cr_actual, ...
    grad, TF_consumed, T2_generated, Cr_profile, initial, Crperslice, Vol_surf, Cr_dissolved, Conc_slice, mtgbed_T2, mtgbed_TF, mtgbed2_T2, ...
    Totmatomsurf, surface, PP_TF, PP_T2, PP_T2_HX_primary, PP_T2_HX_secondary, mole_T2_poffgas, y_1_T2_p, y1_TFp, x_N_TF_p, mole_T2_soffgas, ...
    mt2_TF, y1_TFs, y_1_T2_s, x_N_TF_s, T2_PrimaryConc, T2_SecondaryConc, mole_TF_poffgas, mole_TF_soffgas, Redox_Potential, CumulativeT2_gbed2, ...
    Permeator_p_total, C_T2_permeatorexit_p, J_permeator_p, Permeator_s_total, C_T2_permeatorexit_s, J_permeator_s, CPermp, CPerms, CumulativeT2_gbed, ...
    CumulativeTF_gbed, Crcorrodedaxial, Cm1_bank, Cm2_bank, Check, Checker, Csb_next, mt1_T2_HXout, mt1_TF_HXout, mt1_TF_core, mt1_T2_core, ...
    C_Cr_actual_core, C_Cr_HXactual, CoreGraphiteCumulativeT, CoreGraphiteCumulativeT2, CoreGraphiteCumulativeTF, corrosion_time] = polythermal_v2(Ratio_TF_T2, ...
    k_HenryTF, k_HenryT2, Mole_flibe, Feedbackflag, Corrosionflag, PRF, Ds, c1, c2, j12, j23, Redoxflag, mt1, MoleT, T_uptake, ...
    tperg, DT, k_masspebble_T2, k_masspebble_TF, Vol_1, Surf_Frac, graphite_sa, Vol_2, mt2, mt3, Dp, Thick, ...
    Elements, DX, mt1_TF, mt1_TFnew, mt1_T2, mt1_T2new, mtg_TF, mtg_T2, j, CP, CS, CPC, Core_mesh, Hot_mesh, Cold_mesh, ...
    Hx_mesh, n_axial, mt1_total, T, T_avg, C_Cr_actual, CentralRef_sa, OuterRef_sa, Moles_Cr_ppt, GramsCr_persa, k_Cr_pebble, k_Cr_masspipe, ...
    TF_consumed, Cr_profile, Conc_slice, grad, initial, Crperslice, Vol_surf, T2_generated, Cr_dissolved, k_masspipe_T2, k_masspipe_TF, pipe_sa, pipe_d, ...
    pipe_l, pipe_d2, pipe_l2, Hx_tube_od, Slice_vol, slices1, slices2, slice_thick1, slice_thick2, Conc_bulk, Totmatomsurf, Lattice_param, ...
    surface, Length1_tot, Mole_frac_metal, PP_TF, PP_T2, Sievert_316_primary, Sievert_316_secondary, PP_T2_HX_primary, PP_T2_HX_secondary, ...
    GBflag, GasStrippingFlag, StrippingFlowFraction_p, StrippingFlowFraction_s, NStages_p, NStages_s, m_dot, mole_T2_poffgas, m_dot_secondary, ...
    Mole_flinak, mole_T2_soffgas, y1_TFp, y_1_T2_p, x_N_TF_p, T2_PrimaryConc, y1_TFs, y_1_T2_s, x_N_TF_s, T2_SecondaryConc, ...
    mt2_TF, d, G_s, G_p, mole_TF_poffgas, mole_TF_soffgas, Permeator_p_total, J_permeator_p, C_T2_permeatorexit_p, ...
    PermeationFlag_primary, Permeator_s_total, J_permeator_s, C_T2_permeatorexit_s, PermeationFlag_secondary, PPX, SPX, CPermp, HX1_element_vol, HX2_element_vol, ...
    HX1_element_sa, HX2_element_sa, PermElements, Permp_element_sa, Permp_element_vol, Perms_element_sa, Perms_element_vol, CPerms, ...
    Permeator_number_p, Permeator_number_s, Totalpebblegraphitemass, Tritiumcapturebedflag, mtgbed_T2, mtgbed_TF, Bed_mass, Bed_k_masspebble_T2_temp,...
    Bed_k_masspebble_TF_temp, Bed_surface_area, CumulativeT2_gbed, CumulativeTF_gbed, Bed_frac_rep, Crcorrodedaxial, v_dot, Cm1_bank, Cm2_bank, ...
    v_dot2, k_transp2, k_henryT22, Csb_next, mt1_T2_HXout, mt1_TF_HXout, mt1_TF_core, mt1_T2_core, Molar_dot, C_Cr_actual_core, k_masspipe_T2_perm1, ...
    k_masspipe_T2_perm2, C_Cr_HXactual, Loops, PRFLoops1, CoreGraphiteCumulativeT, CoreGraphiteCumulativeT2, CoreGraphiteCumulativeTF, CoreRefuelFrac, ...
    t, Surf_area_gb, corrosion_time, mtgbed2_T2, CumulativeT2_gbed2, Bed_k_masspebble_T2_temp_s, Bed_surface_area_s, Bed_mass_s, Bed_frac_rep_s, Tritiumcapturebedflag_s, TF_bed_limit, T2_bed_limit)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function carries out the major transport calculations in the system
% This function calls out to the corrosion function
% This function call out to the gas stripping function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Check = 0;
Checker = 0;


%%%% This is the part where TRITIUM IS BORN IN THE COOLANT for the
%%%% The manner of birth depends on he options selected.
%%%% Birth rate is uniform throughout the core
if j >= 1 && j <= Core_mesh
    
    if j == 1
        if Redoxflag == 2 %T born as TF
            mt1_TF_core = (mt1_TF/Mole_flibe)*Molar_dot*DT;  %In this case, mt1_TF_core is the total moles of TF to flow into the core axial segment 1
            mt1_T2_core = (mt1_T2/Mole_flibe)*Molar_dot*DT;  %In this case, mt1_T2_core is the total moles of T2 to flow into the core axial segment 1
            C_total_core = (mt1_TF_core + 2*mt1_T2_core + MoleT/Core_mesh)/(Molar_dot*DT); %New T concentration [Mole of T atoms/Mole of flibe entering the core in time DT] includes tritium birth term "MoleT" for the coolant entering the core
        else %Redoxflag == 1 and only T2 is considered
            mt1_TF_core = 0;  %In this case, mt1_TF_core is the total moles of TF to flow into the core axial segment 1
            mt1_T2_core = (mt1_T2/Mole_flibe)*Molar_dot*DT;  %In this case, mt1_T2_core is the total moles of T2 to flow into the core axial segment 1
            C_total_core = (mt1_TF_core + 2*mt1_T2_core + MoleT/Core_mesh)/(Molar_dot*DT); %New T concentration [Mole of T atoms/Mole of flibe entering the core in time DT] includes tritium birth term "MoleT" for the coolant entering the core
        end
    else
        %If j > 1 the mt1_TF_core and mt1_T2_core are set by the pass through the first axial segment
        C_total_core = ((mt1_TF_core + 2*mt1_T2_core) + (MoleT/Core_mesh))/(Molar_dot*DT) ; %New T conetration [Mole of T atoms/Mole of flibe entering the next axial segment of the core in time DT] includes tritium birth term "MoleT"
    end
    
    if Feedbackflag == 1; %Feedback OFF, uses a constant generation rate for T2 and TF, allows redox to drift a little because the generation rates for TF and T2 are fixed, but the relative amounts of TF versus T2 in the coolant will vary due diffusion of T2 and buildup of TF and some T2.
        %Production of T2 and TF in primary
        mt1_TF_core = mt1_TF_core + mt1_TFnew/Core_mesh;  %TF in primary [mole T+] at beginning of step INCLUDES TRITIUM BIRTH TERM in "mt1_TFnew"
        mt1_T2_core = mt1_T2_core + mt1_T2new/Core_mesh;  %T2 in primary [mole T2] at beginning of step INCLUDES TRITIUM BIRTH TERM in "mt1_T2new"
    elseif Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt.  If some T2 diffuses through the primary Hx, the remaining T2 and TF is adjusted to maintain the specified redox state.  When TF is born, it is also speciated into TF and T2 in order to maintain the specified redox state in the salt.
        %Feedback for redox and tritium speciation
        %This is the part where tritium is born and then speciated into TF and T2 depending on the fixed redox condition specified
        C_T2_core = (4*C_total_core*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total_core*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
            +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
        C_TF_core = C_total_core -2*C_T2_core;        %[mole TF/mole flibe] mole fraction TF in flibe core segment
        %             PP_TF_core = C_TF_core/k_HenryTF;  %Partial pressure of TF above the melt in the current core segment [atm]
        %             PP_T2_core = C_T2_core/k_HenryT2;  %Partial pressure of T2 above the melt in the current core segment [atm]
        mt1_T2new = C_T2_core*(Molar_dot*DT) - mt1_T2_core;  %[Change in moles of T2 in primary] due to generation in flibe and redox control feedback.  mt1_Txnew is calculated in case the new value is less than the old one, then mt1_new will be negative.
        mt1_TFnew = C_TF_core*(Molar_dot*DT) - mt1_TF_core;  %[Change in moles TF in primary] due to generation in flibe and redox control feedback
        mt1_T2_core = mt1_T2_core + mt1_T2new;  %Current T2 in primary [mole T2] at beginning of time step
        mt1_TF_core = mt1_TF_core + mt1_TFnew;  %Current TF in primary [mole T+] at beginning of time step
        mt1_T2 = mt1_T2 + mt1_T2new;  %Current total T2 in total primary [mole T2] at beginning of time step
        mt1_TF = mt1_TF + mt1_TFnew;  %Current total TF in total primary [mole T+] at beginning of time step
        C_T2 = mt1_T2/Mole_flibe;
        C_TF = mt1_TF/Mole_flibe;
        PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
        PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
        mt1_total = 2*mt1_T2 + mt1_TF;  %Current total moles of tritium in the primary loop
        
    elseif Feedbackflag == 3;  %Redox is calculated, but if Redoxflag not 1, all T is born as TF, and T2 is only produced in corrosion reactions with Cr.
        %If Redoxflag = 1, all T is born as T2 and no TF is ever in the system. No corrosion is modeled
        if Redoxflag == 2
            %Location where tritium birth in the coolant occurs
            mt1_TFnew = MoleT/Core_mesh;  %Amount of tritium born in 1 time step. Because feedbackflag = 3, all tritium is born as TF
            mt1_T2new = 0;  %No T2 is born directly from neutron transmutation
            mt1_TF = mt1_TF + mt1_TFnew;  %Current total moles of TF in the primary
            mt1_T2 = mt1_T2 + mt1_T2new;  %Current total moles of T2 in the primary
            mt1_T2_core = mt1_T2_core + mt1_T2new;  %Current T2 in primary [mole T2] at beginning of time step
            mt1_TF_core = mt1_TF_core + mt1_TFnew;  %Current TF in primary [mole T+] at beginning of time step
            C_T2_core = mt1_T2_core/(Molar_dot*DT);  %Current concentration of T2 in the primary [mole T2/mole Flibe]
            C_TF_core = mt1_TF_core/(Molar_dot*DT);  %Current concentration of T2 in the primary [mole T2/mole Flibe]
            C_TF = mt1_TF/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
            C_T2 = mt1_T2/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
            
            mt1_total = 2*mt1_T2 + mt1_TF;  %Current total moles of tritium in the primary [moles T]
            PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
            PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
        else %Redoxflag == 1 where everything is born as T2
            mt1_TFnew = 0;
            mt1_T2new = 0.5*MoleT/Core_mesh;  %mt1_T2new is moles of new T2 born.  (MoleT is moles T born in the time step)
            mt1_TF = mt1_TF + mt1_TFnew;  %Current total moles of TF in the primary
            mt1_T2 = mt1_T2 + mt1_T2new;  %Current total moles of T2 in the primary
            mt1_T2_core = mt1_T2_core + mt1_T2new;  %Current T2 in primary [mole T2] at beginning of time step
            mt1_TF_core = mt1_TF_core + mt1_TFnew;  %Current TF in primary [mole T+] at beginning of time step
            C_T2_core = mt1_T2_core/(Molar_dot*DT);  %Current concentration of T2 in the primary [mole T2/mole Flibe]
            C_TF_core = mt1_TF_core/(Molar_dot*DT);  %Current concentration of T2 in the primary [mole T2/mole Flibe]
            C_TF = mt1_TF/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
            C_T2 = mt1_T2/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
            mt1_total = 2*mt1_T2 + mt1_TF;  %Current total moles of tritium in the primary [moles T]
            PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
            PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
        end
    end
else
end
%%%% End of Tritium birth portion of code %%%%

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% REACTOR CORE %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
%     Flibe_MM = 32.8931;  %Molar mass of flibe [g/mol]
%     m_dotm = m_dot*(1000/Flibe_MM);  %Molar flow rate in primary coolant [mole flibe/s].  m_dot is in kg/s
if j >= 1 && j <= Core_mesh
    %TF and T2 uptake on graphite is allowed in the reactor core, so set bounds based on the index "j"
    %Uptake on graphite is allowed before diffusion through HX
    %Because certain graphite in parts of the core may saturate before others, the graphite pebbles are divided according to the number of axial segments of the core based on the input variable "Core_mesh"
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % TF uptake on core graphite %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if T_uptake == 2 && j >= 1 && j <= Core_mesh   %1 = OFF; 2 = ON, only allowed in the core region defined by j between 1 and Core_mesh
        %Implementation of variable Tgraphite (graphite capacity for tritium) based on temperature and pressure
        mtg_TF(j,1) = mtg_TF(j,1)*(1-CoreRefuelFrac*DT/Core_mesh);  %Option for allowing pebble "refueling".  Essentially resets the amount of tritium held on the graphite as if
        %refueling had occured and pebbles laden with tritium were replaced with fresh pebbles or pebbles which had been processed to remove T.
        PP_TF_core = (C_TF_core/k_HenryTF)*101325;  %Partial pressure of TF above the melt [Pa]
        Tgraphite_TF = ((((2*0.00019)*sqrt(PP_TF_core)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Capacity, Moles TF per gram graphite at T[K] and P[Pa]. From Atsumi, 1988
        %             Tgraphite_TF = Tgraphite_TF/20; %accel
        if mtg_TF(j,1) < Tgraphite_TF*Totalpebblegraphitemass/Core_mesh  %If the graphite in core axial zone "j" has not been saturated, transport additional TF and T2 to the graphite
            mt1_TF_old = mt1_TF;
            mt1_TF_core_old = mt1_TF_core;  %Record moles of TF in coolant prior to updating with new calculation
            mtg_TF_old = mtg_TF(j,1);  %Record the moles of T+ on the graphite before updating with new calculation
            jg_TF = k_masspebble_TF*((mt1_TF_core/(v_dot*DT))-(Surf_Frac*mt1_TF_core/(v_dot*DT)));  %Flux of T+ to graphite surface based on the mass transfer coefficient k_masspebble [mol/m^2-s]
            mtg_TF(j,1) = mtg_TF(j,1) + jg_TF*(graphite_sa/Core_mesh)*DT;  %Calculate new total moles of T+ adsorbed onto graphite in the core axial segment "j" [mol] assuming all T+ reaching graphite surface is adsorbed
            CoreGraphiteCumulativeT_old = CoreGraphiteCumulativeT;
            CoreGraphiteCumulativeTF_old = CoreGraphiteCumulativeTF;
            CoreGraphiteCumulativeT = CoreGraphiteCumulativeT + jg_TF*(graphite_sa/Core_mesh)*DT; %Cumulative moles of T absorbed on pebble graphite in the core
            CoreGraphiteCumulativeTF = CoreGraphiteCumulativeTF + jg_TF*(graphite_sa/Core_mesh)*DT;
            mt1_TF_core = mt1_TF_core - jg_TF*(graphite_sa/Core_mesh)*DT;  %Calc new mt1_TF moles of TF in the coolant after some has been adsorbed on graphite
            mt1_TF = mt1_TF - jg_TF*(graphite_sa/Core_mesh)*DT;
            C_TF_core = mt1_TF_core/(Molar_dot*DT);  %Update the current concentration of TF in primary [mole TF/mol Flibe]
            PP_TF_core = (C_TF_core/k_HenryTF)*101325;  %Update the partial pressure of TF above the melt [Pa]
            Tgraphite_TF = (((2*0.00019*sqrt(PP_TF_core)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Update the capacity for TF [moles TF/g graphite] based on the new partial pressure, Moles T per gram graphite at T[K] and P[Pa]
            %                 Tgraphite_TF = Tgraphite_TF/20;  %accel
            if mtg_TF(j,1) > Tgraphite_TF*Totalpebblegraphitemass/Core_mesh  %If this is true, the graphite was just saturated with TF. Return values to the pre-calculation values and exit
                mt1_TF_core = mt1_TF_core_old;
                C_TF_core = mt1_TF_core/(Molar_dot*DT);  %Update the current concentration of TF in primary [mole TF/mol Flibe]
                mt1_TF = mt1_TF_old;
                mtg_TF(j,1) = mtg_TF_old;
                CoreGraphiteCumulativeT = CoreGraphiteCumulativeT_old;
                CoreGraphiteCumulativeTF = CoreGraphiteCumulativeTF_old;
            else %Accept the values from the calculation and proceed
            end
        else %Graphite is already saturated.  Do not transport to it any more
        end
    else  %Graphite uptake is not turned on.  Do not do uptake calculations.
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % T2 uptake on core graphite %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if T_uptake == 2 && j >= 1 && j <= Core_mesh   %1 = OFF; 2 = ON, only allowed in the core region defined by j between 1 and Core_mesh
        %Implementation of variable Tgraphite (graphite capacity for tritium) based on temperature and pressure
        mtg_T2(j,1) = mtg_T2(j,1)*(1-CoreRefuelFrac*DT/Core_mesh);  %Option for allowing pebble "refueling".  Essentially resets the amount of tritium held on the graphite as if
        %refueling had occured and pebbles laden with tritium were replaced with fresh pebbles or pebbles which had been processed to remove T.
        PP_T2_core = (C_T2_core/k_HenryT2)*101325;  %Partial pressure of T2 above the melt [Pa]
        Tgraphite_T2 = ((((2*0.00019)*sqrt(PP_T2_core)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Capacity, Moles T2 per gram graphite at T[K] and P[Pa]. From Atsumi, 1988
        Tgraphite_T2 = Tgraphite_T2/20;  %acell
        if mtg_T2(j,1) < Tgraphite_T2*Totalpebblegraphitemass/Core_mesh  %If the graphite in core axial zone "j" has not been saturated, transport additional T2 and T2 to the graphite
            mt1_T2_old = mt1_T2;
            mt1_T2_core_old = mt1_T2_core;  %Record moles of T2 in coolant prior to updating with new calculation
            mtg_T2_old = mtg_T2(j,1);  %Record the moles of T2 on the graphite before updating with new calculation
            jg_T2 = k_masspebble_T2*((mt1_T2_core/(v_dot*DT))-(Surf_Frac*mt1_T2_core/(v_dot*DT)));  %Flux of T2 to graphite surface based on the mass transfer coefficient k_masspebble [mol/m^2-s]
            mtg_T2(j,1) = mtg_T2(j,1) + jg_T2*(graphite_sa/Core_mesh)*DT;  %Calculate new total moles of T2 adsorbed onto graphite in the core axial segment "j" [mol] assuming all T+ reaching graphite surface is adsorbed
            CoreGraphiteCumulativeT_old = CoreGraphiteCumulativeT;
            CoreGraphiteCumulativeT2_old = CoreGraphiteCumulativeT2;
            CoreGraphiteCumulativeT = CoreGraphiteCumulativeT + 2*jg_T2*(graphite_sa/Core_mesh)*DT; %Cumulative moles of T atoms absorbed on pebble graphite in the core
            CoreGraphiteCumulativeT2 = CoreGraphiteCumulativeT2 + 2*jg_T2*(graphite_sa/Core_mesh)*DT;
            mt1_T2_core = mt1_T2_core - jg_T2*(graphite_sa/Core_mesh)*DT;  %Calc new mt1_T2 moles of T2 in the coolant after some has been adsorbed on graphite
            mt1_T2 = mt1_T2 - jg_T2*(graphite_sa/Core_mesh)*DT;
            C_T2_core = mt1_T2_core/(Molar_dot*DT);  %Update the current concentration of T2 in primary [mole T2/mol Flibe]
            PP_T2_core = (C_T2_core/k_HenryT2)*101325;  %Update the partial pressure of T2 above the melt [Pa]
            Tgraphite_T2 = (((2*0.00019*sqrt(PP_T2_core)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Update the capacity for T2 [moles T2/g graphite] based on the new partial pressure, Moles T per gram graphite at T[K] and P[Pa]
            %                 Tgraphite_T2 = Tgraphite_T2/20;  %accel
            if mtg_T2(j,1) > Tgraphite_T2*Totalpebblegraphitemass/Core_mesh  %If this is true, the graphite was just saturated with T2. Return values to the pre-calculation values and exit
                mt1_T2_core = mt1_T2_core_old;
                C_T2_core = mt1_T2_core/(Molar_dot*DT);  %Update the current concentration of T2 in primary [mole T2/mol Flibe]
                mt1_T2 = mt1_T2_old;
                mtg_T2(j,1) = mtg_T2_old;
                CoreGraphiteCumulativeT = CoreGraphiteCumulativeT_old;
                CoreGraphiteCumulativeT2 = CoreGraphiteCumulativeT2_old;
            else %Accept the values from the calculation and proceed
            end
        else %Graphite is already saturated.  Do not transport to it any more
        end
    else  %Graphite uptake is not turned on.  Do not do uptake calculations.
    end
    
    
    %Need to check for Cr corrosion/deposition in the reactor core.  Currently, only deposition in the core is accounted for because the core does not have metal facing the coolant.
    if Corrosionflag == 1      %Corrosion is off
    elseif Corrosionflag == 2  %Corrosion is on
        dens_flibe = (2415.6-0.49072*T)*1000;  %Flibe density from Janz correlation in Sohal, 2010 [g/m^3]
        Cr_MM = 51.9961;  %Cr molar mass [g/mol]
        Flibe_MM = 32.8931;  %Molar mass of flibe [g/mol]
        C_Cr_metal = Mole_frac_metal(1,1);  %Mole fraction of Cr in the base metal [mol Cr/mol Stainless]
        if Feedbackflag == 1||Feedbackflag == 3   %Calculate the current ratio of TF to T2 in the salt
            PP_TF_core = C_TF_core/k_HenryTF;  %Partial pressure of TF above the melt in the current core segment [atm]
            PP_T2_core = C_T2_core/k_HenryT2;  %Partial pressure of T2 above the melt in the current core segment [atm]
            if PP_T2_core <= 0  %If no T2 is in the system yet, set a very small value so that a Ratio_TF_T2 can be calculated below
                PP_T2_core = 1E-40;
            else
            end
            Ratio_TF_T2 = PP_TF_core/sqrt(PP_T2_core);  %Calculate current ratio of TF to T2
        else
        end
        C_Cr_eq = (((Cr_MM/Flibe_MM)*1E6)*10^(-5.12+((9.06E3)/T)+log10(C_Cr_metal)+log10(Ratio_TF_T2^2)))*(1E-6)*(Vol_1*dens_flibe/Cr_MM)*(1/Vol_1);  %Calculate local equilibrium Cr concentration given the base metal [moles Cr/m3 coolant]
        if j == 1
            C_Cr_actual_core = C_Cr_actual;  %Cr concentration in the current coolant volume element
        else
        end
        if C_Cr_actual_core - C_Cr_eq > 0  %If yes, then do preciptation of Cr
            J_ppt_pebble = k_Cr_pebble*(C_Cr_actual_core - C_Cr_eq);  %Flux of dissolved Cr to pebble surface [mol Cr2+/m2-s]
            J_ppt_reflector = k_Cr_masspipe*(C_Cr_actual_core - C_Cr_eq); %Flux of dissolved Cr to reflector surface [mol Cr2+/m2-s]
            New_moles_Cr_ppt = (J_ppt_pebble*graphite_sa + J_ppt_reflector*CentralRef_sa + J_ppt_reflector*OuterRef_sa)*DT/Core_mesh;  %Moles Cr deposited in the current timestep at the current axial position
            if New_moles_Cr_ppt > C_Cr_actual_core*(v_dot*DT)  %Ensure that amount of Cr precipitated from the current time step does not exceed the amount of Cr presently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + C_Cr_actual_core*(v_dot*DT);  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - C_Cr_actual_core*(v_dot*DT);   %Update total moles of T2 in primary.  Precipitation means CrF2 + T2 = Cr + 2TF
                mt1_TF = mt1_TF + 2*C_Cr_actual_core*(v_dot*DT); %Update total moles of TF in primary
                mt1_T2_core = mt1_T2_core - C_Cr_actual_core*(v_dot*DT);   %Update moles of T2 in current coolant volume segment.  Precipitation means CrF2 + T2 = Cr + 2TF
                mt1_TF_core = mt1_TF_core + 2*C_Cr_actual_core*(v_dot*DT); %Update moles of TF in current coolant volume segment
                C_Cr_actual = C_Cr_actual - C_Cr_actual_core*(v_dot*DT);
                C_Cr_actual_core = 0;  %[mol Cr/m3] concentration of Cr dissolved in current coolant volume element
            else  %If amount of Cr preciptated from the current time step is less than the total amount of Cr currently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + New_moles_Cr_ppt;  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - New_moles_Cr_ppt;   %Update total moles of T2 in primary.  One T2 consumed for every Cr precipitated
                mt1_TF = mt1_TF + 2*New_moles_Cr_ppt; %Update total moles of TF in primary.  Two TF generated for every Cr precipitated
                mt1_T2_core = mt1_T2_core - New_moles_Cr_ppt;   %Update total moles of T2 in primary.  One T2 consumed for every Cr precipitated
                mt1_TF_core = mt1_TF_core + 2*New_moles_Cr_ppt; %Update total moles of TF in primary.  Two TF generated for every Cr precipitated
                C_Cr_actual = (C_Cr_actual*Vol_1 - New_moles_Cr_ppt)/(Vol_1);  %New concentration of Cr normalized throughout the coolant [mol Cr/m3]
                C_Cr_actual_core = (C_Cr_actual_core*v_dot*DT - New_moles_Cr_ppt)/(v_dot*DT);  %New concentration of Cr in coolant volume element [mol Cr/m3]
            end
            GramsCr_persa(j,1) = (Moles_Cr_ppt(j,1)*Cr_MM/(graphite_sa + CentralRef_sa + OuterRef_sa))/Core_mesh;  %Total grams of Cr deposited per unit surface area [g Cr/m2]
        else   %If C_Cr_actual - C_Cr_eq < 0, THEN do corrosion. No metal in the core.  No corrosion in the core.
        end
        
        
        %As coolant exits the core and enters the first volume element
        %of the hot leg, adjust T2 vs TF if fixed redox condition is
        %applied.  Coolant is allowed to mix completely in the hot and
        %cold leg pipes.
        if j == Core_mesh
            if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
                C_total_core =  (mt1_TF_core + 2*mt1_T2_core)/(Molar_dot*DT); %[Mole of T atoms/Mole of flibe]
                C_T2_core = (4*C_total_core*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total_core*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
                    +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
                C_TF_core = C_total_core -2*C_T2_core;        %[mole TF/mole flibe] mole fraction TF in flibe
                mt1_T2new = C_T2_core*(Molar_dot*DT) - mt1_T2_core;  %[Change in moles of T2 in primary] due to redox control feedback.  mt1_Txnew is calculated in case the new value is less than the old one, then mt1_new will be negative.
                mt1_TFnew = C_TF_core*(Molar_dot*DT) - mt1_TF_core;  %[Change in moles TF in primary] due to redox control feedback
                mt1_T2 = mt1_T2 + mt1_T2new;  %Current total T2 in total primary [mole T2] at entrance to hot leg
                mt1_TF = mt1_TF + mt1_TFnew;  %Current total TF in total primary [mole T+] at entrance to hot leg
                
            else
            end
        else
        end
    end
    
    mt1_total = mt1_TF + 2*mt1_T2;  %[mole tritium]  total moles of tritium in the primary system
    C_T2 = mt1_T2/Mole_flibe;  %Current concentration of T2 in the primary [mole T2/mole Flibe]
    C_TF = mt1_TF/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
    PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
    PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
    if PP_T2 <=0
        PP_T2 = 1E-40;  %Set PP_T2 to vanishinghly small so that we don't get division by zero below
    else
    end
    Redox_Potential = 2*0.00831446*T*log(PP_TF/sqrt(PP_T2))+2*(-4.6976E-10*T^3 + 3.1425E-6*T^2 - 8.8612E-3*T - 2.7305E2);  %[kJ/mol F2] Temp in [K] Calculate redox fluorine potential at the current axial location [kJ/mol F2]
    
    
    %%%%%%%%%%%%%%%%%
    %%%% HOT LEG %%%%
    %%%%%%%%%%%%%%%%%
    
elseif j >= Core_mesh +1 && j <= Core_mesh + Hot_mesh
    
    if j == Core_mesh +1 %Option to do graphite capture bed only upon entering the hot leg
        %OPTIONAL GRAPHITE BED for absorption of tritium placed at core
        %exit.  Corrosion deposition is not modeled here at the moment.
        if Tritiumcapturebedflag == 2 %if this is true, then run the graphite capture bed
            C_TF = mt1_TF/Mole_flibe;  %Current concentration of TF in primary [mole TF/mol Flibe]
            PP_TF = (C_TF/k_HenryTF)*101325;  %Partial pressure of TF above the melt [Pa]
            Tgraphite_TF = ((((2*0.00019)*sqrt(PP_TF)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Capacity, [Moles TF per gram graphite] at T[K] and P[Pa]. From Atsumi, 1988
            TF_bed_limit = Tgraphite_TF;       %solubility limit of the graphite pebble [mol TF per gram graphite]            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Capture TF on graphite bed %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %Alter mtgbed_TF if graphite bed removal fraction is > 0.
            %This means that some of the graphite in the bed is removed at some rate so that the tritium can be removed and the graphite regenerated.  Thus, the total amount of TF on the
            %graphite has decreased but the bed mass remains constant
            mtgbed_TF = mtgbed_TF*(1-Bed_frac_rep*DT);  %Bed_frac_rep is the fraction of the bed graphite that is replaced per second.  If Bed_frac_rep = 0, then no replacement occurs.
            if mtgbed_TF < Tgraphite_TF*Bed_mass  %If moles of TF on grahpite in the absorption bed has not been saturated, do transport
                mt1_TF_old = mt1_TF;  %Record old moles of TF in the primary coolant
                mtgbed_TF_old = mtgbed_TF;  %Record old moles of TF on the bed graphite
                
                %Solve for the concentration at the surface of the graphite by uisng solubilty equaiton and henry's law        
                PP_TFeq = ((mtgbed_TF/Bed_mass)*(8314.46*273.15)/100/(exp(19/(0.00831446*T)))/(2*0.00019))^2;       %TF equilibrium pressure in Pa with the solid ISO-88
                C_TF_surf = (PP_TFeq/101325)*k_HenryTF*(Mole_flibe/Vol_1);    %surface concentration of TF using henry's law [mol TF/m^3 of flibe]
                %%%%%%%%%%%%%%%%%%%%%
                
                jgbed_TF = Bed_k_masspebble_TF_temp*((mt1_TF/Vol_1) - C_TF_surf);
                mtgbed_TF = mtgbed_TF + jgbed_TF*(Bed_surface_area)*DT;  %Calculate new total moles of T+ adsorbed onto bed graphite [mol] assuming all T+ reaching graphite surface is adsorbed
                CumulativeTF_old_gbed = CumulativeTF_gbed;  %Total, Cumulative moles of TF absorbed on the graphite up to this point [moles TF]
                CumulativeTF_gbed = CumulativeTF_gbed + jgbed_TF*(Bed_surface_area)*DT; %New cumulative amount of TF captured on graphite in the bed
                mt1_TF = mt1_TF - jgbed_TF*(Bed_surface_area)*DT;  %Calc new mt1_TF moles of TF in the coolant after some has been adsorbed on graphite
                C_TF = mt1_TF/Mole_flibe;  %Update the current concentration of TF in primary [mole TF/mol flibe]
                PP_TF = (C_TF/k_HenryTF)*101325;  %Calculate new partial pressure of TF above the melt [Pa]
                Tgraphite_TF = ((((2*0.00019)*sqrt(PP_TF)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Calculate new bed capacity for TF [mole TF/g graphite]
                if mtgbed_TF > Tgraphite_TF*Bed_mass  %If this is true, the graphite has saturated.  Return the values to the previous ones.
                    mt1_TF = mt1_TF_old;
                    mtgbed_TF = mtgbed_TF_old;
                    CumulativeTF_gbed = CumulativeTF_old_gbed;
                else %Accept the values from the calculation and proceed
                end
            else %Graphite is already saturated.  Do not transport to it any more
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% Capture T2 on graphite bed %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            C_T2 = mt1_T2/Mole_flibe;  %Current concentration of T2 in the primary [mole T2/mole Flibe]
            PP_T2 = (C_T2/k_HenryT2)*101325;  %Partial pressure of T2 above the melt [Pa]
            Tgraphite_T2 = ((((0.00019)*sqrt(PP_T2)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Capacity, [Moles T2 per gram graphite] at T[K] and P[Pa]. From Atsumi, 1988
            T2_bed_limit = Tgraphite_T2; %solubility limit of the graphite pebble [mol T2 per gram graphite]   
            
            mtgbed_T2 = mtgbed_T2*(1-Bed_frac_rep*DT);  %Bed_frac_rep is the fraction of the bed graphite that is replaced per second by fresh graphite. If Bed_frac_rep = 0, then no replacement occurs.
            
            if mtgbed_T2 < Tgraphite_T2*Bed_mass  %If moles of T2 on grahpite in the absorption bed has not been saturated, do transport
                mt1_T2_old = mt1_T2;  %Record old moles of T2 in the primary coolant
                mtgbed_T2_old = mtgbed_T2;  %Record old moles of T2 on the bed graphite
                
                %Solve for the concentration at the surface of the graphite by uisng solubilty equaiton and henry's law
                PP_T2eq = ((mtgbed_T2/Bed_mass)*(8314.46*273.15)/100/(exp(19/(0.00831446*T)))/(0.00019))^2;       %ISO-88 TF equilibrium pressure in Pa with the solid 
                C_T2_surf = (PP_T2eq/101325)*k_HenryT2*(Mole_flibe/Vol_1);    %surface concentration of T2 using henry's law [mol TF/m^3 of flibe]
                %%%%%%%%%%%%%%%%%%%%%
                
                jgbed_T2 = Bed_k_masspebble_T2_temp*((mt1_T2/Vol_1) - C_T2_surf);
                mtgbed_T2 = mtgbed_T2 + jgbed_T2*(Bed_surface_area)*DT;  %Calculate new total moles of T2 adsorbed onto bed graphite [mol] assuming all T2 reaching graphite surface is adsorbed
                CumulativeT2_old_gbed = CumulativeT2_gbed;  %Total, Cumulative moles of T2 absorbed on the graphite up to this point [moles T2]
                CumulativeT2_gbed = CumulativeT2_gbed + jgbed_T2*(Bed_surface_area)*DT; %New cumulative amount of T2 captured on graphite in the bed
                mt1_T2 = mt1_T2 - jgbed_T2*(Bed_surface_area)*DT;  %Calc new mt1_T2 moles of T2 in the coolant after some has been adsorbed on graphite
                C_T2 = mt1_T2/Mole_flibe;  %Update the current concentration of T2 in primary [mole T2/mol flibe]
                PP_T2 = (C_T2/k_HenryT2)*101325;  %Calculate new partial pressure of T2 above the melt
                Tgraphite_T2 = ((((0.00019)*sqrt(PP_T2)*exp(19/(0.00831446*T)))*100)/(8314.46*273.15));  %Calculate new bed capacity for T2 [mole T2/g graphite]

                if mtgbed_T2 > Tgraphite_T2*Bed_mass  %If this is true, the graphite has saturated.  Return the values to the previous ones.
                    mt1_T2 = mt1_T2_old;
                    mtgbed_T2 = mtgbed_T2_old;
                    CumulativeT2_gbed = CumulativeT2_old_gbed;
                else %Accept the values from the calculation and proceed
                end
            else %Graphite is already saturated.  Do not transport to it any more
            end
            
            %Model corrosion/deposition here? If so, need mass transfer
            %parameters
            
            %Do redox balance for coolant exiting the bed if the redox is held constant in the coolant
            if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
                C_total =  (mt1_TF + 2*mt1_T2)/(Mole_flibe); %[Mole of T atoms/Mole of flibe]
                C_T2 = (4*C_total*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
                    +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
                C_TF = C_total -2*C_T2;        %[mole TF/mole flibe] mole fraction TF in flibe
                mt1_T2new = C_T2*(Mole_flibe) - mt1_T2;  %[Change in moles of T2 in primary] due to redox control feedback.  mt1_Txnew is calculated in case the new value is less than the old one, then mt1_new will be negative.
                mt1_TFnew = C_TF*(Mole_flibe) - mt1_TF;  %[Change in moles TF in primary] due to redox control feedback
                mt1_T2 = mt1_T2 + mt1_T2new;  %Current total T2 in total primary [mole T2] at entrance to hot leg
                mt1_TF = mt1_TF + mt1_TFnew;  %Current total TF in total primary [mole T+] at entrance to hot leg
                
            else
            end
        else
        end
    
    else
    end
    
    % Now in HOT LEG consider corrosion/deposition and generation of T2 from corrosion
    % etc.  Call out to corrosion module if corrosion is deemed to occur
    
    if Corrosionflag == 1      %Corrosion is off
    elseif Corrosionflag == 2  %Corrosion is on
        dens_flibe = (2415.6-0.49072*T)*1000;  %Flibe density from Janz correlation in Sohal, 2010 [g/m^3]
        Cr_MM = 51.9961;  %Cr molar mass [g/mol]
        Flibe_MM = 32.8931;  %Molar mass of flibe [g/mol]
        if Feedbackflag == 1||Feedbackflag == 3   %Calculate the current ratio of TF to T2 in the salt
            C_TF = mt1_TF/Mole_flibe;
            C_T2 = mt1_T2/Mole_flibe;
            PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
            PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
            if PP_T2 <= 0  %If no T2 is in the system yet, set a very small value so that a Ratio_TF_T2 can be calculated below
                PP_T2 = 1E-40;
            else
            end
            Ratio_TF_T2 = PP_TF/sqrt(PP_T2);  %Calculate current ratio of TF to T2
        else
        end
        C_Cr_metal = Mole_frac_metal(1,1);  %Mole fraction of Cr in the base metal [mol Cr/mol Stainless]
        C_Cr_eq = (((Cr_MM/Flibe_MM)*1E6)*10^(-5.12+((9.06E3)/T)+log10(C_Cr_metal)+log10(Ratio_TF_T2^2)))*(1E-6)*(Vol_1*dens_flibe/Cr_MM)*(1/Vol_1);  %Calculate equilibrium Cr concentration given the base metal [moles Cr/m3 coolant]
        if C_Cr_actual - C_Cr_eq > 0  %If yes, then do preciptation of Cr
            J_ppt_pipe = k_Cr_masspipe*(C_Cr_actual - C_Cr_eq); %Flux of dissolved Cr to reflector surface [mol Cr2+/m2-s]
            New_moles_Cr_ppt = (J_ppt_pipe*pipe_sa(j,1))*DT;  %Moles Cr deposited in the current timestep at the current axial position
            if New_moles_Cr_ppt > C_Cr_actual*Vol_1  %Ensure that amount of Cr precipitated from the current time step does not exceed the amount of Cr presently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + C_Cr_actual*Vol_1;  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - C_Cr_actual*Vol_1;   %Update total moles of T2 in primary
                mt1_TF = mt1_TF + 2*C_Cr_actual*Vol_1; %Update total moles of TF in primary
                C_Cr_actual = 0;  %[mol Cr/m3] concetration of Cr dissolved in primary coolant
            else  %If amount of Cr preciptated from the current time step is less than the total amount of Cr currently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + New_moles_Cr_ppt;  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - New_moles_Cr_ppt;   %Update total moles of T2 in primary
                mt1_TF = mt1_TF + 2*New_moles_Cr_ppt; %Update total moles of TF in primary
                C_Cr_actual = C_Cr_actual - New_moles_Cr_ppt/Vol_1;  %New concentration of Cr in coolant [mol Cr/m3]
            end
            GramsCr_persa(j,1) = (Moles_Cr_ppt(j,1)*Cr_MM/(pipe_sa(j,1)));  %Total grams of Cr deposited per unit surface area at the particular axial location [g Cr/m2]
            
        elseif C_Cr_actual - C_Cr_eq < 0   %Then do corrosion
            Cr_dissolved = C_Cr_actual*Vol_1;  %[moles Cr2+ in primary coolant]
            Conc_TF = mt1_TF/Vol_1;        %[mole TF/m3 flibe] current concentration of TF in primary coolant
            %need to read in k_masspipe to corrosion module
            corrosion_time(j,1) = corrosion_time(j,1) + DT;
            [Cr_dissolved, Cr_coolant, TF_consumed, T2_generated, Cr_profile, mt1_TF, mt1_T2, ...
                Conc_slice, grad, Crperslice, initial, Vol_surf, Totmatomsurf, surface, Crcorrodedaxial] = CorrosionModule(j, initial, surface, Conc_TF, k_masspipe_TF, Vol_surf, ...
                pipe_sa, pipe_d, pipe_l, pipe_d2, pipe_l2, Hx_tube_od, Vol_1, Thick, DT, Slice_vol, Crperslice, ...
                Cr_dissolved, TF_consumed, Cr_profile, slices1, slices2, slice_thick1, slice_thick2, Conc_slice, ...
                Conc_bulk, Totmatomsurf, grad, Lattice_param, mt1_TF, mt1_T2, T, Core_mesh, Hot_mesh, Hx_mesh, Cold_mesh, ...
                Length1_tot, Crcorrodedaxial, t, Surf_area_gb);  %Call out to corrosion module
            C_Cr_actual = Cr_coolant;  %[moles Cr/m3]
        end
        %Before moving on, adjust T2 vs TF depending on redox option
        if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
            C_total =  mt1_TF/Mole_flibe + 2*mt1_T2/Mole_flibe; %[Mole of T atoms/Mole of flibe]
            C_T2 = (4*C_total*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
                +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
            C_TF = C_total -2*C_T2;        %[mole TF/mole flibe] mole fraction TF in flibe
            mt1_T2new = C_T2*Mole_flibe - mt1_T2;  %[Change in moles of T2 in primary] due to generation in flibe and thermodynamic feedback
            mt1_TFnew = C_TF*Mole_flibe - mt1_TF;  %[Change in moles TF in primary] due to generation in flibe and thermodynamic feedback
            mt1_T2 = mt1_T2 + mt1_T2new;  %Current T2 in primary [mole T2]
            mt1_TF = mt1_TF + mt1_TFnew;  %Current TF in primary [mole T+]
            %mt1_total = 2*mt1_T2 + mt1_TF;  %Current total moles of tritium in the primary loop
        else
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Permeation Window Calculations %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if j == Core_mesh+Hot_mesh  %Only calculate permeation windows after coolant has exited the hot leg
        if PermeationFlag_primary == 2
            %Diffusion model for primary permeator. If desired, could add axial component to the permeator by using CPermp(j,i) instead of just CPermp(1,i)
            %A dynamic DT might work here, or, could run this a certain number of times within each DT from the larger thing
            %Dpermp = 0.24E-6*exp(-21.1/(0.008314*T));  %Permeator diffusion coefficient for Pd from Hara, 2012 (T is in K) [m2/s]
            %Dpermp = (3.50552E-14)*T^2 - (4.34355E-11)*T + 1.38630E-08;  %Permeator diffusion coefficient for Ni from a fit to data from Wipf, 1989 (T is in K) [m2/s]
            Dpermp = (7.43E-7)*exp(-44.1/(0.008314*T));  %H2 diffusion coefficient in Ni.  From Tanabe, 1984
            % Dpermp = 6.32E-7*exp(-47.8/(0.008314*T));  %H2 diffusion coefficient in 316 SS from tanabe
            %Assuming dissociation on the surface is virtually instant
            d_flibe = (2415.6-0.49072*T);  %[kg/m3] flibe density from Sohal, T is in K
            Molar_vol = d_flibe/(32.8931/1000);  %Molar volume of flibe [mol/m3]
            
            %Calculate salt and metal solubility constants
            k_perm_sievert = 953*exp(-10.7/(8.314*T/1000));  %[mol/m3 Ni-MPa^0.5] from Tanabe, 1984
            %                 k_perm_sievert = 427*exp(-13.9/(8.314*T/1000));  %[mol/m3 316 SS-MPa^0.5] from Tanabe, 1984
            k_HenryT2mod = (k_HenryT2*Molar_vol/0.101325);  %Convert k_HenryT2 into units of [mol T2/m3 flibe-MPa]
            
            
            %Define certain parameters prior to entering the loop
            %Assumes that the coolant mixes after exiting the HX, going through the cold leg, core, and hot leg prior to re-entering the HX
            Csbp = mt1_T2/Vol_1;  %T2 concentration in the salt bulk at beginning of calculation when salt volume element first enters the HX [moles T2/m3]
            mt1_T2_ve = Csbp*v_dot*DT;  %moles T2 in the coolant volume element of the permeator
            mt1_TF_ve = (mt1_TF/Vol_1)*v_dot*DT;  %moles TF in the coolant volume element entering the permeator
            mt1_T2_balance = mt1_T2 - mt1_T2_ve;  %Total moles of T2 in the total primary coolant inventory minus that which entered the permeator
            mt1_TF_balance = mt1_TF - mt1_TF_ve;  %Total moles of TF in the total primary coolant inventory minus that which entered the permeator
            
            moles_flibe_ve = Molar_dot*DT;  %moles of coolant flowing into the permeator in DT
            for counter = 1:1:Permeator_number_p  %If DT2 < DT, the permeator time step is shorter than the HX timestep.  Run the permeation calculations Permeator_number of times each DT
                %%%%****Csbp and mt1_T2_ve need to be updated within the loop
                for seg = 1:1:Hx_mesh %then divide the surface area and element volume by HX_mesh
                    
                    %%% Primary permeation window iteration block below %%%
                    JP_old = 0;
                    JP = 1;
                    BLP = 0.001;  %Set the salt wall concentration as a fraction of the salt bulk concentration.  If the instantaneous approx is correct, this can be set to a small number e.g. 0.001
                    Cpsw = BLP*Csbp;  %T2 concentration in the salt at the HX1 wall [mole T2/m3 flibe]
                    Cpm1_initial = CPermp(1,1); %Concentration of T2 initially in the first finite difference slice of the HX wall
                    %                     CP_initial = CPermp(1,1);  %Initial concentration of T2 in the first finite difference element of the HX wall
                    %                     Cpm1 = Cm1_bank(j,1);  %Concentration in metal at HX1 metal/salt boundary from previous time step [mole T2/m3 metal]
                    %                     Cpm2 = Cm2_bank(j,1);  %Concentration in metal at HX1 slice 1/slice2 boundary from prevoius time step [mole T2/m3 metal]
                    %Now iterate to convergence
                    for iteration = 1:1:100
                        if abs((JP_old - JP)/JP) < 1E-18
                            break  %Convergence criteria have been met. Exit the loop.
                        else
                            %Segment that solves the boundary value problem at the HX1/primary salt interface
                            %Record values for use in the next iteration, if required.
                            
                            %%%%12/5/2014 Trial 1.  Seems very nice!  Div
                            %Csw is guessed above before entering the loop
                            JP_old = JP;
                            %Calculate new Cm1 based on flux of T2 to the salt/metal interface
                            JP = k_masspipe_T2_perm1*(Csbp - Cpsw);  %T2 flux in salt to the HX1 wall [mole T2/m2-s]
                            Avgp1 = (JP*(Permp_element_sa(1,1)/Core_mesh)*(DT/Permeator_number_p))/(Permp_element_vol(1,1)) + Cpm1_initial;  %The first radial element is divided into 2.  Avg is set at the center of the first radial element
                            %                             Cm1 = Avg + (DX*k_masspipe_T2/(2*Dp))*(Csb - Csw);   %
                            %test
                            Cpm1 = Avgp1 + ((PPX)*k_masspipe_T2_perm1/(2*Dpermp))*(Csbp - Cpsw);
                            Cpsw = k_HenryT2mod*(Cpm1/k_perm_sievert)^2;
                            Csbp = ((mt1_T2_ve - JP*(Permp_element_sa(1,1)/Core_mesh)*(DT/Permeator_number_p)))/(v_dot*DT);
                            mt1_T2_Permpout = mt1_T2_ve - (JP*(Permp_element_sa(1,1)/Core_mesh)*(DT/Permeator_number_p));  %moles of T2 in the coolant volume element exiting the current segement
                            CPp_new = Avgp1;
                            Cpm2 = 2*Avgp1 - Cpm1;
                        end
                    end
                    
                    %%% Iteration block above %%%
                    
                    % %%% Primary permeation window fsolve block below %%%
                    %                                 Avgp1 = CPermp(1,i);
                    %
                    %                                 options = optimoptions('fsolve', 'Display', 'off', 'TolFun', 1E-30, 'TolX', 1E-25);
                    %                                 var1pguess = zeros(4,1);  %var0 is used as the initial guess by the MATLAB function, fsolve, called below
                    %                                 var1pguess(1,1) = Avgp1;
                    %                                 var1pguess(2,1) = 2*Avgp1;
                    %                                 Perm1_element_sa = Permp_element_sa(i,1);
                    %                                 Moles_firstpelement = CPermp(1,i)*Permp_element_vol(i,1);
                    %                                 Permp_element_volume = Permp_element_vol(i,1);
                    %
                    %                                 FP = @(var1p) permentr_primary(var1p, Dpermp, PPX, k_masspipe_T2_perm1, k_HenryT2mod, k_perm_sievert, Moles_firstpelement, Perm1_element_sa, DT, Permeator_number_p, Permp_element_volume, mt1_T2_Perm_in, v_dot);
                    %
                    %                                 [var1p_out] = fsolve(FP, var1pguess, options);
                    %                                 CPermp(1,i) = var1p_out(1,1);
                    %                                 Avgp1 = var1p_out(1,1);
                    %                                 CPp_new = Avgp1;
                    %                                 Cpm1 = var1p_out(2,1);
                    %                                 Cpsw = var1p_out(3,1);
                    %                                 Cpsb = var1p_out(4,1);
                    %                                 Cpm2 = 2*Avgp1 - Cpm1;
                    %                                 mt1_T2_Permpout = mt1_T2_Perm_in - (k_masspipe_T2_perm1*(Cpsb - Cpsw)*Perm1_element_sa(i,1)*DT/Permeator_number_p);  %moles of T2 in the coolant volume element exiting the current segement j
                    % %%% Fsolve block above %%%
                    
                    mt1_T2_ve = mt1_T2_Permpout;  %Redefine mt1_T2_ve (moles of T2 in the current coolant volume element) for next step through the "for seg = 1:1:Core_mesh" loop
                    CPermp(1,1) = CPp_new;  %Average concentration of T2 in the first segment [moles T2/m3]
                    
                    %Balance the redox after each step through the loop
                    if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
                        C_total_local =  mt1_TF_ve/moles_flibe_ve + 2*mt1_T2_ve/moles_flibe_ve; %[Mole of T atoms/Mole of flibe]
                        C_T2_local = (4*C_total_local*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total_local*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
                            +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
                        C_TF_local = C_total_local -2*C_T2_local;        %[mole TF/mole flibe] mole fraction TF in flibe
                        mt1_T2new = C_T2_local*moles_flibe_ve - mt1_T2_ve;  %[Change in moles of T2 in primary] due to generation in flibe and thermodynamic feedback
                        mt1_TFnew = C_TF_local*moles_flibe_ve - mt1_TF_ve;  %[Change in moles TF in primary] due to generation in flibe and thermodynamic feedback
                        mt1_T2_ve = mt1_T2_ve + mt1_T2new;  %Current T2 in current volume element [mole T2]
                        Csbp = mt1_T2_ve/(v_dot*DT);
                        mt1_TF_ve = mt1_TF_ve + mt1_TFnew;  %Current TF in current volume element [mole T+]
                    else
                    end
                end
                
                Tfluxp1 = (Dpermp/PPX)*(CPermp(1,1) - CPermp(1,2));  %Flux of T2 out of (j,1) into (j,2)
                Moles_outof_pone = Tfluxp1*(Permp_element_sa(2,1))*(DT/Permeator_number_p);  %Moles of T2 out of the first radial HX wall segment into the second radial segment
                CPermp(1,1) = CPermp(1,1) - Moles_outof_pone/(Permp_element_vol(1,1));  %[mole T2/m3] New average concentration of T2 in the first segment after accounting for influx and outflux
                
                
                
                for i = 2:1:PermElements   %Go through each radial slice (i) of the permeator tube wall thickness
                    if i >=2 && i < PermElements
                        if i == 2
                            Tflux_inp = Tfluxp1;
                            Tflux_outp = (CPermp(1,i)-CPermp(1,i+1))*Dpermp/PPX;  %[mole T2/m2-sec] flux out of CPermp(1,i) to CP(1,i+1)
                        else
                            Tflux_outp = (CPermp(1,i)-CPermp(1,i+1))*Dpermp/PPX;  %[mole T2/m2-sec] flux out of CPermp(1,i) to CPermp(1,i+1)
                        end
                        CPermp(1,i) = (CPermp(1,i)*Permp_element_vol(i,1) + Tflux_inp*(Permp_element_sa(i,1))*(DT/Permeator_number_p) - Tflux_outp*(Permp_element_sa(i+1,1))*(DT/Permeator_number_p))/(Permp_element_vol(i,1));
                        Tflux_inp = Tflux_outp;  %The flux out of i into i+1 must equal the flux into i+1 from i
                    elseif i == PermElements
                        CPermp(1,i) = (CPermp(1,i)*Permp_element_vol(i,1) + Tflux_inp*(Permp_element_sa(i,1))*(DT/Permeator_number_p))/(Permp_element_vol(i,1));
                        Tflux_outp = ((CPermp(1,i)-0)/PPX)*Dpermp;  %Flux out of CPermp(j,PermElements) into the permeator removal side which is assumed to have a T2 concentration of zero[moles T2/m2-sec]
                        CPermp(1,i) = (CPermp(1,i)*Permp_element_vol(i,1) - Tflux_outp*(Permp_element_sa(i+1,1))*(DT/Permeator_number_p))/(Permp_element_vol(i,1));  %Concentration of T2 in the final radial slice of the permeator tube
                        Permeator_p_total = Permeator_p_total + Tflux_outp*(Permp_element_sa(i+1,1))*(DT/Permeator_number_p);  %[moles T2] total moles of T2 removed from the coolant through the permator
                    end
                end
            end
            C_T2_permeatorexit_p = mt1_T2_ve/(v_dot*DT);  %[mol T2/m3] Concentration of T2 in coolant exiting the primary permeator
            mt1_T2 = mt1_T2_balance + mt1_T2_ve;          %calculate total moles of T2 in primary
            mt1_TF = mt1_TF_balance + mt1_TF_ve;          %calculate total moles of TF in primary
        else
            %Permeation windows not applied
        end
        
        %           Before moving on from the primary permeator, balance the redox assuming full mixing again
        if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
            C_total =  mt1_TF/Mole_flibe + 2*mt1_T2/Mole_flibe; %[Mole of T atoms/Mole of flibe]
            C_T2 = (4*C_total*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
                +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
            C_TF = C_total -2*C_T2;        %[mole TF/mole flibe] mole fraction TF in flibe
            mt1_T2new = C_T2*Mole_flibe - mt1_T2;  %[Change in moles of T2 in primary] due to generation in flibe and thermodynamic feedback
            mt1_TFnew = C_TF*Mole_flibe - mt1_TF;  %[Change in moles TF in primary] due to generation in flibe and thermodynamic feedback
            mt1_T2 = mt1_T2 + mt1_T2new;  %Current T2 in primary [mole T2]
            mt1_TF = mt1_TF + mt1_TFnew;  %Current TF in primary [mole T+]
            %mt1_total = 2*mt1_T2 + mt1_TF;  %Current total moles of tritium in the primary loop
            
        else
        end
    else
        %j not equal to Core_mesh + Hot_mesh
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Calculate Gas stripping in the primary coolant %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if j == Core_mesh+Hot_mesh  %Only calculate stripping after the coolant has exited the hot leg
        if GasStrippingFlag == 2 || GasStrippingFlag == 4  %If GasStrippingFlag = 2 or 4 then do stripping calcs for primary system.  2 = Stripping in primary system only.  4 = Stripping in both primary system and Secondary system
            mt_TF = mt1_TF;  %Give a generic name for the moles of TF in the coolant
            mt_T2 = mt1_T2;  %Give a generic name for the [moles of T2] in the coolant
            Mole_coolant = Mole_flibe;  %[moles coolant] Generic name for moles of coolant in the system
            Flibe_MM = 32.8931;     %Molar mass of flibe with 99.995% Li-7 [g/mol] from doing 0.67*MM_LiF + 0.33*MM_BeF2
            m_dotm = m_dot*(1000/Flibe_MM)*StrippingFlowFraction_p;  %Convert primary coolant flow rate from [kg/s] to [mole/s] and multiply by the fraction of the primary flow that is diverted to the stripper
            NStages = NStages_p;  %Number of stages for use in the primary gas stripper
            G = G_p;  %Stripping gas flow rate [mole/s] in the primary stripper
            Loop_Selector = 1;  %Select primary loop
            [mt_T2, mt_TF, y_1_T2, y1_TF, mole_T2_offgas, x_N_TF, mole_TF_offgas] = GasStrippingModule(NStages, T, T_avg, ...
                Mole_coolant, m_dotm, mt_TF, mt_T2, G, DT, Loop_Selector);  %Call out to gas stripping function
            mt1_TF = mt_TF;  %Update moles of TF in primary coolant after stripping
            mt1_T2 = mt_T2;  %Update moles of T2 in primary coolant after stripping
            y1_TFp = y1_TF;  %Concentration of TF in exiting gas stream from primary stripper
            y_1_T2_p = y_1_T2;  %Concentration of T2 in exiting gas from the primary stripper [mole T2/mole gas]
            mole_T2_poffgas = mole_T2_poffgas + mole_T2_offgas;  %[mole T2] cummulative moles of T2 captured via stripping in the primary system
            mole_TF_poffgas = mole_TF_poffgas + mole_TF_offgas;  %[mole TF] cummulative moles of TF removed form primary coolant via stripping
            x_N_TF_p = x_N_TF;  %Concentration of TF in the coolant exiting the stripper in the primary system [mole TF/mole flibe]
            T2_PrimaryConc = mt1_T2/Mole_flibe;  %Concentration of T2 in coolant exiting the stripper [mole T2/mole flibe]
            
        else
            %Gas stripping not performed
        end
        %Before moving on from the stripper, balance the redox if fixed redox is selected
        if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
            C_total =  mt1_TF/Mole_flibe + 2*mt1_T2/Mole_flibe; %[Mole of T atoms/Mole of flibe]
            C_T2 = (4*C_total*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
                +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
            C_TF = C_total -2*C_T2;        %[mole TF/mole flibe] mole fraction TF in flibe
            mt1_T2new = C_T2*Mole_flibe - mt1_T2;  %[Change in moles of T2 in primary] due to generation in flibe and thermodynamic feedback
            mt1_TFnew = C_TF*Mole_flibe - mt1_TF;  %[Change in moles TF in primary] due to generation in flibe and thermodynamic feedback
            mt1_T2 = mt1_T2 + mt1_T2new;  %Current T2 in primary [mole T2]
            mt1_TF = mt1_TF + mt1_TFnew;  %Current TF in primary [mole T+]
            %mt1_total = 2*mt1_T2 + mt1_TF;  %Current total moles of tritium in the primary loop
        else
        end
    else
        %j not equal to Core_mesh + Hot_mesh
    end
    
    mt1_total = mt1_TF + 2*mt1_T2;  %[mole tritium]  total moles of tritium in the primary system
    C_T2 = mt1_T2/Mole_flibe;  %Current concentration of T2 in the primary [mole T2/mole Flibe]
    C_TF = mt1_TF/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
    PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
    PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
    if PP_T2 <= 0
        PP_T2 = 1E-40;  %If no T2 in the system yet, set it very small so that a potential can be calculated below
    else
    end
    Redox_Potential = 2*0.00831446*T*log(PP_TF/sqrt(PP_T2))+2*(-4.6976E-10*T^3 + 3.1425E-6*T^2 - 8.8612E-3*T - 2.7305E2);  %[kJ/mol F2] Temp in [K] Calculate redox fluorine potential at the current axial location [kJ/mol F2]
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% PRIMARY HEAT EXCHANGER %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
elseif j >= Core_mesh + Hot_mesh +1 && j <= Core_mesh + Hot_mesh + Hx_mesh
    %  1.  Corrosion by TF, deposition of CrF2
    %  2.  T2 diffusion through Hx
    % T2 diffusion in the primary heat exchanger
    
    for i = 1:1:Elements   %Within each axial segment (j), go through each radial slice (i) of the tube wall thickness
        
        %Assuming dissociation on the surface is virtually instant
        CoeffA = (HX1_element_vol(i,1)/Hx_mesh);
        CoeffB = v_dot*DT;  %Volume of coolant to flow through the Hx axial element within time increment DT
        d_flibe = (2415.6-0.49072*T);  %[kg/m3] flibe density from Sohal, T is in K
        Molar_vol = d_flibe/(32.8931/1000);  %Molar volume of flibe [mol/m3]
        
        %Calculate salt and metal solubility constants
        k_sievert = 427*exp(-13.9/(8.314*T/1000));  %[mol/m3 316 ss-MPa^0.5]
        k_HenryT2_mod = (k_HenryT2*Molar_vol/0.101325);  %Convert k_HenryT2 from [mol T2/mol flibe-atm] into units of [mol T2/m3 flibe-MPa]
        
        if i == 1
            if j == Core_mesh + Hot_mesh + 1
                Csb = mt1_T2/Vol_1;  %T2 concentration in the salt bulk at beginning of calculation when salt volume element first enters the HX [moles T2/m3]
                %Assumes that the coolant mixes after exiting the HX, going through the cold leg,
                %core, and hot leg prior to re-entering the HX
                mt1_TF_initial = mt1_TF;  %Total moles of TF currently in the total primary coolant inventory
                mt1_T2_initial = mt1_T2;  %Total moles of T2 currently in the total primary coolant inventory
                mt1_TF_HXin = (mt1_TF/Vol_1)*CoeffB;  %moles of TF that flow into the HX1 volume element in step of size DT
                mt1_T2_HXin = Csb*CoeffB; %moles T2 that flow into the HX1 volume element in time step of size DT
                C_Cr_HXactual = C_Cr_actual;  %[mol/m3] Concentration of Cr in coolant entering the HX at j = Core_mesh + Hot_mesh + 1
                %                         MolesCr_HX = C_Cr_HXactual*CoeffB;  %moles of Cr to flow into the coolant volume element
            else
                Csb = Csb_next;  %Csb_next is calculated from j = j - 1. Set the T2 bulk concentration for j > Core_mesh + Hot_mesh + 1
                mt1_T2_HXin = mt1_T2_HXout;  %Set the entering moles of T2 in the coolant volume element using the value from the previous j segment
                mt1_T2_initial = mt1_T2;  %Total moles of T2 in the total primary coolant inventory
                %                         Cv_TF_HX = mt1_TF_HX/CoeffB;  %C_TF_HX_next is calculated from j = j - 1. Set the TF bulk concentration for j > Core_mesh + Hot_mesh + 1
                mt1_TF_HXin = mt1_TF_HXout;  %moles TF in the current coolant volume element currently in HX1
                mt1_TF_initial = mt1_TF;  %moles TF in the total primary coolant inventory
            end
       %%%Primary HX iteration block below%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %Prior to entering iteration loop below, set all old values to zero because they are only used to judge convergence
            J1_old = 0;
            J1 = 1;
            BL = 0.001;  %Set the salt wall concentration as a fraction of the salt bulk concentration.  If the instantaneous approx is correct, this can be set to a small number e.g. 0.001
            Csw = BL*Csb;  %T2 concentration in the salt at the HX1 wall [mole T2/m3 flibe]
            Cm1_initial = CP(j,1); %Concentration of T2 initially in the first finite difference slice of the HX wall
            CP_initial = CP(j,1);  %Initial concentration of T2 in the first finite differene element of the HX wall
            Cm1 = Cm1_bank(j,1);  %Concentration at HX1 metal/salt boundary from previous time step [mole T2/m3 metal]
            Cm2 = Cm2_bank(j,1);  %Concentration at HX1 slice 1/slice2 boundary from prevoius time step [mole T2/m3 metal]
            for iteration = 1:1:100
                if abs((J1_old - J1)/J1) < 1E-15
                    break  %Convergence criteria have been met. Exit the loop.
                else
                    %Segment that solves the boundary value problem at the HX1/primary salt interface
                    %Record values for use in the next iteration, if required.
                    
                    %Csw is guessed above before entering the loop
                    J1_old = J1;
                    %Calculate new Cm1 based on flux of T2 to the salt/metal interface
                    J1 = k_masspipe_T2*(Csb - Csw);  %T2 flux in salt to the HX1 wall [mole T2/m2-s]
                    Avg = (J1*HX1_element_sa(i,1)*DT/Hx_mesh)/(CoeffA) + Cm1_initial;  %The first radial element is divided into 2.  Avg is set at the center of the first radial element
                    %                             Cm1 = Avg + (DX*k_masspipe_T2/(2*Dp))*(Csb - Csw);   %
                    %test
                    Cm1 = Avg + ((DX*PRFLoops1)*k_masspipe_T2/(2*Dp))*(Csb - Csw);
                    Csw = k_HenryT2_mod*(Cm1/k_sievert)^2;
                    Csb = ((mt1_T2_HXin - J1*HX1_element_sa(i,1)*DT/Hx_mesh))/CoeffB;
                    mt1_T2_HXout = mt1_T2_HXin - (J1*HX1_element_sa(i,1)*DT/Hx_mesh);  %moles of T2 in the coolant volume element exiting the current segement j
                    CP_new = Avg;
                    Cm2 = 2*Avg - Cm1;
                    
                end
            end
       %%%Iteration block above%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
       %%%Primary HX fsolve block below%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                     Avg1 = CP(j,i);
            %                     options = optimoptions('fsolve', 'Display', 'off', 'TolFun', 1E-30, 'TolX', 1E-20);
            % %                     var1a = zeros(4,1);
            % %                     var1a = ones(4,1)*Avg1/2000;  %var0 is used as the initial guess by the MATLAB function, fsolve, called below
            %                     var1a(1,1) = Avg1;
            %                     var1a(2,1) = 2*Avg1;
            %                     var1a(3,1) = Csb;
            %                     var1a(4,1) = 0;
            %                     HX1_element_sa1a = HX1_element_sa(i,1);
            %                     Moles_firstelement = CP(j,1)*CoeffA;
            %
            %                     F1a = @(var1a) HX1entr(var1a, Dp, DX, k_masspipe_T2, k_HenryT2_mod, k_sievert, Moles_firstelement, HX1_element_sa1a, DT, Hx_mesh, CoeffA, mt1_T2_HXin, v_dot);
            %
            %                     [var1a] = fsolve(F1a, var1a, options);
            %                     CP(j,i) = var1a(1,1);
            %                     Avg1 = var1a(1,1);
            %                     CP_new = Avg1;
            %                     Cm1 = var1a(2,1);
            %                     Csw = var1a(3,1);
            %                     Csb = var1a(4,1);
            %                     Cm2 = 2*Avg1 - Cm1;
            %                     mt1_T2_HXout = mt1_T2_HXin - (k_masspipe_T2*(Csb - Csw)*HX1_element_sa(i,1)*DT/Hx_mesh);  %moles of T2 in the coolant volume element exiting the current segement j
       %%%Fsolve block above%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            Csb_next = Csb;  %[mole T2/m3] Bulk concentration of T2 for use when j > Core_mesh + Hot_mesh + 1
            Cm1_bank(j,1) = Cm1;  %Concentration at HX1 metal/salt boundary [mole T2/m3 metal]
            Cm2_bank(j,1) = Cm2;  %Concentration at HX1 slice 1/slice2 boundary [mole T2/m3 metal]
            
            %                     Check = (mt1_T2_HXin - (CP_new - CP_initial)*CoeffA - mt1_T2_HXout);  %this should be zero regardless of the iteration
            mt1_T2 = mt1_T2_initial - (mt1_T2_HXin - mt1_T2_HXout);  %New total moles of T2 in the primary system
            
            CP(j,1) = CP_new;  %Average concentration of T2 in the first radial segment of the HX1 metal wall [moles T2/m3]
            %                     Tflux1 = (Dp/DX)*(CP(j,1) - CP(j,2));  %Flux of T2 out of (j,1) into (j,2)
            %test
            Tflux1 = (Dp/(DX*PRFLoops1))*(CP(j,1) - CP(j,2));  %Flux of T2 out of (j,1) into (j,2)
            Moles_outof_one = Tflux1*(HX1_element_sa(i+1,1)/Hx_mesh)*DT;  %Moles of T2 out of the first radial HX wall segment into the second radial segment
            CP(j,1) = CP(j,1) - Moles_outof_one/(HX1_element_vol(i,1)/Hx_mesh);  %[mole T2/m3] New average concentration of T2 in the first segment after accounting for influx and outflux
            
            
            
            
        elseif i >=2 && i < Elements
            if i == 2
                Tflux_in = Tflux1;  %[moles T2/m2-s] Flux from j,i = 1 into j,i = 2
                %                         Tflux_out = (CP(j,i)-CP(j,i+1))*Dp/DX;  %[mole T2/m2-sec] flux out of CP(j,2) to CP(j,3)
                %TEST
                Tflux_out = (CP(j,i)-CP(j,i+1))*Dp/(DX*PRFLoops1);  %[mole T2/m2-sec] flux out of CP(j,2) to CP(j,3)
            else
                %                         Tflux_out = (CP(j,i)-CP(j,i+1))*Dp/DX;  %[mole T2/m2-sec] flux out of CP(j,i>2) to CP(j,i>2+1)
                %TEST
                Tflux_out = (CP(j,i)-CP(j,i+1))*Dp/(DX*PRFLoops1);  %[mole T2/m2-sec] flux out of CP(j,i>2) to CP(j,i>2+1)
            end
            CP(j,i) = (CP(j,i)*CoeffA + Tflux_in*(HX1_element_sa(i,1)/Hx_mesh)*DT - Tflux_out*(HX1_element_sa(i+1,1)/Hx_mesh)*DT)/CoeffA;
            Tflux_in = Tflux_out;  %The flux out of i into i+1 must equal the flux into i+1 from i
            
            
        elseif i == Elements
            if Loops == 2
                CP(j,i) = (CP(j,i)*CoeffA + Tflux_in*(HX1_element_sa(i,1)/Hx_mesh)*DT)/CoeffA;  %Calculate the concentration in CP(j,i) due to an influx from CP(j,i-1) prior to entering the iteration scheme below
                %The flux out of CP(j,i) to the secondary coolant is calculated in the next block below
            else  %If Loops = 1, only one loop is being simulated, calculate losses to the power cycle
                CP(j,i) = (CP(j,i)*CoeffA + Tflux_in*(HX1_element_sa(i,1)/Hx_mesh)*DT)/CoeffA;
                Tflux_out1 = (CP(j,i) - 0)*Dp/(0.5*DX*PRFLoops1);  %Flux of T2 out into the power cycle if Loops = 1
                CP(j,i) = (CP(j,i)*CoeffA - Tflux_out1*(HX1_element_sa(i+1,1)/Hx_mesh)*DT)/CoeffA;
                CPC = CPC+(Tflux_out1*HX1_element_sa(i+1,1)*DT/Hx_mesh);
                mt3 = CPC; %mole T2 released to power cycle
            end
            
        end
    end
    
    PP_T2_HX_primary = ((CP(j,1)/Sievert_316_primary)^2)*9.86923267;  %[atm] Partial pressure of T2 over the metal required to give the concentration Cp at the surface.  Sieverts' law constant comes from Tanabe, 1984.
    %Note: T2 transport through the secondary heat exchanger is done as the last calculation in the time step.  It is done right after the cold leg calculation.  See the section
    %below for the Cold Leg.
    
    
    
    if Loops == 2  %If the system being simulated has 2 loops, calculate permeation into the secondary loop
        %After calculating transport into HX1 for j = Core_mesh + Hot_mesh
        %+ 1 through j = Core_mesh + Hot_mesh + HX_mesh, now calculate flux OUT
        %of HX1 into secondary coolant.  Secondary coolant flows in
        %opposite direction from the primary.
        if j == Core_mesh + Hot_mesh + Hx_mesh
            i = Elements;  %Select final HX radial element
            j=0;
            for j = Core_mesh + Hot_mesh + Hx_mesh:-1:Core_mesh + Hot_mesh + 1
                if CP(j,i) <= 0
                    break
                else
                    CoeffA = (HX1_element_vol(i,1)/Hx_mesh);
                    Avg2 = CP(j,i);
                    mt2_in = mt2;  %Moles of T2 in the entire secondary loop
                    
                    if j == Core_mesh + Hot_mesh + Hx_mesh
                        Csb2 = (mt2/Vol_2);  %[mole T2/m3] Concentration of T2 in the secondary coolant when it first enters the HX1.  Assumes complete mixing between exiting HX1 and returning ot the entrance of HX1
                    else
                        %Use Csb2 determined from the previous, j-1, calculation
                    end
                    mt2_involel = Csb2*(v_dot2*DT);  %[mole T2] moles of T2 initially in secondary coolant volume-element
                    Moles_finalelement = Avg2*CoeffA;
                    %%% Trial 5 use fsolve 12/8/2014.  Appears to work fine!!
                    %options = optimoptions('fsolve', 'Algorithm', 'levenberg-marquardt', 'Display', 'off', 'TolFun', 1E-30, 'TolX', 1E-10);  %For use with over-constrained problem...Options for fsolve output
                    
                    options = optimoptions('fsolve', 'Display', 'off', 'TolFun', 1E-30, 'TolX', 1E-15);
                    var0 = ones(4,1)*Avg2/2000;  %var0 is used as the initial guess by the MATLAB function, fsolve, called below
                    var0(1,1) = Avg2;
                    var0(2,1) = 0.333*Avg2;
                    HX1_element_sa1 = HX1_element_sa(i+1,1);
                    F1 = @(var1) HX1exit(var1, Dp, DX, k_transp2, k_henryT22, k_sievert, Moles_finalelement, HX1_element_sa1, DT, Hx_mesh, CoeffA, mt2_involel, v_dot2);
                    [var] = fsolve(F1, var0, options);
                    CP(j,i) = var(1,1);
                    Csm2 = var(2,1);
                    Csw2 = var(3,1);
                    Csb2 = var(4,1);
                    mt2 = mt2_in + Csb2*v_dot2*DT - mt2_involel; %update moles of T2 in secondary coolant
                    
                end
                
            end
            
        else
        end
    else
    end
    
    % Consider corrosion/deposition and generation of T2 from corrosion
    % etc.  Call out to corrosion module if corrosion is deemed to occur
    % need to keep tabs on c1b and c1a and mt1_T2 and mt1_TF.
    %Currently assumes that T2 transport occurs before corrosion deposition.  Iin reality, both should occur simultaneously.
    if Corrosionflag == 1      %Corrosion is off
        mt1_TF_HXout = mt1_TF_HXin;
        mt1_TF = mt1_TF_initial;
    elseif Corrosionflag == 2  %Corrosion is on
        if Feedbackflag == 1||Feedbackflag == 3   %Calculate the current ratio of TF to T2 in the salt
            C_TF = mt1_TF_HXin/(Molar_dot*DT);  %Concentration TF [mol TF/mol salt]
            C_T2 = ((Csb*CoeffB)/(Molar_dot*DT)); %Conccentration of T2 [mol T2/mol salt]
            PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
            PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
            if PP_T2 <= 0  %If no T2 is in the system yet, set a very small value so that a Ratio_TF_T2 can be calculated below
                PP_T2 = 1E-40;
            else
            end
            Ratio_TF_T2 = PP_TF/sqrt(PP_T2);  %Calculate current ratio of TF to T2
        else
        end
        dens_flibe = (2415.6-0.49072*T)*1000;  %Flibe density from Janz correlation in Sohal, 2010 [g/m^3]
        Cr_MM = 51.9961;  %Cr molar mass [g/mol]
        Flibe_MM = 32.8931;  %Molar mass of flibe [g/mol]
        C_Cr_metal = Mole_frac_metal(1,1);  %Mole fraction of Cr in the base metal [mol Cr/mol Stainless]
        C_Cr_eq = (((Cr_MM/Flibe_MM)*1E6)*10^(-5.12+((9.06E3)/T)+log10(C_Cr_metal)+log10(Ratio_TF_T2^2)))*(1E-6)*(Vol_1*dens_flibe/Cr_MM)*(1/Vol_1);  %Calculate equilibrium Cr concentration given the base metal [moles Cr/m3 coolant]
        %Record old values prior to updating them below
        mt1_T2_old = mt1_T2;
        mt1_T2_HXout_old = mt1_T2_HXout;
        mt1_TF_old = mt1_TF;
        mt1_TF_HXout_old = mt1_TF_HXout;
        Moles_Cr_ppt_old = Moles_Cr_ppt(j,1);
        C_Cr_HXactual_old = C_Cr_HXactual;
        C_Cr_actual_old = C_Cr_actual;
        if C_Cr_HXactual - C_Cr_eq > 0  %If yes, then do preciptation of Cr
            J_ppt_pipe = k_Cr_masspipe*(C_Cr_HXactual - C_Cr_eq); %Flux of dissolved Cr to reflector surface [mol Cr2+/m2-s]
            New_moles_Cr_ppt = (J_ppt_pipe*pipe_sa(j,1))*DT;  %Moles Cr deposited in the current timestep at the current axial position
            if New_moles_Cr_ppt > C_Cr_HXactual*(v_dot*DT)  %Ensure that amount of Cr precipitated from the current time step does not exceed the amount of Cr presently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + C_Cr_HXactual*(v_dot*DT);  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - C_Cr_HXactual*(v_dot*DT);   %Update total moles of T2 in primary
                mt1_T2_HXout = mt1_T2_HXout - C_Cr_HXactual*(v_dot*DT);   %Update moles of T2 in HX coolant element
                mt1_TF = mt1_TF + 2*C_Cr_HXactual*(v_dot*DT); %Update total moles of TF in primary
                mt1_TF_HXout = mt1_TF_HXin + 2*C_Cr_HXactual*(v_dot*DT); %Update moles of TF in HX coolant element
                C_Cr_actual = (C_Cr_actual*Vol_1 - C_Cr_HXactual*(v_dot*DT))/Vol_1;
                C_Cr_HXactual = 0;  %[mol Cr/m3] concentration of Cr dissolved in primary coolant
                if mt1_T2_HXout < 0  %Check that the amount of available T2 is not exceeded
                    mt1_T2_HXout = 0;  %The T2 in that volume segment has been depleted
                    Moles_Cr_ppt(j,1) = Moles_Cr_ppt_old + mt1_T2_HXout_old;
                    mt1_T2 = mt1_T2_old - mt1_T2_HXout_old;
                    mt1_TF = mt1_TF_old + 2*mt1_T2_HXout_old;
                    mt1_TF_HXout = mt1_TF_HXout_old + 2*mt1_T2_HXout_old;
                    C_Cr_actual = (C_Cr_actual_old*Vol_1 - mt1_T2_HXout_old)/Vol_1;
                    C_Cr_HXactual = (C_Cr_HXactual_old*(v_dot*DT) -  mt1_T2_HXout_old)/(v_dot*DT);
                else
                end
            else  %If amount of Cr preciptated from the current time step is less than the total amount of Cr currently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + New_moles_Cr_ppt;  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - New_moles_Cr_ppt;   %Update total moles of T2 in primary
                mt1_T2_HXout = mt1_T2_HXout - New_moles_Cr_ppt;  %Update moles of T2 in HX coolant element
                mt1_TF = mt1_TF + 2*New_moles_Cr_ppt; %Update total moles of TF in primary
                mt1_TF_HXout = mt1_TF_HXin + 2*New_moles_Cr_ppt; %Update moles of TF in HX coolant element
                C_Cr_actual = C_Cr_actual - New_moles_Cr_ppt/Vol_1;  %Average concentration of Cr throughout the primary coolant [mole Cr/m3]
                C_Cr_HXactual = C_Cr_HXactual - New_moles_Cr_ppt/(v_dot*DT);  %New concentration of Cr in coolant [mol Cr/m3]
                if mt1_T2_HXout < 0  %Check that the amount of available T2 is not exceeded
                    mt1_T2_HXout = 0;  %The T2 in that volume segment has been depleted
                    Moles_Cr_ppt(j,1) = Moles_Cr_ppt_old + mt1_T2_HXout_old;
                    mt1_T2 = mt1_T2_old - mt1_T2_HXout_old;
                    mt1_TF = mt1_TF_old + 2*mt1_T2_HXout_old;
                    mt1_TF_HXout = mt1_TF_HXout_old + 2*mt1_T2_HXout_old;
                    C_Cr_actual = (C_Cr_actual_old*Vol_1 - mt1_T2_HXout_old)/Vol_1;
                    C_Cr_HXactual = (C_Cr_HXactual_old*(v_dot*DT) -  mt1_T2_HXout_old)/(v_dot*DT);
                else
                end
            end
            GramsCr_persa(j,1) = (Moles_Cr_ppt(j,1)*Cr_MM/(pipe_sa(j,1)));  %Total grams of Cr deposited per unit surface area at the particular axial location [g Cr/m2]
            %Before moving on, adjust T2 vs TF depending on redox option
            
        elseif C_Cr_HXactual - C_Cr_eq < 0   %If this is true, then do corrosion
            Cr_dissolvedHX = C_Cr_HXactual*(v_dot*DT);  %[moles Cr2+ in coolant element in HX]
            Cr_dissolved = C_Cr_actual*Vol_1;  %[moles Cr2+ in primary coolant]
            Conc_TF = mt1_TF_HXin/(v_dot*DT);  %[mole TF/m3 flibe] current concentration of TF coolant element in HX
            VolHX = v_dot*DT;
            corrosion_time(j,1) = corrosion_time(j,1) + DT;
            %need to read in k_masspipe to corrosion module
            [mt1_TF_HXout, mt1_T2_HXout, Cr_dissolved, C_Cr_coolantHX, TF_consumed, Cr_coolant, T2_generated, Cr_profile, mt1_TF, mt1_T2, ...
                Conc_slice, grad, Crperslice, initial, Vol_surf, Totmatomsurf, surface, Crcorrodedaxial] = CorrosionModuleHX(j, initial, surface, Conc_TF, k_masspipe_TF, Vol_surf, ...
                pipe_sa, pipe_d, pipe_l, pipe_d2, pipe_l2, Hx_tube_od, VolHX, Vol_1, Thick, DT, Slice_vol, Crperslice, ...
                Cr_dissolvedHX, TF_consumed, Cr_profile, slices1, slices2, slice_thick1, slice_thick2, Conc_slice, ...
                Conc_bulk, Totmatomsurf, grad, Lattice_param, mt1_TF, mt1_T2, T, Core_mesh, Hot_mesh, Hx_mesh, Cold_mesh, ...
                Length1_tot, GBflag, Crcorrodedaxial, Cr_dissolved,  mt1_T2_HXout, mt1_TF_HXin, t, Surf_area_gb);  %Call out to corrosion module
            C_Cr_HXactual = C_Cr_coolantHX;  %[moles Cr/m3]
            C_Cr_actual = Cr_coolant;
        end
    end
    
    if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
        C_total_HX =  (mt1_TF_HXout + 2*mt1_T2_HXout)/(Molar_dot*DT); %[Mole of T atoms/Mole of flibe in current volume element]
        C_T2 = (4*C_total_HX*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total_HX*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
            +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
        C_TF = C_total_HX -2*C_T2;        %[mole TF/mole flibe] mole fraction TF in flibe
        DeltaT2 = C_T2*Molar_dot*DT - mt1_T2_HXout;  %change in moles of T2 after redox balancing
        DeltaTF = C_TF*Molar_dot*DT - mt1_TF_HXout;  %change in moles of TF after redox balancing
        mt1_T2_HXout = C_T2*Molar_dot*DT;  %[mole T2] in coolant element exiting the current HX axial element j
        mt1_TF_HXout = C_TF*Molar_dot*DT;  %[mole TF] in coolant element exiting the current HX axial element j
        mt1_T2 = mt1_T2 + DeltaT2;  %Current total T2 in primary [mole T2]
        mt1_TF = mt1_TF + DeltaTF;  %Current total TF in primary [mole T+]
    else
    end
    mt1_total = mt1_TF + 2*mt1_T2;  %[mole tritium]  total moles of tritium in the primary system
    C_T2 = mt1_T2/Mole_flibe;  %Current concentration of T2 in the primary [mole T2/mole Flibe]
    C_TF = mt1_TF/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
    PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
    PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
    if PP_T2 <= 0  %If no T2 is in the system yet, set a very small value so that a Ratio_TF_T2 can be calculated below
        PP_T2 = 1E-40;
    else
    end
    Redox_Potential = 2*0.00831446*T*log(PP_TF/sqrt(PP_T2))+2*(-4.6976E-10*T^3 + 3.1425E-6*T^2 - 8.8612E-3*T - 2.7305E2);  %[kJ/mol F2] Temp in [K] Calculate redox fluorine potential at the current axial location [kJ/mol F2]
    
elseif j >= Core_mesh + Hot_mesh + Hx_mesh +1 && j<= Core_mesh + Hot_mesh + Hx_mesh + Cold_mesh  %%% COLD LEG %%%
    if Loops == 2  %If Loops = 2, two loops are being simulated.  If Loops = 1, only one loop is simulated.
        %%%%% If j = n_axial, first do tritium transport through the secondary heat exchanger     %%%%%
        %%%%% Calculation of tritium transport and gas stripping in the secondary heat exchanger  %%%%%
        % If gas stripping is activated, gas stripping is performed before any tritium transport calculations for the the secondary heat exchanger are performed
        if j == n_axial  %Do packed bed of graphite tritium capture, permeation window, and gas stripping in the secondary loop and tritium transport through the secondary heat exchanger as the final calculation in the time step
            
            %%%Tritium uptake on packed bed of graphite in secondary system
            if Tritiumcapturebedflag_s == 1
            else %Tritiumcpaturebedflag_2 = 2 and capture is modeled
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%% Capture T2 on graphite bed %%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                C2_T2 = mt2/Vol_2;  %Current concentration of T2 in the primary [mole T2/m3 flinak secondary salt]
                PP2_T2 = (C2_T2/k_henryT22)*1E6;  %Partial pressure of T2 above the melt [Pa]
                %Tgraphite_T2 = ((((0.00019)*sqrt(PP2_T2)*exp(19/(0.00831446*T_avg)))*100)/(8314.46*273.15));  %Capacity, [Moles T2 per gram graphite] at T[K] and P[Pa]. From Atsumi, 1988
                %Tgraphite_T2 = (1.118943393*(PP_T2/1000)^(0.36582176) + 2.714730971*((7.032151715*PP_T2/1000)/(1 + 7.032151715*PP_T2/1000)))*(4.4643e-5);
                Tgraphite_T2 = (5.618912575*((2.453167518*PP_T2/1000)^(1/1.75862069))/(1 + (2.453167518*PP_T2/1000)^(1/1.75862069)))*(4.4643e-5); % ovc 4x8 capacity [moles T2 per gram graphite Pressure in Pa] Langmuir Equation

                
                mtgbed2_T2 = mtgbed2_T2*(1-Bed_frac_rep_s*DT);  %Bed_frac_rep is the fraction of the bed graphite that is replaced per second by fresh graphite. If Bed_frac_rep = 0, then no replacement occurs.
                
                if mtgbed2_T2 < Tgraphite_T2*Bed_mass_s  %If moles of T2 on grahpite in the absorption bed has not been saturated, do transport
                    mt2_old = mt2;  %Record old moles of T2 in the primary coolant
                    mtgbed2_T2_old = mtgbed2_T2;  %Record old moles of T2 on the bed graphite
                    jgbed_T2 = Bed_k_masspebble_T2_temp_s*((mt2/Vol_2) - (Surf_Frac*mt2/Vol_2));
                    mtgbed2_T2 = mtgbed2_T2 + jgbed_T2*(Bed_surface_area_s)*DT;  %Calculate new total moles of T2 adsorbed onto bed graphite [mol] assuming all T2 reaching graphite surface is adsorbed
                    CumulativeT2_old_gbed = CumulativeT2_gbed2;  %Total, Cumulative moles of T2 absorbed on the graphite up to this point [moles T2]
                    CumulativeT2_gbed2 = CumulativeT2_gbed2 + jgbed_T2*(Bed_surface_area_s)*DT; %New cumulative amount of T2 captured on graphite in the bed
                    mt2 = mt2 - jgbed_T2*(Bed_surface_area_s)*DT;  %Calc new mt2 moles of T2 in the coolant after some has been adsorbed on graphite
                    C2_T2 = mt2/Mole_flinak;  %Update the current concentration of T2 in primary [mole T2/mol flibe]
                    PP2_T2 = (C2_T2/k_henryT22)*1E6;  %Calculate new partial pressure of T2 above the melt [Pa]
                    %Tgraphite_T2 = ((((0.00019)*sqrt(PP2_T2)*exp(19/(0.00831446*T_avg)))*100)/(8314.46*273.15));  %Calculate new bed capacity for T2 [mole T2/g graphite]
                    %Tgraphite_T2 = (1.118943393*(PP_T2/1000)^(0.36582176) + 2.714730971*((7.032151715*PP_T2/1000)/(1 + 7.032151715*PP_T2/1000)))*(4.4643e-5);
                    Tgraphite_T2 = (5.618912575*((2.453167518*PP_T2/1000)^(1/1.75862069))/(1 + (2.453167518*PP_T2/1000)^(1/1.75862069)))*(4.4643e-5); % ovc 4x8 capacity [moles T2 per gram graphite Pressure in Pa] Langmuir Equation

                    if mtgbed2_T2 > Tgraphite_T2*Bed_mass_s  %If this is true, the graphite has saturated.  Return the values to the previous ones.
                        mt2 = mt2_old;
                        mtgbed2_T2 = mtgbed2_T2_old;
                        CumulativeT2_gbed2 = CumulativeT2_old_gbed;
                    else %Accept the values from the calculation and proceed
                    end
                else %Graphite is already saturated.  Do not transport to it any more
                end
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%Diffusion model for secondary permeator
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if PermeationFlag_secondary == 2  %Do permeator calculations in the secondary.  If both the permeator and the stripper are selected, the permeator happens first.
                %Dperms = 0.24E-6*exp(-21.1/(0.008314*T_avg));  %Permeator diffusion coefficient for Pd from Hara, 2012 [m2/s]
                Dperms = (7.43E-7)*exp(-44.1/(0.008314*T_avg));  %H2 diffusion coefficient in Ni.  From Tanabe, 1984
                % Dperms = 6.32E-7*exp(-47.8/(0.008314*T_avg));  %H2 diffusion coefficient in 316 SS from tanabe
                for counter = 1:1:Permeator_number_s
                    for i = 1:1:PermElements   %Go through each radial slice (i) of the permeator tube wall thickness
                        if i == 1
                            %Assuming dissociation on the surface is virtually instant
                            %                             d_flinak = 2579.3 - 0.624*(T_avg);  %[kg/m3] from Sohal, 2010 [Temp in K]
                            %                             Molar_vol2 = d_flinak/(41.29/1000);  %Molar volume of flinak [mol/m3]
                            
                            %Calculate salt and metal solubility constants
                            k_perm2_sievert = 953*exp(-10.7/(8.314*T_avg/1000));  %[mol/m3 Ni-MPa^0.5] from Tanabe, 1984
                            %                                 k_perm_sievert = 427*exp(-13.9/(8.314*T_avg/1000));  %[mol/m3 316 SS-MPa^0.5] from Tanabe, 1984
                            %k_henryT22 in units of [mol T2/m3 flinak-MPa]
                            
                            Csbs = mt2/Vol_2;  %T2 concentration in the salt bulk at beginning of calculation when salt volume element first enters the HX [moles T2/m3]
                            %Assumes that the coolant mixes after exiting the HX, going through the cold leg,
                            %core, and hot leg prior to re-entering the HX
                            mt2_T2_sinitial = mt2;  %Total moles of T2 in the total secondary coolant inventory
                            mt2_T2_Perm2_in = Csbs*v_dot2*(DT/Permeator_number_s);  %moles T2 that flow into the Permeator volume element in time step of size DT/Permeator_number_p
                            Avgs1 = CPerms(1,i);
                            
                            options = optimoptions('fsolve', 'Display', 'off', 'TolFun', 1E-30, 'TolX', 1E-25);
                            
                            var1sguess = zeros(4,1);  %var0 is used as the initial guess by the MATLAB function, fsolve, called below
                            var1sguess(1,1) = Avgs1;
                            var1sguess(2,1) = 2*Avgs1;
                            Perm2_element_sa = Perms_element_sa(i,1);
                            Moles_firstselement = CPerms(1,i)*Perms_element_vol(i,1);
                            Perms_element_volume = Perms_element_vol(i,1);
                            
                            FS = @(var1s) permentr_secondary(var1s, Dperms, SPX, k_masspipe_T2_perm2, k_henryT22, k_perm2_sievert, Moles_firstselement, Perm2_element_sa, DT, Permeator_number_s, Perms_element_volume, mt2_T2_Perm2_in, v_dot2);
                            
                            [var1s_out] = fsolve(FS, var1sguess, options);
                            CPerms(1,i) = var1s_out(1,1);
                            Avgs1 = var1s_out(1,1);
                            CPs_new = Avgs1;
                            Csm1 = var1s_out(2,1);
                            Cssw = var1s_out(3,1);
                            Cssb = var1s_out(4,1);
                            Csm2 = 2*Avgs1 - Csm1;
                            mt2_T2_Permsout = mt2_T2_Perm2_in - (k_masspipe_T2_perm2*(Cssb - Cssw)*Perm2_element_sa(i,1)*DT/Permeator_number_s);  %moles of T2 in the coolant volume element exiting the current segement j
                            
                            mt2 = mt2_T2_sinitial - (mt2_T2_Perm2_in - mt2_T2_Permsout);  %New total moles of T2 in the secondary system
                            CPerms(1,i) = CPs_new;  %Average concentration of T2 in the first segment [moles T2/m3]
                            Tfluxs1 = (Dperms/SPX)*(CPerms(1,1) - CPerms(1,2));  %Flux of T2 out of (j,1) into (j,2)
                            Moles_outof_sone = Tfluxs1*(Perms_element_sa(i+1,1))*DT/Permeator_number_s;  %Moles of T2 out of the first radial HX wall segment into the second radial segment
                            CPerms(1,1) = CPerms(1,1) - Moles_outof_sone/(Perms_element_vol(i,1));  %[mole T2/m3] New average concentration of T2 in the first segment after accounting for influx and outflux
                            
                        elseif i >=2 && i < PermElements
                            if i == 2
                                Tflux_ins = Tfluxs1;
                                Tflux_outs = (CPerms(1,i)-CPerms(1,i+1))*Dperms/SPX;  %[mole T2/m2-sec] flux out of CPerms(1,i) to CPerms(1,i+1)
                            else
                                Tflux_outs = (CPerms(1,i)-CPerms(1,i+1))*Dperms/SPX;  %[mole T2/m2-sec] flux out of CPerms(1,i) to CPerms(1,i+1)
                            end
                            CPerms(1,i) = (CPerms(1,i)*Perms_element_vol(i,1) + Tflux_ins*(Perms_element_sa(i,1))*(DT/Permeator_number_s) - Tflux_outs*(Perms_element_sa(i+1,1))*(DT/Permeator_number_s))/(Perms_element_vol(i,1));
                            Tflux_ins = Tflux_outs;  %The flux out of i into i+1 must equal the flux into i+1 from i
                        elseif i == PermElements
                            
                            
                            Tflux_outs = ((CPerms(1,i)-0)/SPX)*Dperms;  %Flux out of CPerms(j,PermElements) into the permeator removal side which is assumed to have a T2 concentration of zero[moles T2/m2-sec]
                            CPerms(1,i) = (CPerms(1,i)*Perms_element_vol(i,1) + Tflux_ins*(Permp_element_sa(i,1))*(DT/Permeator_number_s) - Tflux_outs*(Perms_element_sa(i+1,1))*(DT/Permeator_number_s))/(Perms_element_vol(i,1));  %Concentration of T2 in the final radial slice of the permeator tube
                            Permeator_s_total = Permeator_s_total + Tflux_outs*(Perms_element_sa(i+1,1))*(DT/Permeator_number_s);  %[moles T2] total moles of T2 removed fro mthe coolant through the permator
                        end
                    end
                end
                C_T2_permeatorexit_s = mt2/Vol_1;  %[mol T2/m3] Concentration of T2 in coolant exiting the secondary permeator
            else
                %Permeation windows not applied
            end
            
            %             if d >= 48  %Could try experiment where stripping in the secondary is turned on after a set period of time
            if GasStrippingFlag == 3 || GasStrippingFlag == 4  %3 = gas stripping in secondary only.  4 = gas stripping in both primary and secondary loops
                if PermeationFlag_secondary == 2
                    C_T2_secondary = mt2/Vol_2;
                else
                    C_T2_secondary = mt2/Vol_2;  %Concentration of T2 in the secondary coolant [mol/m3]
                end
                mt_T2 = C_T2_secondary*Vol_2;  %[mole T2] initial moles of T2 in secondary coolant
                mt_TF = 0;  %Secondary coolant does not have any TF
                MM_flinak = 41.29;  %flinak molar mass [g/mol]
                m_dot_secondarym = (m_dot_secondary/(MM_flinak/1000))*StrippingFlowFraction_s;  %Secondary coolant flow rate [kg flinak/s] multiplied by stripping flow fraction
                m_dotm = m_dot_secondarym;  %coolant flow rate in the secondary coolant [mol flinak/s]
                Mole_coolant = Mole_flinak;  %[moles Flinak] moles flinak in secondary
                NStages = NStages_s;  %Number of stages for use in the secondary gas stripper
                G = G_s;  %Stripping gas flow rate in the secondary gas stripper [mole/s]
                Loop_Selector = 2;  %Select secondary loop
                [mt_T2, mt_TF, y_1_T2, y1_TF, mole_T2_offgas, x_N_TF, mole_TF_offgas] = GasStrippingModule(NStages, T, T_avg, ...
                    Mole_coolant, m_dotm, mt_TF, mt_T2, G, DT, Loop_Selector);  %Call out to gas stripping module
                mt2 = mt_T2;  %Update moles of T2 in secondary coolant after stripping
                mt2_TF = mt_TF;  %Moles TF in secondary coolant after stripping (will be zero)
                y1_TFs = y1_TF;  %Concentration of TF in exiting gas stream from secondary stripper (will be zero)
                y_1_T2_s = y_1_T2;  %Concentration of T2 in exiting gas from the secondary stripper [mole T2/mole gas]
                mole_T2_soffgas = mole_T2_soffgas + mole_T2_offgas;  %[mole T2] cummulative moles of T2 captured via stripping in the secondary system
                mole_TF_soffgas = mole_TF_soffgas + mole_TF_offgas;  %[mole TF] cummulative moles of TF removed from the coolant via sparging in the secondary (This will be zero.)
                x_N_TF_s = x_N_TF;  %Concentration of TF in the coolant exiting the stripper in the secondary system [mole TF/mole flibe] (will be zero)
                T2_SecondaryConc = mt2/Mole_flinak;  %Concentration of T2 in secondary coolant exiting the stripper [mole T2/mole flibe]
            else
            end
            %             else
            %             end
            if mt2 > 0
                for i = 1:1:Elements  %T2 transport through the secondary heat exchanger is only implemented at the last calculation of a given time step (i.e. when j = n_axial)
                    if i == 1
                        
                        %Assuming dissociation on the surface is virtually instant
                        %Calculate metal solubility constant.  Assuming whole secondary loop is at T_avg
                        k_sievert = 427*exp(-13.9/(8.314*T_avg/1000));  %[mol/m3 316 ss-MPa^0.5]
                        CoeffA2 = HX2_element_vol(i,1);
                        CoeffB2 = v_dot2*DT;
                        Avg2 = CS(1,i);
                        mt2_in = mt2;
                        Csb2 = mt2/Vol_2;  %Assuming that salt is completely mixed before entering the HX2
                        mt2_involel = Csb2*(v_dot2*DT);  %[mole T2] moles of T2 initially in coolant volume-element
                        Moles_finalelement = Avg2*CoeffA2;
                                                
                        %                     options = optimoptions('fsolve', 'Algorithm', 'levenberg-marquardt', 'Display', 'off', 'TolFun', 1E-30, 'TolX', 1E-22);  %Options for fsolve output
                        %                     options = optimoptions('fsolve', 'Algorithm', 'trust-region-reflective', 'Display', 'off');
                        options = optimoptions('fsolve', 'Display', 'off', 'TolFun', 1E-30, 'TolX', 1E-15);
                        var0 = ones(4,1)*Avg2/2000;
                        var0(1,1) = Avg2;
                        var0(2,1) = 1.333*Avg2;
                        HX2_element_sa1 = HX2_element_sa(i,1);
                        F2 = @(var2) HX2entr(var2, Ds, DX, k_transp2, k_henryT22, k_sievert, Moles_finalelement, HX2_element_sa1, DT, CoeffA2, mt2_involel, v_dot2);
                        [var] = fsolve(F2, var0, options);
                        CS(1,i) = var(1,1);
                        Csm2 = var(2,1);
                        Csw2 = var(3,1);
                        Csb2 = var(4,1);
                        mt2 = mt2_in + Csb2*v_dot2*DT - mt2_involel; %update moles of T2 in secondary coolant
                        Checkerinitial = Moles_finalelement + mt2_involel;
                        Checkerfinal = CS(1,i)*CoeffA2 + (Csb2*v_dot2*DT);
                        error = abs(Checkerinitial - Checkerfinal)/Checkerinitial;
                        Tflux2_1out = (Ds/DX)*(CS(1,i) - CS(1,i+1));
                        CS(1,i) = (CS(1,i)*CoeffA2 - Tflux2_1out*DT*HX2_element_sa(i,1))/CoeffA2;
                        
                    elseif i >=2 && i < Elements-1
                        if i == 2
                            Tflux2_in = Tflux2_1out;  %[mole T2/m2-sec] flux out of i = 1 must equal flux into i = 2
                            Tflux2_out = ((CS(1,i)-CS(1,i+1))/DX)*Ds; %[mole T2/m2-sec] flux of T2 out of i = 2 into i = 3
                        else
                            Tflux2_out = ((CS(1,i)-CS(1,i+1))/DX)*Ds;
                        end
                        CS(1,i) = (1/HX2_element_vol(i,1))*(CS(1,i)*HX2_element_vol(i,1) + Tflux2_in*HX2_element_sa(i,1)*DT - Tflux2_out*HX2_element_sa(i+1,1)*DT);  %Concentration of T2 in the specified radial segment of the secondary HX tube wall [mol T2/m3]
                        Tflux2_in = Tflux2_out;  %[mole T2/m2-sec] flux into i+1 = flux out of i
                    elseif i == Elements-1
                        Tflux2_out = ((CS(1,i)-CS(1,i+1))/DX)*Ds;
                        CS(1,i) = (1/HX2_element_vol(i,1))*(CS(1,i)*HX2_element_vol(i,1) + Tflux2_in*HX2_element_sa(i,1)*DT - Tflux2_out*HX2_element_sa(i+1,1)*DT);
                        Tflux2_in = Tflux2_out;
                    elseif i == Elements
                        CS(1,i) = (CS(1,i)*HX2_element_vol(i,1)+ Tflux2_in*HX2_element_sa(i,1)*DT)/HX2_element_vol(i,1);
                        Tflux2_out = (CS(1,i)-0.00)*Ds/(0.5*DX*PRF);
                        CS(1,i) = (CS(1,i)*HX2_element_vol(i,1) - Tflux2_out*HX2_element_sa(i+1,1)*DT)/HX2_element_vol(i,1);
                        CPC = CPC+(Tflux2_out*HX2_element_sa(i+1,1)*DT);
                        mt3 = CPC;
                    end
                end
            else %mt2 = 0.  Don't do the calculation
            end
            %mt2 = CS(1,1)*Vol_2;  %[mole T2]  mole T2 in secondary loop
            mt3 = CPC;  %[mole T2] released to power cycle
            PP_T2_HX_secondary = ((CS(1,1)/Sievert_316_secondary)^2)*9.86923267;  %[atm] partial pressure of T2 over metal in secondary heat exchanger
        else
        end
    else
    end
    %%%%% End of calculations in the secondary loop  %%%%%
    
    %%%%% Back to primary loop calculations          %%%%%
    
    % Consider corrosion/deposition and generation of T2 from corrosion
    % etc.  Call out to corrosion module if corrosion is deemed to occur
    % need to keep tabs on c1b and c1a and mt1_T2 and mt1_TF.
    %Currently assumes that T2 transport occurs before corrosion deposition.  In reality, both should occur simultaneously.
    if Corrosionflag == 1      %Corrosion is off
    elseif Corrosionflag == 2  %Corrosion is on
        if Feedbackflag == 1||Feedbackflag == 3   %Calculate the current ratio of TF to T2 in the salt
            C_TF = mt1_TF/Mole_flibe;  %Concentration of TF in salt [mole TF/mole salt]
            C_T2 = mt1_T2/Mole_flibe;  %Concentration of T2 in salt [mole TF/mole salt]
            PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
            PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
            if PP_T2 <= 0  %If no T2 is in the system yet, set a very small value so that a Ratio_TF_T2 can be calculated below
                PP_T2 = 1E-40;
            else
            end
            Ratio_TF_T2 = PP_TF/sqrt(PP_T2);  %Calculate current ratio of TF to T2
        else
        end
        dens_flibe = (2415.6-0.49072*T)*1000;  %Flibe density from Janz correlation in Sohal, 2010 [g/m^3]
        Cr_MM = 51.9961;  %Cr molar mass [g/mol]
        Flibe_MM = 32.8931;  %Molar mass of flibe [g/mol]
        C_Cr_metal = Mole_frac_metal(1,1);  %Mole fraction of Cr in the base metal [mol Cr/mol Stainless]
        C_Cr_eq = (((Cr_MM/Flibe_MM)*1E6)*10^(-5.12+((9.06E3)/T)+log10(C_Cr_metal)+log10(Ratio_TF_T2^2)))*(1E-6)*(Vol_1*dens_flibe/Cr_MM)*(1/Vol_1);  %Calculate equilibrium Cr concentration given the base metal [moles Cr/m3 coolant]
        if C_Cr_actual - C_Cr_eq > 0  %If yes, then do preciptation of Cr
            J_ppt_pipe = k_Cr_masspipe*(C_Cr_actual - C_Cr_eq); %Flux of dissolved Cr to reflector surface [mol Cr2+/m2-s]
            New_moles_Cr_ppt = (J_ppt_pipe*pipe_sa(j,1))*DT;  %Moles Cr deposited in the current timestep at the current axial position
            if New_moles_Cr_ppt > C_Cr_actual*Vol_1  %Ensure that amount of Cr precipitated from the current time step does not exceed the amount of Cr presently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + C_Cr_actual*Vol_1;  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - C_Cr_actual*Vol_1;   %Update total moles of T2 in primary
                mt1_TF = mt1_TF + 2*C_Cr_actual*Vol_1; %Update total moles of TF in primary
                C_Cr_actual = 0;  %[mol Cr/m3] concetration of Cr dissolved in primary coolant
            else  %If amount of Cr preciptated from the current time step is less than the total amount of Cr currently dissolved in the coolant
                Moles_Cr_ppt(j,1) = Moles_Cr_ppt(j,1) + New_moles_Cr_ppt;  %Total moles Cr deposited at the current axial position
                mt1_T2 = mt1_T2 - New_moles_Cr_ppt;   %Update total moles of T2 in primary
                mt1_TF = mt1_TF + 2*New_moles_Cr_ppt; %Update total moles of TF in primary
                C_Cr_actual = C_Cr_actual - New_moles_Cr_ppt/Vol_1;  %New concentration of Cr in coolant [mol Cr/m3]
            end
            GramsCr_persa(j,1) = (Moles_Cr_ppt(j,1)*Cr_MM/(pipe_sa(j,1)));  %Total grams of Cr deposited per unit surface area at the particular axial location [g Cr/m2]
            
        elseif C_Cr_actual - C_Cr_eq < 0   %Then do corrosion
            corrosion_time(j,1) = corrosion_time(j,1) + DT;
            Cr_dissolved = C_Cr_actual*Vol_1;  %[moles Cr2+ in primary coolant]
            Conc_TF = mt1_TF/Vol_1;        %[mole TF/m3 flibe] current concentration of TF in primary coolant
            %need to read in k_masspipe to corrosion module
            [Cr_dissolved, Cr_coolant, TF_consumed, T2_generated, Cr_profile, mt1_TF, mt1_T2, ...
                Conc_slice, grad, Crperslice, initial, Vol_surf, Totmatomsurf, surface, Crcorrodedaxial] = CorrosionModule(j, initial, surface, Conc_TF, k_masspipe_TF, Vol_surf, ...
                pipe_sa, pipe_d, pipe_l, pipe_d2, pipe_l2, Hx_tube_od, Vol_1, Thick, DT, Slice_vol, Crperslice, ...
                Cr_dissolved, TF_consumed, Cr_profile, slices1, slices2, slice_thick1, slice_thick2, Conc_slice, ...
                Conc_bulk, Totmatomsurf, grad, Lattice_param, mt1_TF, mt1_T2, T, Core_mesh, Hot_mesh, Hx_mesh, Cold_mesh, ...
                Length1_tot, Crcorrodedaxial, t, Surf_area_gb);  %Call out to corrosion module
            C_Cr_actual = Cr_coolant;  %[moles Cr/m3]
        end
        %Before moving on, adjust T2 vs TF depending on redox option
        if Feedbackflag == 2;  %Feedback ON. Fraction P_TF/(P_T2)^0.5 is held constant in the salt. Adjustment being made now after TF birth and T2 destruction from CrF2 precipitation
            C_total =  mt1_TF/Mole_flibe + 2*mt1_T2/Mole_flibe; %[Mole of T atoms/Mole of flibe]
            C_T2 = (4*C_total*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
                +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
            C_TF = C_total -2*C_T2;        %[mole TF/mole flibe] mole fraction TF in flibe
            mt1_T2new = C_T2*Mole_flibe - mt1_T2;  %[Change in moles of T2 in primary] due to generation in flibe and thermodynamic feedback
            mt1_TFnew = C_TF*Mole_flibe - mt1_TF;  %[Change in moles TF in primary] due to generation in flibe and thermodynamic feedback
            mt1_T2 = mt1_T2 + mt1_T2new;  %Current T2 in primary [mole T2]
            mt1_TF = mt1_TF + mt1_TFnew;  %Current TF in primary [mole T+]
            %mt1_total = 2*mt1_T2 + mt1_TF;  %Current total moles of tritium in the primary loop
        else
        end
    end
    mt1_total = mt1_TF + 2*mt1_T2;  %[mole tritium]  total moles of tritium in the primary system
    C_T2 = mt1_T2/Mole_flibe;  %Current concentration of T2 in the primary [mole T2/mole Flibe]
    C_TF = mt1_TF/Mole_flibe;  %Current concentration of TF in primary [mole T2/mol Flibe]
    PP_TF = C_TF/k_HenryTF;  %Partial pressure of TF above the melt [atm]
    PP_T2 = C_T2/k_HenryT2;  %Partial pressure of T2 above the melt [atm]
    if PP_T2 <= 0  %If no T2 is in the system yet, set a very small value so that a Ratio_TF_T2 can be calculated below
        PP_T2 = 1E-40;
    else
    end
    Redox_Potential = 2*0.00831446*T*log(PP_TF/sqrt(PP_T2))+2*(-4.6976E-10*T^3 + 3.1425E-6*T^2 - 8.8612E-3*T - 2.7305E2);  %[kJ/mol F2] Temp in [K] Calculate redox fluorine potential at the current axial location [kJ/mol F2]
    
end