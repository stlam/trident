function [k_HenryT2_temp, k_HenryTF_temp, k_masspebble_T2_temp, k_masspipe_T2_temp, k_masspipe_TF_temp, k_Cr_pebble_temp, k_Cr_masspipe_temp, Dp_temp, Dp_hot, ...
    Ds_temp, Sievert_316_temp_primary, Sievert_316_temp_secondary, Bed_k_masspebble_T2_temp, Bed_k_Cr_pebble_temp, Bed_pressure_drop, ...
    v_dot_temp, v_dot2_temp, k_henryT22_temp, k_transp2_temp, Bed_k_masspebble_TF_temp, k_masspebble_TF_temp, k_masspipe_T2_perm1_temp, ...
    k_masspipe_T2_perm2_temp, Rey_bed, Rey_bed2, Bed_k_masspebble_T2_temp_s, Bed_pressure_drop2]= temperature_dependent_polythermal(R_univ, ...
    OuterRef_inradius, CentralRef_radius, Hx_tube_ir, N_Hx_tubes1, T_out, T_in, m_dot, m_dot2, Flowarea, Void_frac, Pebble_radius, pipe_d, ...
    pipe_d2, Core_mesh, Hot_mesh, Cold_mesh, Hx_mesh, T, j, Bed_flowarea, Bed_voidfraction, Particle_radius, Bed_vessel_length, T_avg, NumPermptubes, ...
    NumPermstubes, Permp_tube_flowarea, Perms_tube_flowarea, Permp_tube_ir, Perms_tube_ir, Hx_height1, Hx_height2,Permp_height,Perms_height, Bed_flowarea_s, ...
    Particle_radius_s, Bed_voidfraction_s, Bed_vessel_length_s)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the values of temperature dependent properties 
% based on the temperature profile so that they can be used throughout the
% loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Flibe thermophysical properties
% k_flibe = 0.0005*T+32/Flibe_MM - 0.34;  %Flibe thermal conductivity, Williams 2006 [W/m-K]  
dens_flibe = 2415.6-0.49072*T;          %Flibe density from Janz correlation in Sohal 2010 [kg/m^3] (T has units of K)
mu_flibe = 0.116*exp(3755/T)/1000;      %Flibe viscosity Benes 2012[Pa-s]

%Flinak properties
dens_flinak = 2579.3 - 0.624*(T_avg);  %[kg/m3] from Sohal, 2010 [Temp in K]
% MM_flinak = 41.29/1000;  %[kg/mol] Molar mass for Flinak (0.465LiF-0.115NaF-0.42KF)
k_henryT22_temp = 3.98E-7*exp(34400/(8.314*(T_avg)))*1E6;  %[mol/m3-MPa] for flinak from Fukada expression modified to 34400 instead of 34.4 in the exponent. Requires temperature in Kelvin
% k_henryT22 = k_henryT22*(1/(9.86923E-6))*(1/dens_flinak)*MM_flinak; %Converted to units of [mol T2/mol melt-Pa]
mu_flinak = (0.00002487)*exp(4478.62/T_avg);  %[Pa-s] viscosity from Janz correlation in Sohal, 2010
v_dot2 = m_dot2/dens_flinak;  %[m3/s] volumetric flow rate of secondary coolant flinak
v_dot2_temp = v_dot2;  
Diff_T2_secondary = (2.4537E-29)*T_avg^6.9888;  %[m2/s] Diffusion coefficient of T2 in flinak from Fukada, 2006 Figure 5 (note that Fukada equation is wrong, but figure 5 is right)


%Tritium solubility in salts:
%  Tritium (T2) solubility in flibe from Calderoni, 2008
%  [mol/m^3-Pa]
% S_flibe = (7.9e-2)*exp((-35e3)/(R_univ*T));
%  Hydrogen (H2) solubility in flinak from Fukada, 2006
%  [mol/m^3-Pa] Expression fixes Fukada error by using 34.4E3 in place of 34.4
% S_flinak = (3.98e-7)*exp(34.4e3/(R_univ*T));

%Henry's Law constants for T2 and TF in flibe:
k_HenryT2_temp =3.923E-8*exp(4.483E-3*(T-273.15)); %[mole T2/mol flibe-atm] Fit to Malinauskas, 1974 for H2
k_HenryTF_temp = 24.716E-4*exp(-0.004*(T-273.15));  %[mole HF/mol flibe-atm] Exponential fit to Field and Shaffer, 1967 for HF

%Sieverts' Law constants for T2 in 316 SS, from Tanabe, 1984:
Sievert_316_temp_primary = 427*exp(-13.9/(R_univ*T/1000));  %[mol/m3-MPa^0.5]
Sievert_316_temp_secondary = 427*exp(-13.9/(R_univ*((T_out+T_in)/2)/1000));  %[mol/m3-MPa^0.5] set at average coolant temperature 

%Parameters for calculating diffusion coefficient of Cr2+ in flibe from the Stokes-einstein equaiton
k_boltz = 1.3806488E-23;  %[J/K] aka [(kg-m^2)/(K-s^2)] Boltzmann constant
r_Cr_ion = 80E-12;  %[m] effective radius of the Cr2+ ion (from Wikipedia)
D_Cr = (1/mu_flibe)*(k_boltz*T)/(6*pi*r_Cr_ion);  %[m^2/s] diffusion coefficient of Cr2+ (dissolved CrF2) in flibe from Stokes-Einstein

  
%%% Mass transport coefficient calculation for transport to pebble surfaces
%%% in reactor core
v_dot = m_dot/dens_flibe;              %Primary coolant volumetric flow rate [m^3/s]
v_dot_temp = v_dot;
super_V = m_dot/(dens_flibe*Flowarea); %Primary coolant revised superficial velocity [m/s]
% V_char = m_dot/(dens_flibe*Void_frac*Flowarea);  %Characteristic velocity
% L_char = 2*Pebble_radius*(Void_frac/(1-Void_frac));  %Characteristic length in pebble ped
Rey = dens_flibe*super_V*2*Pebble_radius/(mu_flibe*(1-Void_frac));  %Reynolds number in a packed bed
Diff_T2_primary = (9.3E-7)*exp(-(42E3)/(R_univ*T));      %Diffusion coefficient for T2 in flibe from Calderoni, 2008 [m^2/s]
Diff_TF_primary = 6.4854E-26*T^5.7227;  %Diffusion coefficient of TF in flibe from Oishi et. al. 1989 [m2/s].  T in units of K


%For T2 and Cr
Sc_T2 = mu_flibe/(dens_flibe*Diff_T2_primary);   %Schmit number
Sh_T2 = (0.5*Rey^0.5+0.2*Rey^(2/3))*Sc_T2^(1/3); %Sherwood number for mass transfer in a packed bed
% Sh_T2_achenbach =((Sc_T2)^(1/3))*((1.18*Rey^0.58)^4+(0.23*Rey^0.75)^0.4)^0.25;  %From Dobranich thesis and Achenbach review article
Sc_Cr = mu_flibe/(dens_flibe*D_Cr);              %Schmit number
Sh_Cr = (0.5*Rey^0.5+0.2*Rey^(2/3))*Sc_Cr^(1/3); %Sherwood number for mass transfer in a packed bed
k_masspebble_T2_temp = Sh_T2*(Diff_T2_primary/(2*Pebble_radius));   %Mass transfer coefficient for tritium to pebble surface in flibe [m/s]
% k_masspebble_T2_achenbach = Sh_T2_achenbach*(Diff_T2_primary/(2*Pebble_radius));
k_Cr_pebble_temp = Sh_Cr*(D_Cr/(2*Pebble_radius));      %Mass transfer coefficient for Cr2+ to pebble surface in flibe [m/s]

%For TF
Sc_TF = mu_flibe/(dens_flibe*Diff_TF_primary);        %Schmidt number
Sh_TF = (0.5*Rey^0.5+0.2*Rey^(2/3))*Sc_TF^(1/3);      %Sherwood number for mass transfer in a packed bed
k_masspebble_TF_temp = Sh_TF*(Diff_TF_primary/(2*Pebble_radius));   %Mass transfer coefficient for tritium to pebble surface in flibe [m/s]



%%% Mass transport coefficient calculation for transport to PRIMARY system graphite tritium
%%% absorber bed surfaces. These values may be different from those used for
%%% the core pebble bed.  Assumes bed is at core outlet temp
dens_flibe_bed = 2415.6-0.49072*T_out;          %Flibe density from Janz correlation in Sohal 2010 [kg/m^3]
mu_flibe_bed = 0.116*exp(3755/T_out)/1000;      %Flibe viscosity Benes 2012[Pa-s]
Bed_super_V = m_dot/(dens_flibe_bed*Bed_flowarea); %Primary coolant revised superficial velocity [m/s]
% Bed_V_char = m_dot/(dens_flibe*Bed_voidfraction*Bed_flowarea);  %Characteristic velocity
% Bed_L_char = 2*Particle_radius*(Bed_voidfraction/(1-Bed_voidfraction));  %Characteristic length in pebble ped
Rey_bed = dens_flibe_bed*Bed_super_V*2*Particle_radius/(mu_flibe_bed*(1-Bed_voidfraction));  %Reynolds number in a packed bed
Diff_T2_primary_bed = (9.3E-7)*exp(-(42E3)/(R_univ*T_out));      %Diffusion coefficient for T2 in flibe from Calderoni, 2008 [m^2/s];
Sc_bed_T2 = mu_flibe_bed/(dens_flibe_bed*Diff_T2_primary_bed);

%For T2 in absorber bed
Bed_Sh_T2 = (0.5*Rey_bed^0.5+0.2*Rey_bed^(2/3))*Sc_bed_T2^(1/3);  %Sherwood number for mass transfer in a packed bed
D_Cr_bed = (1/mu_flibe_bed)*(k_boltz*T_out)/(6*pi*r_Cr_ion);
Sc_bed_Cr = mu_flibe_bed/(dens_flibe_bed*D_Cr_bed);
Bed_Sh_Cr = (0.5*Rey_bed^0.5+0.2*Rey_bed^(2/3))*Sc_bed_Cr^(1/3);
Bed_k_masspebble_T2_temp = Bed_Sh_T2*(Diff_T2_primary_bed/(2*Particle_radius));  %Mass transfer coefficient for tritium to pebble surface in flibe [m/s]
Bed_k_Cr_pebble_temp = Bed_Sh_Cr*(D_Cr_bed/(2*Particle_radius));  %Mass transfer coefficient for Cr2+ to pebble surface in flibe [m/s]

%For TF in absorber bed
Diff_TF_primary_bed = 6.4854E-26*T_out^5.7227;  %Diffusion coefficient of TF in flibe from Oichi et. al. 1989 [m2/s].  T in units of K;
Sc_bed_TF = mu_flibe_bed/(dens_flibe_bed*Diff_TF_primary_bed);
Bed_Sh_TF = (0.5*Rey_bed^0.5+0.2*Rey_bed^(2/3))*Sc_bed_TF^(1/3);  %Sherwood number for mass transfer in a packed bed
Bed_k_masspebble_TF_temp = Bed_Sh_TF*(Diff_TF_primary_bed/(2*Particle_radius));   %Mass transfer coefficient for tritium to pebble surface in flibe [m/s]

Bed_friction_factor = (150/Rey_bed)+1.75;  %Ergun packed bed friction factor 
Bed_pressure_drop = -(Bed_friction_factor*Bed_vessel_length)*((dens_flibe_bed*Bed_super_V^2*(1-Bed_voidfraction))/(2*Particle_radius*Bed_voidfraction^3)); %[Pa] Bed pressure drop


%%% Mass transport coefficient calculation for transport to SECONDARY system graphite tritium
%%% absorber bed surfaces. Assumes bed is at average secondary loop temp
dens_flinak = 2579.3 - 0.624*(T_avg);  %[kg/m3] from Sohal, 2010 [Temp in K]
mu_flinak_bed_s = (0.00002487)*exp(4478.62/T_avg);  %[Pa-s] viscosity from Janz correlation in Sohal, 2010
Bed_super_V2 = m_dot2/(dens_flinak*Bed_flowarea_s); %Primary coolant revised superficial velocity [m/s]
Rey_bed2 = dens_flinak*Bed_super_V2*2*Particle_radius_s/(mu_flinak_bed_s*(1-Bed_voidfraction_s));  %Reynolds number in packed bed of graphite spheres in secondary system
Sc_bed_T2_s = mu_flinak_bed_s/(dens_flinak*Diff_T2_secondary);

%For T2 in absorber bed
Bed_Sh_T2_s = (0.5*Rey_bed2^0.5+0.2*Rey_bed2^(2/3))*Sc_bed_T2_s^(1/3);  %Sherwood number for mass transfer in a packed bed
Bed_k_masspebble_T2_temp_s = Bed_Sh_T2_s*(Diff_T2_secondary/(2*Particle_radius_s));  %Mass transfer coefficient for tritium to pebble surface in flibe [m/s]

% %For TF in absorber bed.  No TF on secondary side
% Diff_TF_primary_bed = 6.4854E-26*T_out^5.7227;  %Diffusion coefficient of TF in flibe from Oichi et. al. 1989 [m2/s].  T in units of K;
% Sc_bed_TF = mu_flibe_bed/(dens_flinak*Diff_TF_primary_bed);
% Bed_Sh_TF = (0.5*Rey_bed2^0.5+0.2*Rey_bed2^(2/3))*Sc_bed_TF^(1/3);  %Sherwood number for mass transfer in a packed bed
% Bed_k_masspebble_TF_temp = Bed_Sh_TF*(Diff_TF_primary_bed/(2*Particle_radius_s));   %Mass transfer coefficient for tritium to pebble surface in flibe [m/s]

Bed_friction_factor2 = (150/Rey_bed2)+1.75;  %Ergun packed bed friction factor 
Bed_pressure_drop2 = -(Bed_friction_factor2*Bed_vessel_length_s)*((dens_flinak*Bed_super_V2^2*(1-Bed_voidfraction_s))/(2*Particle_radius_s*Bed_voidfraction_s^3)); %[Pa] Bed pressure drop
%%%

% Mass transport coefficient calculation for TF and Cr2+ transport to pipe surfaces (reactor core walls, hot leg, cold leg, hx).
if j >= 1 && j <= Core_mesh  %In REACTOR CORE, calculate TF and Cr2+ mass transport coefficient to inner and outer graphite reflector and pebbles
    d_pebble = Pebble_radius*2;                              %[m] pebble diameter
    d_reactor = 2*(OuterRef_inradius - CentralRef_radius);       %[m] Reactor core pseudo diameter of pebble-filled region
    Sh_TF_wall = (1-d_pebble/d_reactor)*(Sc_TF^(1/3))*(Rey^0.61);  %Sherwood number for mass transfer from fluid to inner and outer graphite reflector wall in a packed bed.  From Dixon 1985. 
    Sh_T2_wall = (1-d_pebble/d_reactor)*(Sc_T2^(1/3))*(Rey^0.61);
    Sh_Cr_wall = (1-d_pebble/d_reactor)*(Sc_Cr^(1/3))*(Rey^0.61);
    k_masspipe_TF_temp = Sh_TF_wall*(Diff_TF_primary/d_reactor);        %Mass trans coeff for TF to inner and outer reflector surface [m/s]
    k_masspipe_T2_temp = Sh_T2_wall*(Diff_T2_primary/d_reactor);        %Mass trans coeff for T2 to inner and outer reflector surface [m/s]
    k_Cr_masspipe_temp = Sh_Cr_wall*(D_Cr/d_reactor);       %Mass trans coeff for Cr2+ to inner and outer reflector surface [m/s]
elseif j >= Core_mesh +1 && j <= Core_mesh + Hot_mesh  %Calculate TF mass transport coefficient to the HOT LEG pipe surface
    v_flow = v_dot/(pi*(pipe_d/2)^2);               %flow velocity in outlet pipe [m/s]
    Reypipe = (dens_flibe*v_flow*pipe_d)/mu_flibe;  %Reynolds number in core outlet pipe
    Scpipe_T2 =  mu_flibe/(dens_flibe*Diff_T2_primary);         %Schmit number for T2 in pipe
    Scpipe_TF =  mu_flibe/(dens_flibe*Diff_TF_primary);
    Scpipe_Cr =  mu_flibe/(dens_flibe*D_Cr);         %Schmit number for pipe
    if Reypipe <= 2030
        Shpipe_T2 = 1.86*(Reypipe^(1/3))*(Scpipe_T2^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_TF = 1.86*(Reypipe^(1/3))*(Scpipe_TF^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_Cr = 1.86*(Reypipe^(1/3))*(Scpipe_Cr^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
    elseif Reypipe > 2030 && Reypipe < 10000
        %Sherwood number for Re > 2030 up to 1E5.  Use Dittus-Boelter above 1E4 though
        Shpipe_T2 = 0.015*(Reypipe^0.83)*(Scpipe_T2^0.42);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_TF = 0.015*(Reypipe^0.83)*(Scpipe_TF^0.42);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_Cr = 0.015*(Reypipe^0.83)*(Scpipe_Cr^0.42);  %From Whitaker 1972 for laminar flow in pipes
    else
        Shpipe_T2 = 0.023*(Reypipe^0.8)*(Scpipe_T2^0.4);       %Sherwood number for hx tube from Dittus-Boelter
        Shpipe_TF = 0.023*(Reypipe^0.8)*(Scpipe_TF^0.4);       %Sherwood number for hx tube
        Shpipe_Cr = 0.023*(Reypipe^0.8)*(Scpipe_Cr^0.4);       %Sherwood number for pipe
    end
    k_masspipe_T2_temp = (Diff_T2_primary/pipe_d)*Shpipe_T2;  %Mass trans coeff for T2 to pipe surface (Todreas & Kazimi Eqn 10.91) [m/s]
    k_masspipe_TF_temp = (Diff_TF_primary/pipe_d)*Shpipe_TF;  %Mass trans coeff for TF to pipe surface (Todreas & Kazimi Eqn 10.91) [m/s]
    k_Cr_masspipe_temp = (D_Cr/pipe_d)*Shpipe_Cr;      %Mass trans coeff for Cr2+ to hot leg pipe surface [m/s]
elseif j >= Core_mesh + Hot_mesh +1 && j <= Core_mesh + Hot_mesh + Hx_mesh  %Calculate TF mass transport coefficient to the PRIMARY HX tube surfaces
    v_flow = v_dot/(N_Hx_tubes1*pi*(Hx_tube_ir)^2);      %flow velocity in hx tubes [m/s]
    Reypipe = (dens_flibe*v_flow*Hx_tube_ir*2)/mu_flibe; %Reynolds number in hx tubes
    Scpipe_T2 =  mu_flibe/(dens_flibe*Diff_T2_primary);              %Schmit number hx tube
    Scpipe_TF =  mu_flibe/(dens_flibe*Diff_TF_primary);
    Scpipe_Cr =  mu_flibe/(dens_flibe*D_Cr);         %Schmit number for pipe
    if Reypipe <= 2030
        Shpipe_T2 = 1.86*(Reypipe^(1/3))*(Scpipe_T2^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_TF = 1.86*(Reypipe^(1/3))*(Scpipe_TF^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_Cr = 1.86*(Reypipe^(1/3))*(Scpipe_Cr^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
    elseif Reypipe > 2030 && Reypipe < 10000
        %Sherwood number for Re > 2030 up to 1E5.  Use Dittus-Boelter above 1E4 though
        Shpipe_T2 = 0.015*(Reypipe^0.83)*(Scpipe_T2^0.42);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_TF = 0.015*(Reypipe^0.83)*(Scpipe_TF^0.42);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_Cr = 0.015*(Reypipe^0.83)*(Scpipe_Cr^0.42);  %From Whitaker 1972 for laminar flow in pipes
    else
        Shpipe_T2 = 0.023*(Reypipe^0.8)*(Scpipe_T2^0.4);       %Sherwood number for hx tube from Dittus-Boelter
        Shpipe_TF = 0.023*(Reypipe^0.8)*(Scpipe_TF^0.4);       %Sherwood number for hx tube
        Shpipe_Cr = 0.023*(Reypipe^0.8)*(Scpipe_Cr^0.4);       %Sherwood number for pipe
    end
    k_masspipe_T2_temp = (Diff_T2_primary/(Hx_tube_ir*2))*Shpipe_T2;    %Mass trans coeff for T2 to hx tube inner surface (Todreas & Kazimi Eqn 10.91) [m/s]
    k_masspipe_TF_temp = (Diff_TF_primary/(Hx_tube_ir*2))*Shpipe_TF;
    k_Cr_masspipe_temp = (D_Cr/(Hx_tube_ir*2))*Shpipe_Cr;   %Mass trans coeff for Cr2+ to hx tube inner surface (Todreas & Kazimi Eqn 10.91) [m/s]
elseif j >= Core_mesh + Hot_mesh + Hx_mesh +1 && j<= Core_mesh + Hot_mesh + Hx_mesh + Cold_mesh  %Calculate TF mass transport coefficient to the COLD LEG pipe surface
    v_flow = v_dot/(pi*(pipe_d2/2)^2);              %flow velocity in outlet pipe [m/s]
    Reypipe = (dens_flibe*v_flow*pipe_d2)/mu_flibe; %Reynolds number in core outlet pipe
    Scpipe_T2 =  mu_flibe/(dens_flibe*Diff_T2_primary);         %Schmit number for pipe
    Scpipe_TF =  mu_flibe/(dens_flibe*Diff_TF_primary); 
    Scpipe_Cr =  mu_flibe/(dens_flibe*D_Cr);         %Schmit number for pipe
    if Reypipe <= 2030
        Shpipe_T2 = 1.86*(Reypipe^(1/3))*(Scpipe_T2^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_TF = 1.86*(Reypipe^(1/3))*(Scpipe_TF^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_Cr = 1.86*(Reypipe^(1/3))*(Scpipe_Cr^(1/3))*(Hx_height1/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
    elseif Reypipe > 2030 && Reypipe < 10000  %Sherwood number for Re > 2030 up to 1E5.  Use Dittus-Boelter above 1E4 though
        Shpipe_T2 = 0.015*(Reypipe^0.83)*(Scpipe_T2^0.42);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_TF = 0.015*(Reypipe^0.83)*(Scpipe_TF^0.42);  %From Whitaker 1972 for laminar flow in pipes
        Shpipe_Cr = 0.015*(Reypipe^0.83)*(Scpipe_Cr^0.42);  %From Whitaker 1972 for laminar flow in pipes
    else
        Shpipe_T2 = 0.023*(Reypipe^0.8)*(Scpipe_T2^0.4);       %Sherwood number for hx tube from Dittus-Boelter
        Shpipe_TF = 0.023*(Reypipe^0.8)*(Scpipe_TF^0.4);       %Sherwood number for hx tube
        Shpipe_Cr = 0.023*(Reypipe^0.8)*(Scpipe_Cr^0.4);       %Sherwood number for pipe
    end
    k_masspipe_T2_temp = (Diff_T2_primary/pipe_d2)*Shpipe_T2;      %Mass trans coeff for T2 to cold leg pipe surface (Todreas & Kazimi Eqn 10.91) [m/s]
    k_masspipe_TF_temp = (Diff_TF_primary/pipe_d2)*Shpipe_TF;
    k_Cr_masspipe_temp = (D_Cr/pipe_d2)*Shpipe_Cr;     %Mass trans coeff for Cr2+ to cold leg pipe surface (Todreas & Kazimi Eqn 10.91) [m/s]
end

%Mass transfer coefficient for T2 to primary permeator tube wall
dens_flibe = 2415.6-0.49072*T_out;          %Flibe density from Janz correlation in Sohal 2010 [kg/m^3]
mu_flibe = 0.116*exp(3755/T_out)/1000;      %Flibe viscosity Benes 2012[Pa-s]
Diff_T2_primary = (9.3E-7)*exp(-(42E3)/(R_univ*T_out));      %Diffusion coefficient for T2 in flibe from Calderoni, 2008 [m^2/s]
v_flowp1 = v_dot/(NumPermptubes*Permp_tube_flowarea);  %Coolant velocity in primary permeator tubes
Reyperm1 = (dens_flibe*v_flowp1*2*Permp_tube_ir)/mu_flibe; %Reynolds number in permeator tube
Scperm1 = mu_flibe/(dens_flibe*Diff_T2_primary);              %Schmit number for T2 in permeator tube;
    if Reyperm1 <= 2030
        Shperm1 = 1.86*(Reyperm1^(1/3))*(Scperm1^(1/3))*(Permp_height/(Permp_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
    elseif Reyperm1 > 2030 && Reyperm1 < 10000
        %Sherwood number for Re > 2030 up to 1E5.  Use Dittus-Boelter above 1E4 though
        Shperm1 = 0.015*(Reyperm1^0.83)*(Scperm1^0.42);  %From Whitaker 1972 for laminar flow in pipes
    else
        Shperm1 = 0.023*(Reyperm1^0.8)*(Scperm1^0.4);       %Sherwood number for hx tube from Dittus-Boelter
    end
k_masspipe_T2_perm1_temp = (Diff_T2_primary/(2*Permp_tube_ir))*Shperm1;

%Mass transfer coefficient for T2 to secondary permeator tube wall
v_flowp2 = v_dot2/(NumPermstubes*Perms_tube_flowarea);  %Coolant velocity in secondary permeator tubes
Reyperm2 = (dens_flinak*v_flowp2*2*Perms_tube_ir)/mu_flinak; %Reynolds number in permeator tube
Scperm2 = mu_flinak/(dens_flinak*Diff_T2_secondary);              %Schmit number for T2 in permeator tube;
    if Reyperm2 <= 2030
        Shperm2 = 1.86*(Reyperm2^(1/3))*(Scperm2^(1/3))*(Perms_height/(Perms_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes
    elseif Reyperm2 > 2030 && Reyperm2 < 10000
        %Sherwood number for Re > 2030 up to 1E5.  Use Dittus-Boelter above 1E4 though
        Shperm2 = 0.015*(Reyperm2^0.83)*(Scperm2^0.42);  %From Whitaker 1972 for turbulent flow in pipes
    else
        Shperm2 = 0.023*(Reyperm2^0.8)*(Scperm2^0.4);       %Sherwood number for hx tube from Dittus-Boelter
    end
k_masspipe_T2_perm2_temp = (Diff_T2_secondary/(2*Perms_tube_ir))*Shperm2;  %Mass transfer coefficient for T2 to the inside of the secondary permeator surface
    
% Mass transport coefficients inside HX2
    v_flow2 = v_dot2/(N_Hx_tubes1*pi*(Hx_tube_ir)^2);      %flow velocity in hx2 tubes [m/s]
    Reypipe2 = (dens_flinak*v_flow2*Hx_tube_ir*2)/mu_flinak; %Reynolds number in hx2 tubes
    Scpipe2 =  mu_flinak/(dens_flinak*Diff_T2_secondary);    %Schmit number hx2 tube

    if Reypipe2 <= 2030
        Shpipe2 = 1.86*(Reypipe2^(1/3))*(Scpipe2^(1/3))*(Hx_height2/(Hx_tube_ir*2))^(-1/3);  %From Whitaker 1972 for laminar flow in pipes       
    elseif Reypipe2 > 2030 && Reypipe2 < 10000
        %Sherwood number for Re > 2030 up to 1E5.  Use Dittus-Boelter above 1E4 though
        Shpipe2 = 0.015*(Reypipe2^0.83)*(Scpipe2^0.42);  %From Whitaker 1972 for laminar flow in pipes   
    else
        Shpipe2 = 0.023*(Reypipe2^0.8)*(Scpipe2^0.4);       %Sherwood number for hx tube from Dittus-Boelter  
    end
        
    k_transp2_temp = (Diff_T2_secondary/(Hx_tube_ir*2))*Shpipe2;    %Mass trans coeff for T2 to hx tube inner surface (Todreas & Kazimi Eqn 10.91) [m/s]

%Hydrogen transport in standard in 316 SS
% S_316 = 4.27E2*exp(-13.9/((R_univ/1000)*T)); %[mol/m3-MPa^1/2] from Tanabe, 1984
% D_316 = 6.32E-7*exp(-47.8/((R_univ/1000)*T)); %[m2/s] Hydrogen diffusivity in 316 SS (Not specifically called 316 L, assume it is standard 316)
% P_316 = S_316*D_316;  %Permeability
%There is also a metals permeation model from Calderoni and Ebner, 2010


%Sets hydrogen diffusion coefficient for metals in primary heat exchanger
%Note that there are two possible Dp_temp values for 316.  One is explicitly for 316 L, and the other just says 316.  
  %Do, diffusion pre-exponential factor [m^2/s]
  %Ed, diffusion activation energy  [kJ/mol]
    %Dp_temp = 2E-36*(T^9.2887);  %[m^2/s] H transport in 316 L, From fit to data from Lee,2011
    Dp_temp = 6.32E-7*exp(-47.8/((R_univ/1000)*T)); %[m2/s] Hydrogen diffusivity in 316 SS from Tanabe, 1984 (Not specifically called 316 L, assume it is standard 316)
    Dp_hot = 6.32E-7*exp(-47.8/((R_univ/1000)*T_out));  %[m2/s] Hydrogen diffusivity in 316 SS (Not specifically called 316 L, assume it is standard 316)  This is used to help set the size of the time step.
    %Dp_temp = 1.36E-7*exp(-37.7*1000/(R_univ*T));  %Hydrogen diffusion coefficient for Inconel 600 from 2011 MIT NSE Design Class [m^2/s]
    %Dp_temp = 4.62E-8*exp(36.6*1000/(R_univ*T));  %Hydrogen diffusion coefficient for Alloy X750 from 2011 MIT NSE Design Class  [m^2/s]
    %Dp_temp = 3.5E-8*exp(-42.42/((R_univ/1000)*T));  %[m^2/s] D transport in 304 L from Kasuta, 1981
    %Dp_temp = 7.33E-7*exp(-52.30/((R_univ/1000)*T)); %[m^2/s] H transport in 321 from Kasuta, 1981

%Sets hydrogen diffusion coefficient for metals in secondary HX (if different from primary Hx)
    %Ds_temp = 2E-36*(T^9.2887);  %[m^2/s] H transport in 316 L, From fit to data from Lee,2011    
    Ds_temp = 6.32E-7*exp(-47.8/((R_univ/1000)*((T_out+T_in)/2))); %[m2/s] Hydrogen diffusivity in 316 SS (Not specifically called 316 L, assume it is standard 316)
    %Ds_temp = 1.36E-7*exp(-37.7*1000/(R_univ*T));  %Hydrogen diffusion coefficient for Inconel 600 from 2011 MIT NSE Design Class [m^2/s]
    %Ds_temp = 4.62E-8*exp(36.6*1000/(R_univ*T));  %Hydrogen diffusion coefficient for Alloy X750  from 2011 MIT NSE Design Class [m^2/s]
    %Ds_temp = 3.5E-8*exp(-42.42/((R_univ/1000)*T));  %[m^2/s] D transport in 304 L from Kasuta, 1981
    %Ds_temp = 7.33E-7*exp(-52.30/((R_univ/1000)*T)); %[m^2/s] H transport in 321 from Kasuta, 1981

