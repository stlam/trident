function F1 = HX1exit(var1, Dp, DX, k_transp2, k_henryT22, k_sievert, Moles_finalelement, HX1_element_sa1, DT, Hx_mesh, CoeffA, mt2_involel, v_dot2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This function sets up a system of equations to solve the boundary
%condition for the transport of T2 out of the primary heat exchanger tube
%wall into a secondary (intermediate) system coolant.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


F1 = [(2*Dp/DX)*(var1(1) - var1(2)) - k_transp2*(var1(3) - var1(4));  %var1(1) = Avg2, var1(2) = Csm2, var1(3) = Csw2, var1(4) = Csb2
      var1(3) - k_henryT22*(var1(2)/k_sievert)^2;  
      var1(1) - (Moles_finalelement - k_transp2*(var1(3) - var1(4))*(HX1_element_sa1/Hx_mesh)*DT)/CoeffA;
      var1(4) - (mt2_involel + (2*Dp/DX)*(var1(1) - var1(2))*(HX1_element_sa1/Hx_mesh)*DT)/(v_dot2*DT)]; 

     
      
   
   