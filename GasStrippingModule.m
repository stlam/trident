function [mt_T2, mt_TF, y_1_T2, y1_TF, mole_T2_offgas, x_N_TF, mole_TF_offgas] = GasStrippingModule(NStages, T, T_avg, ...
    Mole_coolant, m_dotm, mt_TF, mt_T2, G, DT, Loop_Selector)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function contains a gas stripping model for the countercurrent gas  
% stripping of of TF and T2 from the salt by an inert gas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T = T - 273.15; %Convert temperature from Kelvin into Celsius
if Loop_Selector == 1  %Primary loop, currently flibe
    k_Henry_T2 = 3.923E-8*exp(0.004483*T); %From Malinauskas requires T in Celsius [mole T2/mole melt-atm]
elseif Loop_Selector == 2  %Secondary loop, currently flinak
    dens_flinak = 2579.3 - 0.624*(T_avg);  %[kg/m3] from Sohal, 2010 [Temp in K] 
    MM_flinak = 41.29/1000;  %[kg/mol] Molar mass for Flinak (0.465LiF-0.115NaF-0.42KF)
    k_Henry_T2 = 3.98E-7*exp(34400/(8.314*(T_avg)));  %[mol/m3-Pa] for flinak from Fukada expression modified to 34400 instead of 34.4 in the exponent. Requires temperature in Kelvin
    k_Henry_T2 = k_Henry_T2*(1/(9.86923E-6))*(1/dens_flinak)*MM_flinak; %Converted to units of [mol T2/mol melt-atm]
end
    k_Henry_TF = 24.716E-4*exp(-0.004014*T);  %From Shaffer requires T in C [mole T2/mole melt-atm]

%Convert concentrations [mol/m3] to concentrations [mol TF or T2/mol flibe]
x_0_T2 = mt_T2/Mole_coolant;  %Concentration of T2 in coolant entering stripper [mole T2/mole flibe]
x_0_TF = mt_TF/Mole_coolant;  %Concentration of TF in coolant entering stripper.  Currently set based on the ratio required for constant standard MSRE redox with T2.

m_T2 = 1/k_Henry_T2;  %Slope of equilibrium line given by 1/k_Henry
m_TF = 1/k_Henry_TF;  %Slope of equilibrium line
b_T2 = 0;  %Intercept of equilibrium line
b_TF = 0;  %Interecept of equilibrium line

%Specifications of inlet and outlet stream
y_N1_TF = 0;  %Concentration of TF in the inlet stripping gas
y_N1_T2 = 0;  %Concentration of T2 in the inlet stripping gas
x_Ns_T2 = (y_N1_T2 - b_T2)/m_T2;  %From Wankat p. 491 equation 15-41
x_Ns_TF = (y_N1_TF - b_TF)/m_TF;  %From Wankat p. 491 equation 15-41
%x_0s_T2 = (y1_T2 - b_T2)/m_Ts;  %From Wankat p. 491 equation 15-41
%x_0s_TF = (y1_TF - b_TF)/m_TF;  %From Wankat p. 491 equation 15-41
L = m_dotm;  %Flibe flow-rate [mole coolant/s] (Before m_dotm is sent into this function, it is multiplied by a flow fraction from the input file which may vary from 0 to 1)
y_1s_TF = m_TF*x_0_TF + b_TF;  %From Wankat equation 15-35

%Calculate conentration of T2 in coolant exiting the stripper for fixed gas flow rate and number of stages specified in the input file 
x_N_T2 =((1-(m_T2*G/L))/(1 - (m_T2*G/L)^(NStages+1)))*(x_0_T2 - x_Ns_T2)+x_Ns_T2;  %Concentration of T2 in coolant exiting stripper [mol T2/mol coolant] From equation 15-39 from Wankat Ch. 15
y_1_T2 = m_T2*(x_0_T2-((L/(m_T2*G))^-NStages)*(x_N_T2 - x_Ns_T2))+b_T2;  %Concentration of T2 in the exiting gas [mole T2/mole gas] From solving 15-40 for x_o* and subbing 15-41
mole_T2_offgas = (x_0_T2 - x_N_T2)*(L*DT);  %Moles of T2 sent to offgas in the current time step. (L*DT equals the number of moles of coolant to have passed through the stripper in a single time step)
mt_T2 = mt_T2 - mole_T2_offgas;  %Calculate new moles of T2 in primary coolant after stripping
if mt_T2 < 0
    mt_T2 = 0;
else
end

%Calculate concentration of TF exiting the stripper for fixed gas flow rate and number of stages specified in the input file
x_N_TF = ((1-(m_TF*G/L))/(1-(m_TF*G/L)^(NStages+1)))*(x_0_TF - x_Ns_TF) + x_Ns_TF;  %Concentration of TF in coolant exiting the stipper [mol TF/mole coolant]. From Wankat Eq 15-39.  
y1_TF = -(((L/(m_TF*G)) - (L/(m_TF*G))^(NStages+1))/(1-(L/(m_TF*G))^(NStages+1)))*(y_N1_TF - y_1s_TF)+y_N1_TF;  %Concentration of TF in exiting stripping gas [mol TF/mol gas] from solving 15-31
mole_TF_offgas = (x_0_TF - x_N_TF)*L*DT;  %Moles of TF sent to offgas in the current time step
mt_TF = mt_TF - mole_TF_offgas;  %Moles of TF in primary after stripping

end




