function FS = permentr_secondary(var1s, Dperms, SPX, k_masspipe_T2_perm2, k_henryT22, k_perm2_sievert, Moles_firstselement, Perm2_element_sa, DT, Permeator_number_s, Perms_element_volume, mt2_T2_Perm2_in, v_dot2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function sets up a system of non-linear equations for tritum 
% transport from the salt into the metal at the upstream side of the 
% secondary permeator which can be solved by the MATLAB function 'fsolve'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


FS = [k_masspipe_T2_perm2*(var1s(4) - var1s(3)) - (2*Dperms/SPX)*(var1s(2) - var1s(1));  %var1a(1) = Avg1, var1a(2) = Cm1, var1a(3) = Csw, var1a(4) = Csb
       var1s(3) - k_henryT22*(var1s(2)/k_perm2_sievert)^2;
       var1s(1) - (Moles_firstselement + k_masspipe_T2_perm2*(var1s(4) - var1s(3))*(Perm2_element_sa)*(DT/Permeator_number_s))/Perms_element_volume;  %CoeffA is the volume of the first element
       var1s(4) - (mt2_T2_Perm2_in - (2*Dperms/SPX)*(var1s(2) - var1s(1))*(Perm2_element_sa)*(DT/Permeator_number_s))/(v_dot2*(DT/Permeator_number_s))];
