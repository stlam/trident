%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            |||    |||    |||
%                            |||    |||    |||
%                            |||    |||    |||
%                            |||    |||    |||
%                             \\\   |||   ///
%                              \\\  |||  ///
%                               \\\ ||| ///
%                                \\\|||///
%                                 \\|||//
%                                  \|||/
%                                   |||
%                                   |||
%                                   |||
%                                   |||
%                                   |||
%                                   |||
%                                   |||
%  #######  ########     #######  #######     #######  ##     #  #######
%     #     #       #       #     #      #    #        ##     #     #
%     #     #      #        #     #       #   #        # #    #     #
%     #     ########        #     #        #  ######   #  #   #     #
%     #     #      #        #     #        #  #        #   #  #     #
%     #     #       #       #     #       #   #        #    # #     #
%     #     #        #      #     #      #    #        #     ##     #
%     #     #        ##  #######  #######     #######  #     ##     #
%
%              TRItium Diffusion EvolutioN and Transport v1.1
%                                   by
%                             John D. Stempien
%             MIT, Department of Nuclear Science and Engineering
%                           Completed May 2015
%                          Last Updated May 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Code Description:  Calculates tritium birth rates and concentrations in
%  the coolant loops of the FHR.  Simulates corrosion reactions of TF with
%  Cr and selective leaching of Cr from the structural metals in the
%  system.  Options exist to simulate tritium removal systems.
%  
%  Now with fix to error in graphite capture bed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Use run_TRIDENT.m to initialize program
tic

if Feedbackflag == 3 && Corrosionflag == 1
    disp('Warning with Feedbackflag = 3 and Corrosionflag = 1 all tritium will be TF.')
    disp('Changing Corrosionflag to 2 turns on corrosion and allows T2 to be generated from corrosion of Cr by TF')
    disp('Changing Feedbackflag to 2 fixes the redox potential and assures that T2 and TF exist in relative quatities consistent with the chosen redox potential.')
else
end

%Primary coolant
Delta_T = T_out-T_in;  %Reactor temperature rise across the core
%Time constants:
Hoursperday = 24;      %number of hours per day
Secperday = 86400;     %seconds in one day
Secperhour = 3600;     %seconds in one hour

if Redoxflag == 1  %Redoxflag = 1 assumes all tritium is T2 all the time
    Feedbackflag = 3;  %Ensure that redox calculations are not applied
    Corrosionflag = 1; %Ensure that corrosion model is off
else
end

%Set Physical Constants:
R_univ = 8.31446;       %Universal gas constant [J/mol-K]
R_igl = 8.20575e-5;     %Gas consant for ideal gas law [m^3-atm/mol-K]
N_A = 6.022e23;         %Avogadro's number
k_Boltzmann = 8.617E-5; %Boltzmann constant [eV/K]
Surf_Frac = 0.01;       %Fraction of T at surface compared to bulk  

%Temperature independent flibe properties
Flibe_MM = 32.8931;   %Molar mass of flibe with 99.995% Li-7 [g/mol] from doing 0.67*MM_LiF + 0.33*MM_BeF2 
cp_flibe = 2390;     %Flibe heat capacity, Benes 2012[J/kg-K]

cp_flinak = 976.78+1.0634*T_avg;  %[J/kg-K] Takes temp in K. Flinak heat capacity from Sohal, 2010 Engineering Database of Liquid Salt Thermophysical Properties 

%Reactor and fuel Dimensions
%Fuel Pebble and TRISO properties
TRISO_vol = (4/3)*pi*((Kernel_d/2)+Buffer_t+IPyC_t+SiC_t+OPyC_t)^3; %TRISO particle volume [m^3]
Pebble_vol = (4/3)*pi*Pebble_radius^3;  %Individual pebble volume [m^3]
FPebble_graph_vol = Pebble_vol - TRISOperPebble*TRISO_vol; %Volume of graphite only in a single fueled pebble [m^3]
Gpebble_graph_vol = Pebble_vol;   %Volume of graphite in a single graphite pebble [m^3]

% Reactor Core geometry   
Gpebblezone1_inradius = CentralRef_radius;      %First graphite-only pebbles zone inner radius [m]
Gpebblezone1_outradius = Fuelzone_innerradius;  %First graphite-only pebbles zone outer radius [m]
Gpebblezone2_inradius = Fuelzone_outerradius;   %Second graphite-only pebbles zone inner radius [m]
Gpebblezone2_outradius = OuterRef_inradius;     %Second graphite-only pebbles zone outer radius [m]
CentralRef_sa = pi*Core_height*2*CentralRef_radius;  %Central reflector surface area [m2]
OuterRef_sa = pi*Core_height*2*OuterRef_inradius;    %Outer reflector flibe-facing surface area [m2]
Fuelzone_vol = pi*Core_height*(Fuelzone_outerradius^2 - Fuelzone_innerradius^2);  %Volume of the fueled zone in the core [m3]
Graphitepebblezone_vol = pi*Core_height*((Gpebblezone2_outradius^2 - Gpebblezone2_inradius^2)+(Gpebblezone1_outradius^2 - Gpebblezone1_inradius^2));  %Volume of unfueled, graphite-only pebbles
Flowarea = pi*(OuterRef_inradius^2 - CentralRef_radius^2);  %Core cross sectional area for flow (if the core were emtpy with no pebbles)
Core_vol_empty = Flowarea*Core_height;      %Free volume in core if no pebbles in it [m3]
m_dot = (Rx_power*10^6)/(cp_flibe*Delta_T); %Primary coolant mass flow rate [kg/s]
Molar_dot = m_dot/(Flibe_MM/1000);  %Coolant molar flow rate [moles coolant/s]
m_dot2 = (Rx_power*10^6)/(cp_flinak*Delta_T); %Secondary coolant mass flow rate [kg/]

%Pebble packing in the core and calculation of number of pebbles in reactor core
Void_frac = 1 - PF;  %Core pebble void fraction is 1 - Packing Fraction
Fpebbles = (Fuelzone_vol/Pebble_vol)*PF;            %Number of fuel pebbles in core
Gpebbles = (Graphitepebblezone_vol/Pebble_vol)*PF;  %Number of graphite-only (un-fueled) pebbles in the core
Core_coolant_vol = Core_vol_empty - (Fpebbles + Gpebbles)*Pebble_vol;  %Coolant volume in core [m3]


if CoreGeometryAdjust == 2
    Fpebbles = N_CoreFuelPebbles;  %If the number of fuel pebbles is specified
    Gpebbles = N_CoreGrapPebbles;
else  %If number of pebbles is not specified, TRIDENT will calculate it
    Fpebbles = (Fuelzone_vol/Pebble_vol)*PF;            %Number of fuel pebbles in core
    Gpebbles = (Graphitepebblezone_vol/Pebble_vol)*PF;  %Number of graphite-only (un-fueled) pebbles in the core
end


%Graphite properties
Tgraphite = 2.3E16;        %[atoms tritium/m2] POCO graphite specific tritium capapcity [atoms/m^2] where m^2 should be in BET terms.  This (average of unreacted carbon) and BET come from Strehlow 1986 table II at pressure 0.14 Pa
BET_graphite = 0.2465;     %BET surface area of POCO graphite [m^2/g] From Strehlow 1986.  Also see Smolik and Anderl 1997.
graphite_density = 1.77E6;  %From Atsumi, 2000 for IG-110U [g/m^3]
Totalpebblegraphitemass = (FPebble_graph_vol*Fpebbles+Gpebble_graph_vol*Gpebbles)*graphite_density;  %Total mass of graphite due to pebbles in the core [g]
Totalpebblegraphitevolume = Totalpebblegraphitemass/graphite_density;  %[m3] total graphite pebble volume in the core
BETgraphite_sa = Totalpebblegraphitemass*0.2465;  %Total BET surface area of all graphite in both the fuel and the un-fueled pebbles combined [m^2]
graphite_sa = (Fpebbles+Gpebbles)*(4*pi*Pebble_radius^2);   %Total geometric surface area of all the pebbles in the core [m^2]  


%Reactor Primary and Secondary Pipe Parameters
hot_pipe_vol = pi*pipe_l*(pipe_d/2)^2;     %[m3] primary hot leg pipe coolant volume
cold_pipe_vol = pi*pipe_l2*(pipe_d2/2)^2;    %[m3] primary cold leg pipe coolant volume
hot_pipe_vol2 = pi*pipe_l*(pipe_d/2)^2;  %[m3] secondary hot leg pipe coolant volume
cold_pipe_vol2 = pi*pipe_l2*(pipe_d2/2)^2; %[m3] secondary cold leg pipe coolant volume

%Heat exchanger properties, coolant volumes, borrowed from Berkeley in MK1-PB-FHR paper and/or calculated in the code
Hx_tube_outercir = pi*Hx_tube_od;     %[m] tube outer circumference
DX = Thick/Elements;  %Thickness of each finite difference slice in HX tube wall [m]
Hx_tube_ir = Hx_tube_od/2 - Thick;    %[m] inner radius of Hx tube
Hx_tube_flowarea1 = pi*Hx_tube_ir^2;  %[m2] area of individual Hx tube
if TubeNumber == 0  %If the number of tubes is not specified, calculate them assuming the total HX flow area matches that of the main leg pipes
    N_Hx_tubes1 = (pi*(pipe_d/2)^2)/Hx_tube_flowarea1;  %Primary side number of individual Hx tubes required to keep same flow area as in hot leg pipe
    N_Hx_tubes2 = (pi*(pipe_d2/2)^2)/Hx_tube_flowarea1; %Secondary side number of individual Hx tubes required to keep same flow area as in hot leg pipe
else
    N_Hx_tubes1 = Hx1tubes;
    N_Hx_tubes2 = Hx2tubes;
end
Length1_tot = A1/Hx_tube_outercir;  %[m] total length of HX1 tubing
Length2_tot = A2/Hx_tube_outercir;  %[m] total length of HX2 tubing
%From MK1-PB-FHR Table 3-2, 252697m of tubing in 18.5 m high CTAH
Hx_height1 = Length1_tot/N_Hx_tubes1;   %[m] Hx 1, distance from Hx inlet to Hx outlet within a single tube
Hx_height2 = Length2_tot/N_Hx_tubes2;   %[m] Hx 2, distance from Hx inlet to Hx outlet within a single tube
Hx1_vol = pi*Length1_tot*Hx_tube_ir^2;  %[m3] volume of flibe coolant in Hx1
Hx2_vol = pi*Length2_tot*Hx_tube_ir^2;  %[m3] volume of flinak coolant in Hx2
HX1_element_vol = zeros(Elements:1);    %[m3] volume of metal in each radial slice of HX1 tube wall
HX2_element_vol = zeros(Elements:1);    %[m3] volume of metal in each radial slice of HX2 tube wall
HX1_element_sa = zeros(Elements:1);     %[m2] surface area of each radial slice
HX2_element_sa = zeros(Elements:1);     %[m2] surface area of each radial slice
HX2_tube_vol = pi*Length2_tot*((Hx_tube_od/2)^2 - (Hx_tube_ir)^2);  %[m3] volume of metal HX2 tube wall 
for j = 1:1:Elements
    HX1_element_vol(j,1) = pi*Length1_tot*((Hx_tube_ir+j*DX)^2 - (Hx_tube_ir + (j-1)*DX)^2);  %[m3] Volume of metal in each radial slice of the HX1 tube wall over the entire axial length
    HX2_element_vol(j,1) = pi*Length2_tot*((Hx_tube_ir+j*DX)^2 - (Hx_tube_ir + (j-1)*DX)^2);  %[m3] Volume of metal in each radial slice of the HX1 tube wall over the entire axial length
end

HX1_element_sa(1,1) = 2*pi*Length1_tot*Hx_tube_ir;
HX2_element_sa(1,1) = 2*pi*Length2_tot*Hx_tube_ir;
for j = 1:1:Elements
    HX1_element_sa(j+1,1) = 2*pi*(Hx_tube_ir+j*DX)*Length1_tot;  %[m2] HX1_element surface area of each radial segment
    HX2_element_sa(j+1,1) = 2*pi*(Hx_tube_ir+j*DX)*Length2_tot;  %[m2] HX2_element surface area of each radial segment
end

if Oxideflag == 2;
     PRF = PRFinput;  %permeabililty reduction factor due to oxide scale on SHX
                %A reduction by a factor of 10 is common consensus for 316 
else PRF = 1;
end
if Loops == 2  %If two loops are modeled, no permeation reduction factor can be applied to the primary heat exchanger
    PRFLoops1 = 1;
else %If loops equals 1, a PRF can be applied to the primary HX
    PRFLoops1 = PRFinput;
end

%Primary graphite bed for tritium capture allocations and definitions
%Values calculated from graphite bed input variables
    Bed_surface_area = 3*Bed_vessel_length*pi*Bed_vessel_radius^2*Bed_packingfraction/Particle_radius;  %[m2] bed surface area
    Particle_vol = (4/3)*pi*Particle_radius^3;  %[m3] volume per graphite particle/pebble
    Particle_sa = 4*pi*Particle_radius^2;  %[m2] surface area per particle
    Particle_number = Bed_surface_area/Particle_sa;  %number of particles in the bed
    Particle_mass = Particle_density*Particle_vol;  %[g] grams per particle
    Bed_mass = Particle_mass*Particle_number;  %[g] mass of graphite in the bed
    Bed_voidfraction = 1-Bed_packingfraction;  %Graphite bed void fraction.  Fraction of space that is empty
    Bed_flowarea = pi*Bed_vessel_radius^2;  %Free cross sectional area of the bed vessel [m2]
    Bed_volume = Bed_flowarea*Bed_voidfraction*Bed_vessel_length;  %[m3] this represents the volume of coolant that would fill the graphite absorber bed
    
%Secondary graphite bed for tritium capture allocations and definitions
%Values calculated from graphite bed input variables
    Particle_vol_s = (4/3)*pi*Particle_radius_s^3;  %[m3] volume per graphite particle/pebble
    Particle_sa_s = 4*pi*Particle_radius_s^2;  %[m2] surface area per particle
    Particle_number_s = Bed_surface_area_s/Particle_sa_s;  %number of particles in the bed
    Particle_mass_s = Particle_density_s*Particle_vol_s;  %[g] grams per particle
    Bed_mass_s = Particle_mass_s*Particle_number_s;  %[g] mass of graphite in the bed
    Bed_voidfraction_s = 1-Bed_packingfraction_s;  %Graphite bed void fraction.  Fraction of space that is empty
    Bed_vessel_length_s = (Particle_number_s*Particle_vol_s)/(Bed_packingfraction_s*pi*Bed_vessel_radius_s^2); %[m] length of packed bed
    Bed_flowarea_s = pi*Bed_vessel_radius_s^2;  %Free cross sectional area of the bed vessel [m2]
    Bed_volume_s = Bed_flowarea_s*Bed_voidfraction_s*Bed_vessel_length_s;  %[m3] this represents the volume of coolant that would fill the graphite absorber bed

%Primary (P) and Secondary (S) permeation window variable definitions
PPX = WindowThick_p/PermElements;  %Thickness of each element in the permeator wall [m]
SPX = WindowThick_s/PermElements;  %Thickness of each element in the secondary permeator wall [m]
CPermp = zeros(1,PermElements);  %Concentration of T2 in each radial slice of the primary permeator wall
CPerms = zeros(1,PermElements);  %Concentration of T2 in each radial slice of the secondary permeator wall
Permp_tube_length = WindowArea_p/(pi*Permp_tube_od);  %[m] Total length of tubing in primary permeator in order that the outer surface area = WindowArea_p
Perms_tube_length = WindowArea_s/(pi*Perms_tube_od);  %[m] Total length of tubing in the secondary permeator
Permp_tube_ir = (Permp_tube_od/2) - WindowThick_p;  %[m] primary permeator tube inner radius
Perms_tube_ir = (Perms_tube_od/2) - WindowThick_s;  %[m] secondary permeator tube inner radius
Permp_tube_flowarea = pi*Permp_tube_ir^2; %[m2] flow area inside a single primary permeator tube
Perms_tube_flowarea = pi*Perms_tube_ir^2; %[m2] flow area inside a single secondary permeator tube

if NumPermptubes_opt ==2  %If this equals 2, the number of tubes in the permeator is specified in the input file
    NumPermptubes = NumberofPermeatorTubes;
else
    NumPermptubes = (pi*(pipe_d/2)^2)/Permp_tube_flowarea;  %number of primary permeator tubes
end

NumPermstubes = (pi*(pipe_d/2)^2)/Perms_tube_flowarea;  %number of secondary permeator tubes
Permp_height = Permp_tube_length/NumPermptubes;  %Height of Primary permeator [m]
Perms_height = Perms_tube_length/NumPermstubes;  %Height of secondary permeator [m]
Permp_coolant_vol = Permp_tube_flowarea*Permp_tube_length;  %[m3] volume of coolant inside primary permeator
Perms_coolant_vol = Perms_tube_flowarea*Perms_tube_length;  %[m3] volume of coolant inside secondary permeator
Permp_element_vol = zeros(PermElements,1);  %[m3] primary permeator radial element volumes
Perms_element_vol = zeros(PermElements,1);  %[m3] secondary permeator radial element volumes
Permp_element_sa = zeros(PermElements+1,1); %[m2] primary permeator surface areas at each radial node
Perms_element_sa = zeros(PermElements+1,1); %[m2] secondary permeator surface areas at each radial node
Permeator_number_p = 0;
Permeator_number_s = 0;
    
%  Primary flibe coolant volume and flibe inventory
Vol_1 = Hx1_vol + hot_pipe_vol +cold_pipe_vol + Core_coolant_vol;  %[m3] baseline primary coolant volume
if Tritiumcapturebedflag == 2
    Vol_1 = Vol_1 + Bed_volume;  %[m^3]
else
end
if PermeationFlag_primary == 2
    Vol_1 = Vol_1 + Permp_coolant_vol;  %[m3] volume of coolant in the primary system
else
end
dens_flibe = 2415.6-0.49072*T_avg;  %Flibe density from Janz correlation in Sohal 2010 [kg/m^3].  Uses T [K] from loop_parameters input file (average coolant Temp).
Mole_flibe = (Vol_1*dens_flibe*1000)/Flibe_MM;  %[mole]  total moles of flibe in primary

% Secondary loop flinak coolant volume
Vol_2 = Hx2_vol + cold_pipe_vol2 + hot_pipe_vol2;  %[m^3]
MM_flinak = 41.29;  %[g/mol] Molar mass for Flinak (0.465LiF-0.115NaF-0.42KF)
dens_flinak = 2579.3 - 0.624*T_avg;  %[kg/m3] from Sohal, 2010 (Temp is in K)
Mole_flinak = dens_flinak*Vol_2*(1000/MM_flinak);  %Moles of flinak in secondary loop [mol]
cp_flinak = 976.78 + 1.0634*(T_in+Delta_T/2);  %Specific heat capacity [J/kg-K] from Janz correlation in Sohal, 2010 INL/EXT-10-18297
m_dot_secondary = (Rx_power*10^6)/(cp_flinak*Delta_T); %Secondary coolant mass flow rate [kg/s];

if PermeationFlag_secondary == 2
    Vol_2 = Vol_2 + Perms_coolant_vol;  %[m3] volume of coolant in the secondary system if a permeation window is used
else
end

%Tritium production rate
if Tritiumproductionflag == 1
    t = 0;  %[sec] Calculate BOL generation rate at t = 0 seconds
    [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);  %Use t = 0 to get tritium birth rate for BOL
elseif Tritiumproductionflag == 2
    t = 20*365*86400;  %[sec] Calculate equilibrium generation rate at 20 years
    [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);  %Use large t to get equilibrium tritium birth rate
else
end


%Call function "temperatureprofile" in order to construct the vector Temp_profile (K)
[T_core, T_hot, T_Hx, T_cold, Temp_Profile] = temperatureprofile(T_in, qo, m_dot, cp_flibe, Core_height, Core_mesh, T_out, Hx_height1, Hot_mesh, Cold_mesh, Hx_mesh);

n_axial = Core_mesh + Hot_mesh + Hx_mesh + Cold_mesh;  %Total number of axial segments in the primary system = number of segments over which things need to be calculated
dx_core = Core_height/Core_mesh;  %[m] thickness of each axial core slice
dx_hot = pipe_l/Hot_mesh;         %[m] thickness of each axial hot leg slice
dx_hx1 = Hx_height1/Hx_mesh;      %[m] thickness of each axial slice of the primary Hx
dx_cold = pipe_l2/Cold_mesh;      %[m] thickness of each axial slice of cold leg

%Generate Vector of Positions within the primary loop
axial_position_core = zeros(Core_mesh,1);
axial_position_hot = zeros(Hot_mesh,1);
axial_position_hx = zeros(Hx_mesh,1);
axial_position_cold = zeros(Cold_mesh,1);
Primary_loop_length = Core_height + pipe_l + pipe_l2 + Hx_height1;   %[m] total axial length of the primary sytem
for j = 1:1:Core_mesh
    axial_position_core(j,1) = (dx_core/2)+(j-1)*dx_core;
end
for j = 1:1:Hot_mesh
    axial_position_hot(j,1) = Core_height + (dx_hot/2)+(j-1)*dx_hot;
end
for j = 1:1:Hx_mesh
    axial_position_hx(j,1) = pipe_l + Core_height + (dx_hx1/2)+(j-1)*dx_hx1;
end
for j = 1:1:Cold_mesh
    axial_position_cold(j,1) = pipe_l2 + pipe_l + Core_height + (dx_cold/2)+(j-1)*dx_hot;
end
axial_position = [axial_position_core; axial_position_hot; axial_position_hx; axial_position_cold];  %axial positions [m] along the primary loop
axial_frac_position = axial_position/Primary_loop_length;  %fractional distance along the primary system

%Define all temperature dependent parameters
%Loop is polythermal and is divided into different axial segments with varying temperatures
    %pre-allocating temperature-dependent variables
    v_dot_o = zeros(n_axial,1);
    k_HenryT2_o = zeros(n_axial,1);  
    k_HenryTF_o = zeros(n_axial,1);
    k_masspebble_T2_o = zeros(n_axial,1);
    k_masspebble_TF_o = zeros(n_axial,1);
    k_masspipe_T2_o = zeros(n_axial,1);
    k_masspipe_TF_o = zeros(n_axial,1);
    k_Cr_masspipe_o = zeros(n_axial,1);
    k_Cr_pebble_o = zeros(n_axial,1);
    Dp_o = zeros(n_axial,1);
    Ds_o = zeros(n_axial,1);
    Sievert_316_primary_o = zeros(n_axial,1);
    Sievert_316_secondary_o = zeros(n_axial,1);
    
    for j = 1:1:n_axial
        T = Temp_Profile(j,1);  %[Kelvin]
        [k_HenryT2_temp, k_HenryTF_temp, k_masspebble_T2_temp, k_masspipe_T2_temp, k_masspipe_TF_temp, k_Cr_pebble_temp, k_Cr_masspipe_temp, Dp_temp, Dp_hot, ...
            Ds_temp, Sievert_316_temp_primary, Sievert_316_temp_secondary, Bed_k_masspebble_T2_temp, Bed_k_Cr_pebble_temp, Bed_pressure_drop, ...
            v_dot_temp, v_dot2_temp, k_henryT22_temp, k_transp2_temp, Bed_k_masspebble_TF_temp, k_masspebble_TF_temp, k_masspipe_T2_perm1_temp, ...
            k_masspipe_T2_perm2_temp, Rey_bed, Rey_bed2, Bed_k_masspebble_T2_temp_s, Bed_pressure_drop2]= temperature_dependent_polythermal(R_univ, ...
            OuterRef_inradius, CentralRef_radius, Hx_tube_ir, N_Hx_tubes1, T_out, T_in, m_dot, m_dot2, Flowarea, Void_frac, Pebble_radius, pipe_d, ...
            pipe_d2, Core_mesh, Hot_mesh, Cold_mesh, Hx_mesh, T, j, Bed_flowarea, Bed_voidfraction, Particle_radius, Bed_vessel_length, T_avg, NumPermptubes, ...
            NumPermstubes, Permp_tube_flowarea, Perms_tube_flowarea, Permp_tube_ir, Perms_tube_ir, Hx_height1, Hx_height2,Permp_height,Perms_height, Bed_flowarea_s, ...
            Particle_radius_s, Bed_voidfraction_s, Bed_vessel_length_s);
        v_dot2 = v_dot2_temp;
        k_henryT22 = k_henryT22_temp;
        k_transp2 = k_transp2_temp;
        v_dot_o(j,1) = v_dot_temp;  %Primary coolant volumetric flow rate [m3/s]
        k_HenryT2_o(j,1) = k_HenryT2_temp;  %Henry's law constants [mol T2/mol salt-atm]
        k_HenryTF_o(j,1) = k_HenryTF_temp;  %Henry's law constants [mol TF/mol salt-atm]
        k_masspebble_T2_o(j,1) = k_masspebble_T2_temp;  %Mass transfer coefficients for T2 to pebble surface
        k_masspebble_TF_o(j,1) = k_masspebble_TF_temp;  %Mass transfer coefficients for TF to pebble surface
        k_masspipe_T2_o(j,1) = k_masspipe_T2_temp;      %Mass transfer coefficients for T2 to pipe surface
        k_masspipe_TF_o(j,1) = k_masspipe_TF_temp;      %Mass transfer coefficients for T2 to pipe surface
        k_masspipe_T2_perm1 = k_masspipe_T2_perm1_temp; %Mass transfer coefficient for T2 to primary permeator inner tube surface
        k_masspipe_T2_perm2 = k_masspipe_T2_perm2_temp; %Mass transfer coefficient for T2 to secondary permeator inner tube surface
        
        k_Cr_masspipe_o(j,1) = k_Cr_masspipe_temp;  %Mass transfer coefficients for Cr2+ to pipe surfaces and reactor core surfaces
        k_Cr_pebble_o(j,1) = k_Cr_pebble_temp;      %Mass transfer coefficients for Cr2+ to pebble surfaces
        Sievert_316_primary_o(j,1) = Sievert_316_temp_primary;      %Sieverts' law constant from Tanabe, 1984 [mol T2/m3 flibe-MPa^0.5]
        Sievert_316_secondary_o(j,1) = Sievert_316_temp_secondary;  %Sieverts' law constant from Tanabe, 1984 [mol T2/m3 flibe-MPa^0.5]
        Dp_o(j,1) = Dp_temp;  %Diffusion coefficient for T2 through the primary (p) Hx metal
        Ds_o(j,1) = Ds_temp;  %Diffusion coefficient for T2 through the secondary (s) HX metal.  All values are currently at Tavg = (T_in + T_out)/2
    end

%%%%%Pre-allocations, initial values, and simple initial calculations:
CT1 = zeros(1, Days*Hoursperday/Hour_Fraction);  %[moles T2/m3]  concentration of T2 in primary.  Not used if Redoxflag = 2
CT2 = zeros(1, Days*Hoursperday/Hour_Fraction);
MT1 = zeros(Days*Hoursperday/Hour_Fraction,1);
MT2 = zeros(Days*Hoursperday/Hour_Fraction,1);
MT3 = zeros(Days*Hoursperday/Hour_Fraction,1);
ReleaseRate_Ci = zeros(Days*Hoursperday/Hour_Fraction,1);
J12 = zeros(1, Days*Hoursperday/Hour_Fraction);
J23 = zeros(1, Days*Hoursperday/Hour_Fraction);
Cm1_bank = zeros(n_axial,1);  %T2 concentration in the metal at HX1 metal/salt boundary [mole T2/m3 metal]  
Cm2_bank = zeros(n_axial,1);  %T2 Concentration at HX1 slice 1/slice2 boundary [mole T2/m3 metal]
CP = zeros(Hot_mesh+Hx_mesh+Cold_mesh+Core_mesh, Elements);  %Concentration of T2 in axial segment j in radial segment i of primary Hx tubing. Think of it as the average concentration in a given volume element of the HX wall
Moles_finalelement = 0;
CS = zeros(1, Elements);        %Concentration of T2 in radial segment i of secondary HX tubing
CPC = 0;                        %Amount of T released to the power cycle.
PP_T2 = 0;          %Partial pressure of T2 over flibe 
PP_TF = 0;          %Partial pressure of TF over flibe
PP_T2_HX_primary = 0; %Partial pressure of T2 over primary HX metal
PP_T2_HX_secondary = 0;  %Partial pressure of T2 over secondary HX metal
PP_T2_coolantbank = zeros(Days*Hoursperday/Hour_Fraction,1);  %[atm] partial pressure of T2 above melt
PP_TF_coolantbank = zeros(Days*Hoursperday/Hour_Fraction,1);  %[atm] partial pressure of TF above melt
PP_T2_HX_primarybank = zeros(Days*Hoursperday/Hour_Fraction,1);  %[atm] partial pressure of T2 over primary HX flibe-facing metal
PP_T2_HX_secondarybank = zeros(Days*Hoursperday/Hour_Fraction,1);  %[atm] particle pressure of T2 over secondary HX flibe-facing metal
Axial_Redox_Potential = zeros(n_axial,1);  %[kJ/mol F2] redox potential of coolant after exiting each axial segment of the primary loop
Axial_Redox_Potential_store = zeros(n_axial,Days*Hoursperday/Hour_Fraction);  %[kJ/mol F2] store the axial redox potential after each fraction of an hour.  The final row is the global redox potential at the end of each time step
Global_Redox = zeros(Days*Hoursperday/Hour_Fraction,1);  %[kJ/mol F2] redox potential in the coolant at the end of each d
HX1_T = zeros(Days*Hoursperday/Hour_Fraction,1);  %[mol T dissolved in HX1 tube walls]
HX2_T = zeros(Days*Hoursperday/Hour_Fraction,1);  %[mol T dissolved in HX2 tube walls]
TritiumBalance = zeros(Days*Hoursperday/Hour_Fraction,1);  %Accounts for all moles of tritium in throughout the system
CumulativeT_bank = zeros(Days*Hoursperday/Hour_Fraction,1);  %Total moles of T produced in the calculation.  The tritium balance must be equal to this.
Ratio = zeros(Days*Hoursperday/Hour_Fraction,1);  %Ratio of produced tritium to TritiumBalance.  Ratio = 1 if all tritium is conserved



for j = 1:1:PermElements
    Permp_element_vol(j,1) = pi*Permp_tube_length*((Permp_tube_ir+j*PPX)^2 - (Permp_tube_ir + (j-1)*PPX)^2);  %[m3] Volume of metal in each radial slice of the primary permeator tube wall over the entire axial length
    Perms_element_vol(j,1) = pi*Length2_tot*((Perms_tube_ir+j*SPX)^2 - (Perms_tube_ir + (j-1)*SPX)^2);  %[m3] Volume of metal in each radial slice of the secondary permeator tube wall over the entire axial length
end

Permp_element_sa(1,1) = 2*pi*Permp_tube_length*Permp_tube_ir;
Perms_element_sa(1,1) = 2*pi*Perms_tube_length*Perms_tube_ir;
for j = 1:1:PermElements
    Permp_element_sa(j+1,1) = 2*pi*(Permp_tube_ir+j*PPX)*Permp_tube_length;  %[m2] Primary permeator surface area of each radial segment
    Perms_element_sa(j+1,1) = 2*pi*(Perms_tube_ir+j*SPX)*Perms_tube_length;  %[m2] Secondary permeator surface area of each radial segment
end

Permeator_p_total = 0;  %Total moles of T2 removed from the coolant in the primary permeator
Permeator_s_total = 0;
C_T2_permeatorexit_p = 0;
C_T2_permeatorexit_s = 0;
J_permeator_p = 0;
J_permeator_s = 0;
Permeator_total_p_store = zeros(Days*Hoursperday/Hour_Fraction,1);
C_T2_permeatorexit_p_store = zeros(Days*Hoursperday/Hour_Fraction,1);
J_permeator_p_store = zeros(Days*Hoursperday/Hour_Fraction,1);
Permeator_total_s_store = zeros(Days*Hoursperday/Hour_Fraction,1);
C_T2_permeatorexit_s_store = zeros(Days*Hoursperday/Hour_Fraction,1);
J_permeator_s_store = zeros(Days*Hoursperday/Hour_Fraction,1);

%Gas Stripping Pre-Allocations
PrimaryStripGas_TF_Conc = zeros(Days*24/Hour_Fraction,1);  %Concentration of TF in the strip gas from the primary stripper [mol TF/mol gas]
y1_TFp = 0;   %Concentration of TF in the strip gas from the primary stripper during a specific time step [mol TF/mol gas]
PrimaryStripGas_T2_Conc = zeros(Days*24/Hour_Fraction,1);  %Concentration of T2 in the strip gas from the primary stripper [mol T2/mol gas]
y_1_T2_p = 0;  %Concentration of T2 in the strip gas from the primary stripper during a specific time step [mol T2/mol gas]
PrimaryStripTotal_T2 = zeros(Days*24/Hour_Fraction,1);  %Cumulative amount of T2 removed into offgas [mol T2]
mole_T2_poffgas = 0;  %Cumulative amount of T2 removed into the offgas during a specific time step[mol T2]
PrimaryStripTotal_TF = zeros(Days*24/Hour_Fraction,1);  %Cumulative amount of TF removed into offgas [mol TF]
mole_TF_poffgas = 0;  %Cumulative amount of TF removed into the offgas during a specific time step[mol TF]
PrimaryStripCoolantExit_TF = zeros(Days*24/Hour_Fraction,1);   %Concentration of TF in the coolant exiting the stripper [mol TF/mol flibe]
x_N_TF_p = 0;  %Concentration of TF in the coolant exiting the stripper during a specific time step [mol TF/mol flibe]
PrimaryStripCoolantExit_T2 = zeros(Days*24/Hour_Fraction,1);  %Concentration of T2 in the coolant exiting the stripper [mol T2/mol flibe]
T2_PrimaryConc = 0;  %Concentration of T2 in the coolant exiting the stripper for a specific time step [mol T2/mol flibe]

SecondaryStripGas_TF_Conc = zeros(Days*24/Hour_Fraction,1);  %Concentration of TF in the strip gas from the Secondary stripper [mol TF/mol gas]
y1_TFs = 0;   %Concentration of TF in the strip gas from the Secondary stripper during a specific time step [mol TF/mol gas]
SecondaryStripGas_T2_Conc = zeros(Days*24/Hour_Fraction,1);  %Concentration of T2 in the strip gas from the Secondary stripper [mol T2/mol gas]
y_1_T2_s = 0;  %Concentration of T2 in the strip gas from the Secondary stripper during a specific time step [mol T2/mol gas]
SecondaryStripTotal_T2 = zeros(Days*24/Hour_Fraction,1);  %Cmmulative amount of T2 removed into offgas [mol T2]
mole_T2_soffgas = 0;  %Cumulative amount of T2 removed into the offgas during a specific time step[mol T2]
SecondaryStripTotal_TF = zeros(Days*24/Hour_Fraction,1);  %Cumulative amount of TF removed into offgas [mol TF]
mole_TF_soffgas = 0;  %Cumulative amount of TF removed into the offgas during a specific time step[mol TF]
SecondaryStripCoolantExit_TF = zeros(Days*24/Hour_Fraction,1);   %Concentration of TF in the coolant exiting the stripper [mol TF/mol flibe]
x_N_TF_s = 0;  %Concentration of TF in the coolant exiting the stripper during a specific time step [mol TF/mol flibe]
SecondaryStripCoolantExit_T2 = zeros(Days*24/Hour_Fraction,1);  %Concentration of T2 in the coolant exiting the stripper [mol T2/mol flibe]
T2_SecondaryConc = 0;  %Concentration of T2 in the coolant exiting the stripper for a specific time step [mol T2/mol flibe]
mt2_TF = 0;  %Concentration of TF in the secondary system (This will always be zero because TF cannot diffuse from the primary into the secondary, and there is no source of TF in the secondary.)


%%%_______Corrosion/precipitation pre-allocations________%%% 
dens_flibe = 2415.6-0.49072*(T_avg);  %Flibe density at 650 C. From Janz correlation in Sohal 2010 [kg/m^3].
Cr_MM = 51.9961;  %Cr molar mass [g/mol]
C_Cr_actual = C_Cr_initial_ppm*(1E-6)*dens_flibe*1000/Cr_MM;  %Convert initial dissolved Cr from ppm to [mol/m3].  Use flibe density at 650 C.
Cr_coolant_conc = zeros(Days*Hoursperday/Hour_Fraction,1);   %[mol Cr/m3] concentration of Cr2+ dissolved in primary coolant
Moles_Cr_ppt = zeros(n_axial,1);   %[moles Cr] moles of Cr precipitated in each axial segment
GramsCr_persa = zeros(n_axial,1);  %[grams Cr/m2 surface area] Grams of Cr deposited in reactor from mass transport precipitation at each axial position
Crcorrodedaxial = zeros(n_axial, 1);  %Moles of Cr corroded from a given axial position in the loop
Crcorrodedaxial_bank = zeros(n_axial, Days*Hoursperday/Hour_Fraction);  %Store moles of Cr corrded at each axial position after each recording period
Crcorroded_persa = zeros(n_axial, Days*Hoursperday/Hour_Fraction);  %Store mg Cr/cm2 corroded from each axial position after each recording period
Moles_Cr_ppt_tot = zeros(n_axial,Days*Hoursperday/Hour_Fraction);  %Moles of Cr precipitated in each axial position after each hour.  Each column is an hour and each row is an axial position
GRAMSCR_pptpersa = zeros(n_axial, Days*Hoursperday/Hour_Fraction);    %g Cr per m3 precipitated in each axial position after each hour.  Each column is an hour and each row is an axial position
initial = ones(n_axial,1);   %initial = 1 for the initial period of time where corrosion is controlled by the flux of TF to the surface. Then, initial is set to 2 once 
                             %the TF-limited period has ended and the Cr diffusion-limited corrosion has begun.
surface = ones(n_axial,1);   %Selects the surface point. If initial surface point 1 becomes depleted in Cr, then surface will be set to 2 or higher in order to reflect 
                                  %the fact that the surface for Cr has moved.
Slice_vol = zeros(n_axial,slices1+1);  %[m3]  volume of each radial slice at each axial position
Crperslice = zeros(n_axial,slices1+1); %[mol Cr per radial slice of pipe wall at each axial position]
Vol_surf = zeros(n_axial,slices1+1);  %[m3] volume of metal considered to be the surface on pipes
pipe_metvol = zeros(n_axial,1);  %[m3] total volume of metal in the pipe within each axial segment
pipe_sa = zeros(n_axial,1);  %[m2] surface area of pipe facing the primary coolant
grad = zeros(n_axial,1);  %Initial Cr concentration gradient in the pipe walls. Gets updated in CorrosionModule

Conc_bulk = zeros(n_axial,numel(Wtfrac));  %numel(Wtfrac) is number of elements considered in 316 SS at each axial location.  Conc_bulk is time invariant
Atomdens = zeros(n_axial,numel(Wtfrac));
Atomsurf = zeros(n_axial,numel(Wtfrac));
Matomsurf = zeros(n_axial,numel(Wtfrac));
Totmatomsurf = zeros(n_axial,numel(Wtfrac));
Totatomsurf = zeros(n_axial,numel(Wtfrac));
Mole_pipe_i = zeros(n_axial,numel(Wtfrac));

corrosion_time = zeros(n_axial,1);  %[seconds] amount of time a particular axial zone sees corrosion
Cr_dissolved = 0;  %[moles] moles of Cr dissolved by TF in TF-limited corrosion
Cr_coolant = 0;    %[moles/m3] concentration of Cr in the primary coolant
Cr_primary_coolant = zeros(Days,1);  %[moles Cr/m3 primary coolant]
TF_consumed = 0;   %[moles] moles of TF consumed in reaction Cr + 2TF = CrF2 + T2 during TF transport-limited corrosion
T2_generated = 0;  %[moles T2] generated from corrosion reaction
TF_primary_consumed = zeros(Days,1); %[moles TF] consumed in corrosion
T2_primary_generated = zeros(Days,1); %[moles] generated from corrosion
%Calculate mole fraction of elements in the base metal
Grams_metal_el = 100*Wtfrac;  %Grams of each element in 100 g of base metal
Moles_metal_el = zeros(numel(Wtfrac),1);  %Moles of each metal element in piping
for i = 1:1:numel(Wtfrac)
   Moles_metal_el(i,1) = Grams_metal_el(i,1)/MM(i,1);
end
Mole_frac_metal = Moles_metal_el/sum(Moles_metal_el);

for j = 1:1:n_axial
    if j >= 1 && j <= Core_mesh  %In reactor core
        Vol_surf(j,1) = 0;       %Currently not accounting for any corrosion in reactor core
        pipe_metvol(j,1) = 0;
        pipe_sa(j,1) = 0;  %Pipe inner surface area
        for i = 1:1:numel(Wtfrac)  %Order 1-10:  Cr, Fe, Ni, C, Mn, P, S, Si, Mo, N
            Conc_bulk(j,i) = ((Wtfrac(i,1)*Density_metal)/MM(i,1))*1000;  %Bulk concentrations [mol Element/m3 SS].  Does not vary with time.
            Atomdens(j,i) = Conc_bulk(j,i)*N_A;  %Atom density [atoms per m3 stainless steel]
            Totmatomsurf(j,i) = Conc_bulk(j,i)*Vol_surf(j,i);  %initial moles of atoms on the inner surface of the pipe [moles]
            Totatomsurf(j,i) = Totmatomsurf(j,i)*N_A;     %initial number of atoms on inner surface of pipe [atoms]
            Atomsurf(j,i) = Totatomsurf(j,i)/pipe_sa(j,1);  %Atoms per surface area [atoms per m2]
            Matomsurf(j,i) = Atomsurf(j,i)/N_A;        %Moles of atoms per surface area [moles of atoms/m2]
            Mole_pipe_i(j,i) = Conc_bulk(j,i)*pipe_metvol(j,1);  %Total moles of given element in the pipe [mole]
        end
        for i = 1:1:slices1+1    %define radial concentrations within the pipe at each axial segment
            Slice_vol(j,i) = 0;  %Currently not accounting for corrosion in reactor core b/c no metal.  [Volume of each slice m3]
            Crperslice(j,i) = 0;   %[mol Cr per slice of pipe wall]
        end
    elseif j >= Core_mesh +1 && j <= Core_mesh + Hot_mesh  %In hot leg
        Vol_surf(j,1) = pi*(pipe_l/Hot_mesh)*(((pipe_d/2)+Lattice_param)^2 - ((pipe_d/2))^2);  %[m3] volume considered to be the surface in the hot leg;
        pipe_metvol(j,1) = pipe_l*pi*(((pipe_d/2)+pipe_thick1)^2 - (pipe_d/2)^2)/Hot_mesh;  %volume of metal in pipe [m3];
        pipe_sa(j,1) = (1/Hot_mesh)*(pi*pipe_l*pipe_d);  %Pipe inner surface area [m2]
        for i = 1:1:numel(Wtfrac)  %Order 1-10:  Cr, Fe, Ni, C, Mn, P, S, Si, Mo, N
            Conc_bulk(j,i) = ((Wtfrac(i,1)*Density_metal)/MM(i,1))*1000;  %Bulk concentrations [mol Element/m3 SS]
            Atomdens(j,i) = Conc_bulk(j,i)*N_A;  %Atom density [atoms per m3 stainless steel]
            Totmatomsurf(j,i) = Conc_bulk(j,i)*Vol_surf(j,i);  %initial moles of atoms on the inner surface of the pipe [moles]
            Totatomsurf(j,i) = Totmatomsurf(j,i)*N_A;     %initial number of atoms on inner surface of pipe [atoms]
            Atomsurf(j,i) = Totatomsurf(j,i)/pipe_sa(j,1);  %Atoms per surface area [atoms per m2]
            Matomsurf(j,i) = Atomsurf(j,i)/N_A;        %Moles of atoms per surface area [moles of atoms/m2]
            Mole_pipe_i(j,i) = Conc_bulk(j,i)*pipe_metvol(j,1);  %Total moles of given element in the pipe [mole]
        end
        for i = 1:1:slices1+1
            Slice_vol(j,i) = pi*(pipe_l/Hot_mesh)*(((pipe_d/2)+i*slice_thick1)^2 - ((pipe_d/2)+(i-1)*slice_thick1)^2);  %Volume of each slice [m3]
            Crperslice(j,i) = Slice_vol(j,i)*Conc_bulk(1,1);   %[mol Cr per slice of pipe wall]
        end
    elseif j >= Core_mesh + Hot_mesh +1 && j <= Core_mesh + Hot_mesh + Hx_mesh  %%% PRIMARY HEAT EXCHANGER %%%
        Hx_tube_ir = (Hx_tube_od - 2*Thick)/2;  %Hx tube inner radius [m]
        Vol_surf(j,1) = pi*(Length1_tot/Hx_mesh)*((Hx_tube_ir+Lattice_param)^2-Hx_tube_ir^2);  %[m3] volume considered to be the surface in the Hx
        pipe_metvol(j,1) = Length1_tot*pi*((Hx_tube_od/2)^2 - Hx_tube_ir^2)/Hx_mesh;  %volume of metal in pipe [m3];
        pipe_sa(j,1) = (1/Hx_mesh)*(pi*Length1_tot*2*Hx_tube_ir);  %Pipe inner surface area [m2] over the specified axial segment.  Hx_height1 determines axial height of Hx tube bundles
        for i = 1:1:10  %Order 1-10:  Cr, Fe, Ni, C, Mn, P, S, Si, Mo, N
            Conc_bulk(j,i) = ((Wtfrac(i,1)*Density_metal)/MM(i,1))*1000;  %Bulk concentrations [mol Element/m3 SS]
            Atomdens(j,i) = Conc_bulk(j,i)*N_A;  %Atom density [atoms per m3 stainless steel]
            Totmatomsurf(j,i) = Conc_bulk(j,i)*Vol_surf(j,i);  %initial moles of atoms on the inner surface of the pipe [moles]
            Totatomsurf(j,i) = Totmatomsurf(j,i)*N_A;     %initial number of atoms on inner surface of pipe [atoms]
            Atomsurf(j,i) = Totatomsurf(j,i)/pipe_sa(j,1);  %Atoms per surface area [atoms per m2]
            Matomsurf(j,i) = Atomsurf(j,i)/N_A;        %Moles of atoms per surface area [moles of atoms/m2]
            Mole_pipe_i(j,i) = Conc_bulk(j,i)*pipe_metvol(j,1);  %Total moles of given element in the pipe [mole]
        end
        for i = 1:1:slices1+1
            Slice_vol(j,i) = pi*(Length1_tot/Hx_mesh)*((Hx_tube_ir+i*slice_thick1)^2 - (Hx_tube_ir+(i-1)*slice_thick1)^2);  %Volume of each slice [m3]
            Crperslice(j,i) = Slice_vol(j,i)*Conc_bulk(1,1);   %[mol Cr per slice of pipe wall]
        end
    elseif   j >= Core_mesh + Hot_mesh + Hx_mesh +1 && j<= Core_mesh + Hot_mesh + Hx_mesh + Cold_mesh   %Cold Leg
        Vol_surf(j,1) = pi*(pipe_l2/Cold_mesh)*(((pipe_d2/2)+Lattice_param)^2 - ((pipe_d2/2))^2);  %[m3] volume considered to be the surface in the cold leg;
        pipe_metvol(j,1) = pipe_l2*pi*(((pipe_d2/2)+pipe_thick2)^2 - (pipe_d2/2)^2)/Cold_mesh;  %volume of metal in pipe [m3];
        pipe_sa(j,1) = (1/Cold_mesh)*(pi*pipe_l2*pipe_d2);  %Pipe inner surface area [m2]
        for i = 1:1:10  %Order 1-10:  Cr, Fe, Ni, C, Mn, P, S, Si, Mo, N
            Conc_bulk(j,i) = ((Wtfrac(i,1)*Density_metal)/MM(i,1))*1000;  %Bulk concentrations [mol Element/m3 SS]
            Atomdens(j,i) = Conc_bulk(j,i)*N_A;  %Atom density [atoms per m3 stainless steel]
            Totmatomsurf(j,i) = Conc_bulk(j,i)*Vol_surf(j,i);  %initial moles of atoms on the inner surface of the pipe [moles]
            Totatomsurf(j,i) = Totmatomsurf(j,i)*N_A;     %initial number of atoms on inner surface of pipe [atoms]
            Atomsurf(j,i) = Totatomsurf(j,i)/pipe_sa(j,1);  %Atoms per surface area [atoms per m2]
            Matomsurf(j,i) = Atomsurf(j,i)/N_A;        %Moles of atoms per surface area [moles of atoms/m2]
            Mole_pipe_i(j,i) = Conc_bulk(j,i)*pipe_metvol(j,1);  %Total moles of given element in the pipe [mole]
        end
        for i = 1:1:slices1+1
            Slice_vol(j,i) = pi*(pipe_l2/Cold_mesh)*(((pipe_d2/2)+i*slice_thick1)^2 - ((pipe_d2/2)+(i-1)*slice_thick1)^2);  %Volume of each slice [m3]
            Crperslice(j,i) = Slice_vol(j,i)*Conc_bulk(1,1);   %[mol Cr per slice of pipe wall]
        end
    end
end
Conc_slice = repmat(Conc_bulk',n_axial,1); %[mole/m3] Conc_slice will be altered and used in the ratio calculation.  Conc_bulk is time invariant, but Conc_slice can vary.  Initally Conc_slice=Conc_bulk
                                           %each row is an axial segment, each column corresponds to a particular element.  Cr is the first column
Cr_profile = ones(n_axial,slices1+1)*Conc_bulk(1,1);  %[mole/m3] Array storing the Cr concentration profile as a function of depth in the pipe wall.

SystemTotalT = zeros(1, Days*Hoursperday/Hour_Fraction);  %Total moles of T atoms produced in the simulation
MT1_TF = zeros(Days*Hoursperday/Hour_Fraction,1);
MT1_T2 = zeros(Days*Hoursperday/Hour_Fraction,1);
MT1_Total = zeros(Days*Hoursperday/Hour_Fraction,1);
c1 = 0;  %[mole/m3] concentration in primary loop only used if Redoxflag = 1
c2 = 0;  %[mole/m3] concentration in secondary loop.  only used if Redoxflag = 1
mt1 = 0;  %[mole T] tritium in primary.  only used if Redoxflag = 1
Csb_next = 0; %[mole T2/m3] bulk concentration of T2 in primary coolant for use when j > Core_mesh + Hot_mesh + 1
mt2_HXout = 0; %[mole T2] moles T2 in secondary coolant volume element exiting the j segment
mt1_T2_HXout = 0;  %[moles T2] moles of T2 exiting HX volume element denoted by variable j
mt1_TF_HXout = 0;
mt1_TF = 0;  %[moles] TF in primary only used if Redoxflag = 2
mt1_TFnew = 0;  %[mole TF] change in mole TF in primary at beginning of timestep. Only used if Redoxflag = 2
mt1_T2 = 0;     %[mole T2] current mole of T2 in primary at beginning of timestep Only used if Redoxflag = 2
mt1_T2new = 0;  %[mole T2] change in mole of T2 in primary at beginning of timestep Only used if Redoxflag = 2
mt1_total = 0;  %[moles T]  total moles of T atoms in the primary system Only used if Redoxflag = 2
mt2 = 0;  %[moles T2] moles of T2 in secondary loop.  Only T2 can exist in secondary loop
mt3 = 0;  %[moles T2] moles of T2 released to power cycle
mt1_TF_core = 0; %[moles TF] moles of TF in the coolant at each axial zone in the core
mt1_T2_core = 0; %[moles T2] moles of T2 in the coolant at each axial zone in the core
C_Cr_actual_core = 0;  %[moles Cr/m3] concentration of Cr in a coolant volume element as it passes through each axial zone in the core
C_Cr_HXactual = 0;  %[moles Cr/m3] concentration of Cr in a coolant volume element as it passes through each axial zone in the HX
CumulativeT = 0;  %[moles T] total moles of T produced throughout the simulation
j12 = 0;  %flux from primary to secondary
j23 = 0;  %flux from secondary to powercycle
jg_TF = 0;  %TF flux to graphite
jg_T2 = 0;  %T2 flux to graphite
mtgbed_TF = 0;  %Moles TF absorbed on bed graphite
mtgbed_T2 = 0;  %Moles T2 absorbed on bed graphite
TF_bed_limit = 0;   %Solubility limit of TF [mol/g] on adsorbent bed 
T2_bed_limit = 0;     %Solubility limit of T2 [mol/g] on adsorbent bed 
mtgbed2_T2 = 0; %Moles T2 absorbed on bed of graphite in secondary system
CumulativeT2_gbed = 0;  %Total moles of T2 absorbed on bed graphite
CumulativeTF_gbed = 0;  %Total moles of TF absorbed on bed graphite
CumulativeT2_gbed2 = 0; %Total moles of T2 abosrbed on bed of graphite in secondary system
CoreGraphiteCumulativeT = 0; %Total moles of T absorbed on core pebble graphite
CoreGraphiteCumulativeT2 = 0; %Total moles of T2 absorbed on core pebble graphite
CoreGraphiteCumulativeTF = 0;%Total moles of TF absorbed on core pebble graphite
CoreGraphiteCumulativeT_bank = zeros(Days*Hoursperday/Hour_Fraction,1);  %Total moles of T absorbed on core pebble graphite vs time
CoreGraphiteCumulativeT2_bank = zeros(Days*Hoursperday/Hour_Fraction,1); %Total moles of T2 absorbed on core pebble graphite vs time
CoreGraphiteCumulativeTF_bank = zeros(Days*Hoursperday/Hour_Fraction,1); %Total moles of TF absorbed on core pebble graphite vs time
TFonbedbank = zeros(Days*Hoursperday/Hour_Fraction,1);  %Total moles of TF adsorbed on the graphite bed for tritium capture
T2onbedbank = zeros(Days*Hoursperday/Hour_Fraction,1);  %Total moles of T2 adsorbed on the graphite bed for tritium capture
T2onbedbank2 = zeros(Days*Hoursperday/Hour_Fraction,1);  %Total moles of T2 adsorbed on the graphite bed in the secondary system for tritium capture

tperg = zeros(Core_mesh, 1);    %Total atoms of tritium per m^2 graphite if Redoxflag = 1
tperg_TF = zeros(Core_mesh,1);  %Atoms of TF per m2 of graphite if Redoxflag = 2
tperg_T2 = zeros(Core_mesh,1);  %Atoms of T2 per m2 of graphite if Redoxflag = 2
mtg = zeros(Core_mesh,1);  %[moles T] moles of T on graphite if Redoxflag = 1
mtg_TF = zeros(Core_mesh,1);  %[moles TF]  moles of TF absorbed on graphite if Redoxflag = 2
mtg_T2 = zeros(Core_mesh,1);  %[moles of T2] moles T2 absorbed on graphite if Redoxflag = 2

%Calculate length of timestep in seconds based on the Fourier Number
%being equal to 0.5
Fn = 0.5;  %Fourier number
DT = (Fn*DX^2)/Dp_hot;  %Size of time step [sec] set by thickness of elements in the primary HX and the diffusion coefficient on the HX

if PermeationFlag_primary == 2||PermeationFlag_secondary == 2  %If permeators are being modeled, determine whether DT or DT2 is smaller
    DT2 = (Fn*PPX^2)/((7.43E-7)*exp(-44.1/(0.008314*T_out)));  %Time step based on the thickness and diffusion coefficient in the primary permeator.  %H2 diffusion coefficient in Ni.  From Tanabe, 1984
    DT3 = (Fn*SPX^2)/((7.43E-7)*exp(-44.1/(0.008314*T_out)));  %Time step based on the thickness and diffusion coefficient in the secondary permeator.  %H2 diffusion coefficient in Ni.  From Tanabe, 1984
    if DT2 < DT
        rounder = DT/DT2;
        Permeator_number_p = round(rounder);  %Determine how many times DT2 would run for a single DT, then round up to the nearest integer
        DT = DT2;
    else
        Permeator_number_p = 1;  %if DT is limiting from the HX is limiting, Permeator_number = 1
    end
    if DT3 < DT
        rounder2 = DT/DT3;
        Permeator_number_s = round(rounder2);  %Determine how many times DT2 would run for a single DT, then round up to the nearest integer
    else
        Permeator_number_s = 1;  %if DT is limiting from the HX is limiting, Permeator_number = 1
    end
else
end

Steps = Hour_Fraction*3600/DT; %total number of timesteps per output cycle (For example, if you want to record ouput every hour, Hour_Fraction = 1.  If you only want to store output after ever 30 minutes, Hour_fraction = 0.5)


%Determine mode and starting birth rates/fractions for TF and T2
if Redoxflag == 1         %Redox consideration is OFF and everthing is born as T2, there is no corrosion and no consideration of TF
elseif Redoxflag == 2     %Redox ON, tritium born as TF and speciated into TF and T2 based on redox condition
    if Feedbackflag == 1  %Redox is ON, but Feedback is OFF. Calculate a constant rate of TF and T2 generation.  Redox state will drift in the code due to T2 diffusion out of the system, but fraction of tritium born as TF and T2 will remain constant.
        %Tritium production rate.  Birth has units [mol T/s]
        if Tritiumproductionflag == 1
            t = 0;  %[sec] Calculate BOL generation rate at t = 0 seconds
            [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);  %Use t = 0 to get tritium birth rate for BOL
        elseif Tritiumproductionflag == 2
            t = 20*365*86400;  %[sec] Calculate equilibrium generation rate at 20 years
            [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);  %Use large t to get equilibrium tritium birth rate
        else
        end
        MoleT = Birth*DT;  %mole T per time step. Birth has units of [mol T/s].  DT is [sec]  
        %Use concept of fluorine potential to determine speciation of T+ versus T2
        C_total = MoleT/Mole_flibe; %[Mole of T atoms/Mole of flibe]
        C_T2 = (4*C_total*k_HenryT2+(k_HenryTF^2)*(Ratio_TF_T2^2)-sqrt(8*C_total*k_HenryT2*(k_HenryTF^2)*(Ratio_TF_T2^2) ...
            +(k_HenryTF^4)*(Ratio_TF_T2^4)))/(8*k_HenryT2);  %[mole T2/mole flibe] This is a generation rate for per unit dt. This is the solution from squaring and then using quadratic formula
        mt1_T2new = C_T2*Mole_flibe;   %[mole T2]
        mt1_TFnew = (C_total -2*C_T2)*Mole_flibe;  %[mole TF]
    elseif Feedbackflag == 2 %Redox is ON and Feedback ON, TF and T2 calculated later, once in the loop. The redox potential is always fixed, and [T2] vs [TF] is continually adjusted to maintain this redox potential.
        mt1_T2new = 0;  %initial [mole T2]
        mt1_TFnew = 0;  %initial [mole TF]
    elseif Feedbackflag == 3  %Redox is ON, but active feedback is not used.  All tritium is born as TF, then T2 can be generated via corrosion reactions.  This T2 may build up in the salt, diffuse through Hx, or be trapped on graphite.  The redox potential is calculated based on the relative concentrations of T2 and TF in the salt.
        mt1_T2new = 0;  %initial [mole T2]
        mt1_TFnew = 0;  %initial [mole TF]
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Optional restart from file % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if Restart == 2
    Daysnew = Days;
    load(Restartfilename);
    dimensions = size(ReleaseRate_Ci);
    rows = dimensions(1,1);
    d = rows;
    d_in = d + 1;
    Remaining = Daysnew*Hoursperday/Hour_Fraction - rows;  %Major output segments remaining to run
    clear OutputData  %Clear this matrix so it can be re-written
    nCT1 = zeros(1, Remaining);      %[moles T2/m3] concentration of T2 in primary.  only used if Redoxflag = 1
    CT1 = [CT1,nCT1];
    nCT2 = zeros(1, Remaining);      %[mole/m3] concentration in secondary loop.  only used if Redoxflag = 1
    CT2 = [CT2,nCT2];
    nMT1 = zeros(Remaining, 1);     %Moles of tritium in primary.  Not used if Redoxflag = 2
    MT1 = [MT1;nMT1];
    nMT2 = zeros(Remaining, 1);   %Moles of T atoms in secondary.  Only T2 exists in the secondary system
    MT2 = [MT2;nMT2];
    nMT3 = zeros(Remaining, 1);   %Moles of T atoms released to power cycle
    MT3 = [MT3;nMT3];
    nReleaseRate_Ci= zeros(Remaining, 1);  %Tritium rate of release from power cycle after each hour.  Units are [Ci/d]
    ReleaseRate_Ci = [ReleaseRate_Ci;nReleaseRate_Ci];
    nJ12 = zeros(1, Remaining);
    J12 = [J12,nJ12];
    nJ23 = zeros(1, Remaining);
    J23 =[J23,nJ23];
%     nMTG = zeros(1, Remaining);   %Total moles T atoms in graphite (from TF and T2)
%     MTG = [MTG,nMTG];
    nSystemTotalT = zeros(1, Remaining);  %[moles]  total moles of T atoms produced
    SystemTotalT = [SystemTotalT,nSystemTotalT];
%     nOutputData = zeros(Remaining,17);  %Collects 17 out parameters as a function of time
%     OutputData = cat(1,OutputData,nOutputData);  %Appends new rows below each pre-existing set of data
    nAxial_Redox_Potential_store  = zeros(n_axial,Remaining);  %[kJ/mol F2] store the axial redox potential after each fraction of an hour.  The final row is the global redox potential at the end of each time step
    Axial_Redox_Potential_store = cat(2,Axial_Redox_Potential_store,nAxial_Redox_Potential_store);
    nGlobal_Redox = zeros(Remaining, 1);  %[kJ/mol F2] Global redox potential at the end of each time step
    Global_Redox = [Global_Redox;nGlobal_Redox];
    nMoles_Cr_ppt_tot = zeros(n_axial,Remaining);  %Moles of Cr precipitated in each axial position after each hour.  Each column is an hour and each row is an axial position
    Moles_Cr_ppt_tot = cat(2,Moles_Cr_ppt_tot,nMoles_Cr_ppt_tot);
    nGRAMSCR_persa = zeros(n_axial,Remaining);    %Grams/m3 of Cr precipitated in each axial position after each hour
    GRAMSCR_pptpersa = cat(2,GRAMSCR_pptpersa,nGRAMSCR_persa);
    nCrcorrodedaxial_bank = zeros(n_axial,Remaining);  %Moles of Cr corrded from each axial position after each time period.  Each column is a fraction of an hour and each row is an axial position
    Crcorrodedaxial_bank = cat(2,Crcorrodedaxial_bank,nCrcorrodedaxial_bank);
    nCrcorroded_persa = zeros(n_axial,Remaining);  %mg Cr deposited per cm2
    Crcorroded_persa = cat(2,Crcorroded_persa,nCrcorroded_persa);
    nCr_coolant_conc = zeros(Remaining, 1);   %[mol Cr/m3] concentration of Cr2+ dissolved in primary coolant
    Cr_coolant_conc = [Cr_coolant_conc;nCr_coolant_conc];
    nMT1_TF = zeros(Remaining, 1);        %Moles TF in primary
    MT1_TF = [MT1_TF;nMT1_TF];
    nMT1_T2 = zeros(Remaining, 1);        %Moles T2 molecules in primary
    MT1_T2 = [MT1_T2;nMT1_T2];
    nMT1_Total = zeros(Remaining, 1);  %Moles T atoms in primary
    MT1_Total = [MT1_Total;nMT1_Total];
    nHX1_T = zeros(Remaining, 1);  %[mol T dissolved in HX1 tube walls]
    HX1_T = [HX1_T;nHX1_T];
    nHX2_T = zeros(Remaining, 1);  %[mol T dissolved in HX2 tube walls]
    HX2_T = [HX2_T;nHX2_T];
    nTritiumBalance = zeros(Remaining, 1);
    TritiumBalance = [TritiumBalance;nTritiumBalance];
    nCumulativeT_bank = zeros(Remaining, 1);  %Total moles of T produced in the calculation.  The tritium balance must be equal to this.
    CumulativeT_bank = [CumulativeT_bank;nCumulativeT_bank];
    nCoreGraphiteCumulativeT_bank = zeros(Remaining, 1);
    CoreGraphiteCumulativeT_bank = [CoreGraphiteCumulativeT_bank; nCoreGraphiteCumulativeT_bank];
    nCoreGraphiteCumulativeT2_bank = zeros(Remaining, 1);
    CoreGraphiteCumulativeT2_bank = [CoreGraphiteCumulativeT2_bank; nCoreGraphiteCumulativeT2_bank];
    nCoreGraphiteCumulativeTF_bank = zeros(Remaining, 1);
    CoreGraphiteCumulativeTF_bank = [CoreGraphiteCumulativeTF_bank; nCoreGraphiteCumulativeTF_bank];
    nTFonbedbank = zeros(Remaining, 1);
    TFonbedbank = [TFonbedbank; nTFonbedbank];
    nT2onbedbank = zeros(Remaining, 1);
    T2onbedbank = [T2onbedbank; nT2onbedbank];
    nT2onbedbank2 = zeros(Remaining, 1);
    T2onbedbank2 = [T2onbedbank2; nT2onbedbank2];
    nRatio = zeros(Remaining, 1);  %Ratio of sum of T throughout the system to the cumulative moles of T produced
    Ratio = [Ratio;nRatio];
    nPP_T2_coolantbank = zeros(Remaining, 1);  %[atm] partial pressure of T2 above melt
    PP_T2_coolantbank = [PP_T2_coolantbank;nPP_T2_coolantbank];
    nPP_TF_coolantbank = zeros(Remaining, 1);  %[atm] partial pressure of TF above melt
    PP_TF_coolantbank = [PP_TF_coolantbank;nPP_TF_coolantbank];
    nPP_T2_HX_primarybank = zeros(Remaining, 1);  %[atm] partial pressure of T2 over primary HX flibe-facing metal
    PP_T2_HX_primarybank = [PP_T2_HX_primarybank; nPP_T2_HX_primarybank];
    nPP_T2_HX_secondarybank = zeros(Remaining, 1);  %[atm] particle pressure of T2 over secondary HX flibe-facing metal
    PP_T2_HX_secondarybank = [PP_T2_HX_secondarybank;nPP_T2_HX_secondarybank];
%     nCheck_bank = zeros(Remaining, 1);
%     Check_bank = [Check_bank;nCheck_bank];
    %Permeator output
    if PermeationFlag_primary == 2
        nPermeator_total_p_store = zeros(Remaining, 1);
        Permeator_total_p_store = [Permeator_total_p_store;nPermeator_total_p_store];
        nC_T2_permeatorexit_p_store = zeros(Remaining, 1);
        C_T2_permeatorexit_p_store = [C_T2_permeatorexit_p_store;nC_T2_permeatorexit_p_store];
        nJ_permeator_p_store = zeros(Remaining, 1);
        J_permeator_p_store = [J_permeator_p_store;nJ_permeator_p_store];
    else
    end
    if PermeationFlag_secondary == 2
        nPermeator_total_s_store = zeros(Remaining, 1);
        Permeator_total_s_store = [Permeator_total_s_store;nPermeator_total_s_store];
        nC_T2_permeatorexit_s_store = zeros(Remaining, 1);
        C_T2_permeatorexit_s_store = [C_T2_permeatorexit_s_store;nC_T2_permeatorexit_s_store];
        nJ_permeator_s_store = zeros(Remaining, 1);
        J_permeator_s_store = [J_permeator_s_store;nJ_permeator_s_store];
    else
    end
    
    %Output from Gas Stripping of TF and T2 from the coolant
    nPrimaryStripGas_TF_Conc = zeros(Remaining, 1);   %Concentration of TF in the strip gas from the primary stripper [mol TF/mol gas]
    PrimaryStripGas_TF_Conc = [PrimaryStripGas_TF_Conc;nPrimaryStripGas_TF_Conc];
    nPrimaryStripGas_T2_Conc = zeros(Remaining, 1); %Concentration of T2 in the strip gas from the primary stripper [mol T2/mol gas]
    PrimaryStripGas_T2_Conc = [PrimaryStripGas_T2_Conc;nPrimaryStripGas_T2_Conc];
    nPrimaryStripTotal_T2 = zeros(Remaining, 1);  %Cumulative amount of T2 removed into the offgas [mol T2]
    PrimaryStripTotal_T2 = [PrimaryStripTotal_T2;nPrimaryStripTotal_T2];
    nPrimaryStripTotal_TF = zeros(Remaining, 1);  %Cumulative amount of TF removed into the offgas [mol TF]
    PrimaryStripTotal_TF = [PrimaryStripTotal_TF;nPrimaryStripTotal_TF];
    nPrimaryStripCoolantExit_TF = zeros(Remaining, 1);   %Concentration of TF in the coolant exiting the stripper [mol TF/mol flibe]
    PrimaryStripCoolantExit_TF = [PrimaryStripCoolantExit_TF;nPrimaryStripCoolantExit_TF];
    nPrimaryStripCoolantExit_T2 = zeros(Remaining, 1);  %Concentration of T2 in the coolant exiting the stripper [mol T2/mol flibe]
    PrimaryStripCoolantExit_T2 = [PrimaryStripCoolantExit_T2;nPrimaryStripCoolantExit_T2];
    nSecondaryStripGas_TF_Conc = zeros(Remaining, 1);   %Concentration of TF in the strip gas from the secondary stripper [mol TF/mol gas]
    SecondaryStripGas_TF_Conc = [SecondaryStripGas_TF_Conc;nSecondaryStripGas_TF_Conc];
    nSecondaryStripGas_T2_Conc = zeros(Remaining, 1); %Concentration of T2 in the strip gas from the secondary stripper [mol T2/mol gas]
    SecondaryStripGas_T2_Conc = [SecondaryStripGas_T2_Conc;nSecondaryStripGas_T2_Conc];
    nSecondaryStripTotal_T2 = zeros(Remaining, 1);  %Cumulative amount of T2 removed into the secondary offgas [mol T2]
    SecondaryStripTotal_T2 = [SecondaryStripTotal_T2;nSecondaryStripTotal_T2];
    nSecondaryStripTotal_TF = zeros(Remaining, 1);  %Cumulative amount of TF removed into the secondary offgas [mol TF].  Currently, this will always be zero.
    SecondaryStripTotal_TF = [SecondaryStripTotal_TF;nSecondaryStripTotal_TF];
    nSecondaryStripCoolantExit_TF = zeros(Remaining, 1);   %Concentration of TF in the coolant exiting the stripper [mol TF/mol flibe]
    SecondaryStripCoolantExit_TF = [SecondaryStripCoolantExit_TF;nSecondaryStripCoolantExit_TF];
    nSecondaryStripCoolantExit_T2 = zeros(Remaining, 1); %Concentration of T2 in the coolant exiting the stripper [mol T2/mol flibe]
    SecondaryStripCoolantExit_T2 = [SecondaryStripCoolantExit_T2;nSecondaryStripCoolantExit_T2];
    Days = Daysnew;
else
    d_in = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%MAIN LOOP OF THE PROGRAM. BEGIN TRITIUM TRANSPORT CALCULATIONS%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
temp_time_count = 0;
for d = d_in:1:Days*Hoursperday/Hour_Fraction  %Number of times output will be recorded from the calculations
    Calculation_Progress_DAYS = (d/Hoursperday)*Hour_Fraction  %Written out to the command window to indicate calculation progress
    for Time_Step_Small = 1:1:Steps   %Number of steps of time increment size DT [sec] between recording the output
                                      %Once "Time_Step_Small" = Steps, the output is saved and then the calculation continues
        %Tritium production rate calculation
        if Tritiumproductionflag == 1
            t = 0;  %[sec] Calculate BOL generation rate at t = 0 seconds
            [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);  %Use t = 0 to get tritium birth rate for BOL
            %Birth = 7.44486E-7; %[mole T/sec] from 900 MWt FHR at BOL estimate from AHTR
        elseif Tritiumproductionflag == 2
            t = 20*365*86400;  %[sec] Calculate equilibrium generation rate at 20 years
            [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);  %Use large t to get equilibrium tritium birth rate
            %Birth = 7.44486E-8;  %[mole T/sec] from 900 MWt FHR equilibrium estimate from AHTR
        elseif Tritiumproductionflag == 3
            t = Time_Step_Small*DT + (d-1)*3600*Hour_Fraction; %Total time [sec] of calculation up to the present time step
            [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);   %Calculate tritium production rate as it varies with time
        elseif Tritiumproductionflag == 4  %User specified tritium production rate. Currently configured for tritium desorption accident scenario
            %t = Time_Step_Small*DT + (d-1)*3600*Hour_Fraction; %Total time [sec] of calculation up to the present time step
            t = 300*86400;
            [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment);   %Calculate tritium production rate as it varies with time
            temp_time_count = temp_time_count + DT;
            if temp_time_count < 229
                Birth = Birth + 0.76/229;  %[mole T/sec]
            else
            end
        end
        MoleT = Birth*DT;  %Mole tritium atoms produced in one timestep
        CumulativeT = CumulativeT + MoleT;  %Cumulative amount [moles of T] of tritium produced throughout the simulation
        
        %Primary system is divided up into axial segments, and a temperature profile is applied.  Use function "polythermal11_5".
        for j=1:1:n_axial %Run through each axial SEGMENT of the loop
            v_dot = v_dot_o(j,1);  %Primary coolant volumetric flow rate [m3/s]
            k_HenryT2 = k_HenryT2_o(j,1);  %Select each temperature-dependent parameter associated with the particular position (j) in the loop from the pre-calculated arrays.  Then pass this to the function "polythermal11_5".
            k_HenryTF = k_HenryTF_o(j,1);  %
            k_masspebble_T2 = k_masspebble_T2_o(j,1);
            k_masspebble_TF = k_masspebble_TF_o(j,1);
            k_masspipe_T2 = k_masspipe_T2_o(j,1);
            k_masspipe_TF = k_masspipe_TF_o(j,1);
            k_Cr_pebble = k_Cr_pebble_o(j,1);
            k_Cr_masspipe = k_Cr_masspipe_o(j,1);
            Sievert_316_primary = Sievert_316_primary_o(j,1);
            Sievert_316_secondary = Sievert_316_secondary_o(j,1);
            Dp = Dp_o(j,1);
            Ds = Ds_o(j,1);
            T = Temp_Profile(j,1);  %Temperature [K] for the current axial segment
            %Function "polythermal.m" is the main workhorse of the program.  It also calls out to the corrosion module
            [TF_bed_limit, T2_bed_limit, CP, CS, CPC, c1, c2, mt1, mt2, mt3, j12, j23, mtg_TF, mtg_T2, mt1_TF, mt1_T2, mt1_total, tperg, Moles_Cr_ppt, GramsCr_persa, C_Cr_actual, ...
                grad, TF_consumed, T2_generated, Cr_profile, initial, Crperslice, Vol_surf, Cr_dissolved, Conc_slice, mtgbed_T2, mtgbed_TF, mtgbed2_T2, ...
                Totmatomsurf, surface, PP_TF, PP_T2, PP_T2_HX_primary, PP_T2_HX_secondary, mole_T2_poffgas, y_1_T2_p, y1_TFp, x_N_TF_p, mole_T2_soffgas, ...
                mt2_TF, y1_TFs, y_1_T2_s, x_N_TF_s, T2_PrimaryConc, T2_SecondaryConc, mole_TF_poffgas, mole_TF_soffgas, Redox_Potential, CumulativeT2_gbed2, ...
                Permeator_p_total, C_T2_permeatorexit_p, J_permeator_p, Permeator_s_total, C_T2_permeatorexit_s, J_permeator_s, CPermp, CPerms, CumulativeT2_gbed, ...
                CumulativeTF_gbed, Crcorrodedaxial, Cm1_bank, Cm2_bank, Check, Checker, Csb_next, mt1_T2_HXout, mt1_TF_HXout, mt1_TF_core, mt1_T2_core, ...
                C_Cr_actual_core, C_Cr_HXactual, CoreGraphiteCumulativeT, CoreGraphiteCumulativeT2, CoreGraphiteCumulativeTF, corrosion_time] = polythermal_v1_1(Ratio_TF_T2, ...
                k_HenryTF, k_HenryT2, Mole_flibe, Feedbackflag, Corrosionflag, PRF, Ds, c1, c2, j12, j23, Redoxflag, mt1, MoleT, T_uptake, ...
                tperg, DT, k_masspebble_T2, k_masspebble_TF, Vol_1, Surf_Frac, graphite_sa, Vol_2, mt2, mt3, Dp, Thick, ...
                Elements, DX, mt1_TF, mt1_TFnew, mt1_T2, mt1_T2new, mtg_TF, mtg_T2, j, CP, CS, CPC, Core_mesh, Hot_mesh, Cold_mesh, ...
                Hx_mesh, n_axial, mt1_total, T, T_avg, C_Cr_actual, CentralRef_sa, OuterRef_sa, Moles_Cr_ppt, GramsCr_persa, k_Cr_pebble, k_Cr_masspipe, ...
                TF_consumed, Cr_profile, Conc_slice, grad, initial, Crperslice, Vol_surf, T2_generated, Cr_dissolved, k_masspipe_T2, k_masspipe_TF, pipe_sa, pipe_d, ...
                pipe_l, pipe_d2, pipe_l2, Hx_tube_od, Slice_vol, slices1, slices2, slice_thick1, slice_thick2, Conc_bulk, Totmatomsurf, Lattice_param, ...
                surface, Length1_tot, Mole_frac_metal, PP_TF, PP_T2, Sievert_316_primary, Sievert_316_secondary, PP_T2_HX_primary, PP_T2_HX_secondary, ...
                GBflag, GasStrippingFlag, StrippingFlowFraction_p, StrippingFlowFraction_s, NStages_p, NStages_s, m_dot, mole_T2_poffgas, m_dot_secondary, ...
                Mole_flinak, mole_T2_soffgas, y1_TFp, y_1_T2_p, x_N_TF_p, T2_PrimaryConc, y1_TFs, y_1_T2_s, x_N_TF_s, T2_SecondaryConc, ...
                mt2_TF, d, G_s, G_p, mole_TF_poffgas, mole_TF_soffgas, Permeator_p_total, J_permeator_p, C_T2_permeatorexit_p, ...
                PermeationFlag_primary, Permeator_s_total, J_permeator_s, C_T2_permeatorexit_s, PermeationFlag_secondary, PPX, SPX, CPermp, HX1_element_vol, HX2_element_vol, ...
                HX1_element_sa, HX2_element_sa, PermElements, Permp_element_sa, Permp_element_vol, Perms_element_sa, Perms_element_vol, CPerms, ...
                Permeator_number_p, Permeator_number_s, Totalpebblegraphitemass, Tritiumcapturebedflag, mtgbed_T2, mtgbed_TF, Bed_mass, Bed_k_masspebble_T2_temp,...
                Bed_k_masspebble_TF_temp, Bed_surface_area, CumulativeT2_gbed, CumulativeTF_gbed, Bed_frac_rep, Crcorrodedaxial, v_dot, Cm1_bank, Cm2_bank, ...
                v_dot2, k_transp2, k_henryT22, Csb_next, mt1_T2_HXout, mt1_TF_HXout, mt1_TF_core, mt1_T2_core, Molar_dot, C_Cr_actual_core, k_masspipe_T2_perm1, ...
                k_masspipe_T2_perm2, C_Cr_HXactual, Loops, PRFLoops1, CoreGraphiteCumulativeT, CoreGraphiteCumulativeT2, CoreGraphiteCumulativeTF, CoreRefuelFrac, ...
                t, Surf_area_gb, corrosion_time, mtgbed2_T2, CumulativeT2_gbed2, Bed_k_masspebble_T2_temp_s, Bed_surface_area_s, Bed_mass_s, Bed_frac_rep_s, Tritiumcapturebedflag_s, TF_bed_limit, T2_bed_limit);
            Axial_Redox_Potential(j,1) = Redox_Potential;  %[kJ/mol F2] redox potential in coolant after exiting a specific part of the loop
        end
        %finished running through all the axial segments of the loop for the current time step.  Begin a new time step
    end
    
    CT1(1, d) = c1;      %[moles T2/m3] concentration of T2 in primary.  only used if Redoxflag = 1
    CT2(1, d) = c2;      %[mole/m3] concentration in secondary loop.  only used if Redoxflag = 1
    MT1(d, 1) = mt1;     %Moles of tritium in primary.  Not used if Redoxflag = 2
    MT2(d, 1) = mt2*2;   %Moles of T atoms in secondary.  Only T2 exists in the secondary system
    MT3(d, 1) = mt3*2;   %Moles of T atoms released to power cycle
    CoreGraphiteCumulativeT_bank(d,1) = CoreGraphiteCumulativeT;
    CoreGraphiteCumulativeT2_bank(d,1) = CoreGraphiteCumulativeT2;
    CoreGraphiteCumulativeTF_bank(d,1) = CoreGraphiteCumulativeTF;
    
    Tinventorycore_track(d, 1) = sum(2*mtg_T2 + mtg_TF); %number of moles of T on the core graphite
    T2onbed(d, 1) = 2*mtgbed_T2;    %number of moles of T from T2 on the graphite bed 
    TFonbed(d, 1) = mtgbed_TF;      %number of moles of T from TF on graphite bed 
    
    %track the bed solubility limit and adsortpion concentration (cm3 STP/g) on the graphite bed
    bed_TF_solubility(d,1) = TF_bed_limit/(4.464e-05);  %solubility limit of TF (cm3 STP/g) on the graphite bed
    bed_TF_concentration(d,1) = mtgbed_TF/Bed_mass/(4.464e-05); %concentration of TF (cm3 STP/g) on the graphite bed
    bed_T2_solubility(d,1) = T2_bed_limit/(4.464e-05); %solubility limit of T2 (cm3 STP/g) on the graphite bed
    bed_T2_concentration(d,1) = mtgbed_T2/Bed_mass/(4.464e-05); %concentration of T2 (cm3 STP/g) on the graphite bed
    
    %Graphite tritium absorber bed output
    TFonbedbank(d,1) = CumulativeTF_gbed;  %Store the moles of TF capture on the optional graphite bed (not the core)
    T2onbedbank(d,1) = CumulativeT2_gbed;  %Store the moles of T2 captured on the optional graphite bed (not the core)
    T2onbedbank2(d,1) = CumulativeT2_gbed2;  %Store the moles of T2 captured on the optional graphite bed in the secondary coolant system
    
    if d > 1
    ReleaseRate_Ci(d-1,1) = ((MT3(d,1)-MT3(d-1,1))/(((d-(d-1))*Hour_Fraction)/24))*29263.83;  %Tritium rate of release from power cycle after each hour.  Units are [Ci/d]
    else
    end
    J12(1, d) = j12;
    J23(1, d) = j23;
    if Redoxflag == 2  %Redox is turned ON
        SystemTotalT(1, d) = mt1_TF + mt1_T2*2 + mt2*2 + mt3*2 + CoreGraphiteCumulativeT;  %[moles]  total moles of T atoms produced
    elseif Redoxflag == 1  %Redox is turned OFF
        SystemTotalT(1, d) = (mt1 + mt2 + mt3 + mtg)*2;  %[moles]  total moles of T atoms produced
    end
    Axial_Redox_Potential_store(:,d) = Axial_Redox_Potential;  %[kJ/mol F2] store the axial redox potential after each fraction of an hour.  The final row is the global redox potential at the end of each time step
    Global_Redox(d,1) = Axial_Redox_Potential(n_axial,1);  %[kJ/mol F2] Global redox potential at the end of each time step
    Moles_Cr_ppt_tot(:,d) = Moles_Cr_ppt;  %Moles of Cr precipitated in each axial position after each hour.  Each column is an hour and each row is an axial position
    GRAMSCR_pptpersa(:,d) = GramsCr_persa*0.1;    %mg/cm2 of Cr precipitated in each axial position after each hour
    Crcorrodedaxial_bank(:,d) = Crcorrodedaxial;  %Moles of Cr corroded from each axial position after each time period.  Each column is a fraction of an hour and each row is an axial position
    Crcorroded_persa(:,d) = (Crcorrodedaxial./pipe_sa)*(Cr_MM*0.1);  %mg Cr corroded per cm2
    Cr_coolant_conc (d,1) = C_Cr_actual;   %[mol Cr/m3] concentration of Cr2+ dissolved in primary coolant
    
    Cr_PROFILE = Cr_profile;  %[moles Cr/m3] in piping metal in primary system.  Is overwritten at the end of each day.
    MT1_TF(d, 1) = mt1_TF;        %Moles TF in primary
    MT1_T2(d, 1) = mt1_T2;        %Moles T2 molecules in primary
    MT1_Total(d, 1) = mt1_TF + 2*mt1_T2;  %Moles T atoms in primary    
    CP_bank = CP;  %[mol T2/m3] Concentration of T2 in each radial slice of each axial segment in the primary HX, at the end of each hour.  Overwritten each time 
    CS_bank = CS;  %[mol T2/m3] Concentration of T2 in each radial slice of each axial segment in the secondary HX, at the end of each hour.  Overwritten each time
    HX1_T(d,1) = 2*sum(CP_bank*HX1_element_vol);  %[mol T dissolved in HX1 tube walls]
    HX2_T(d,1) = 2*(CS_bank*HX2_element_vol);  %[mol T dissolved in HX2 tube walls]
    TritiumBalance(d,1) = mt1_TF + 2*mt1_T2 + 2*mt2 + 2*mt3 + 2*sum(CP_bank*(HX1_element_vol/Hx_mesh)) + 2*(CS_bank*HX2_element_vol) ...
                          + CoreGraphiteCumulativeT + 2*mole_T2_soffgas + 2*mole_T2_poffgas + mole_TF_poffgas + mole_TF_soffgas ... 
                          + 2*Permeator_p_total + 2*Permeator_s_total + 2*CPermp*Permp_element_vol + 2*CPerms*Perms_element_vol ...
                          + CumulativeTF_gbed + 2*CumulativeT2_gbed + 2*CumulativeT2_gbed2;
    CumulativeT_bank(d,1) = CumulativeT;  %Total moles of T produced in the calculation.  The tritium balance must be equal to this.
    Ratio(d,1) = TritiumBalance(d,1)/CumulativeT_bank(d,1);  %Ratio of sum of T throughout the system to the cumulative moles of T produced
    PP_T2_coolantbank(d,1) = PP_T2;  %[atm] partial pressure of T2 above melt
    PP_TF_coolantbank(d,1) = PP_TF;  %[atm] partial pressure of TF above melt
    PP_T2_HX_primarybank(d,1) = PP_T2_HX_primary;  %[atm] partial pressure of T2 over primary HX flibe-facing metal
    PP_T2_HX_secondarybank(d,1) = PP_T2_HX_secondary;  %[atm] particle pressure of T2 over secondary HX flibe-facing metal
    
    
    %Permeator output
    if PermeationFlag_primary == 2
        Permeator_total_p_store(d,1) = Permeator_p_total; %Moles T2 removed from primary coolant via permeator 
        C_T2_permeatorexit_p_store(d,1) = C_T2_permeatorexit_p;
        J_permeator_p_store(d,1) = J_permeator_p;  
    else
    end
    if PermeationFlag_secondary == 2
        Permeator_total_s_store(d,1) = Permeator_s_total; %Moles T2 removed from secondary coolant via permeator
        C_T2_permeatorexit_s_store(d,1) = C_T2_permeatorexit_s;
        J_permeator_s_store(d,1) = J_permeator_s;
    else
    end
    
    
    %Output from Gas Stripping of TF and T2 from the coolant
    PrimaryStripGas_TF_Conc(d,1) = y1_TFp;   %Concentration of TF in the strip gas from the primary stripper [mol TF/mol gas]
    PrimaryStripGas_T2_Conc(d,1) = y_1_T2_p; %Concentration of T2 in the strip gas from the primary stripper [mol T2/mol gas]
    PrimaryStripTotal_T2(d,1) = mole_T2_poffgas;  %Cumulative amount of T2 removed into the offgas [mol T2]
    PrimaryStripTotal_TF(d,1) = mole_TF_poffgas;  %Cumulative amount of TF removed into the offgas [mol TF]
    PrimaryStripCoolantExit_TF(d,1) = x_N_TF_p;   %Concentration of TF in the coolant exiting the stripper [mol TF/mol flibe]
    PrimaryStripCoolantExit_T2(d,1) = T2_PrimaryConc;  %Concentration of T2 in the coolant exiting the stripper [mol T2/mol flibe]
    
    SecondaryStripGas_TF_Conc(d,1) = y1_TFs;   %Concentration of TF in the strip gas from the secondary stripper [mol TF/mol gas]
    SecondaryStripGas_T2_Conc(d,1) = y_1_T2_s; %Concentration of T2 in the strip gas from the secondary stripper [mol T2/mol gas]
    SecondaryStripTotal_T2(d,1) = mole_T2_soffgas;  %Cumulative amount of T2 removed into the secondary offgas [mol T2]
    SecondaryStripTotal_TF(d,1) = mole_TF_soffgas;  %Cumulative amount of TF removed into the secondary offgas [mol TF].  Currently, this will always be zero.
    SecondaryStripCoolantExit_TF(d,1) = x_N_TF_s;   %Concentration of TF in the coolant exiting the stripper [mol TF/mol salt]
    SecondaryStripCoolantExit_T2(d,1) = T2_SecondaryConc;  %Concentration of T2 in the coolant exiting the stripper [mol T2/mol salt]
    
    
    if rem(Calculation_Progress_DAYS,1)==0  %Save output after every full day.  This output file can then be used to restart the calculation if it was halted prematurely for any reason
        if exist('output_dir')
            save(strcat(output_dir,'/',Savefilename))
        else
            save(Savefilename)
        end 
    else
    end
    
end

%Visualize output from each day
x = (Hour_Fraction/24:Hour_Fraction/24:Days)';  %x values for plotting as days  [days]
ReleaseRate_Ci(d,1) = ReleaseRate_Ci(d-1,1);  

Cr_ppm = Cr_coolant_conc*(1E6)*(Cr_MM/(dens_flibe*1000));  %ppm Cr in coolant
PrimaryStripTotal_T = 2*PrimaryStripTotal_T2 + PrimaryStripTotal_TF;   %Total moles of T stripped out of the salt
PrimaryStripActivity = PrimaryStripTotal_T*29263.83;  %Tritium activity stripped out of the salt
SecondaryStripTotal_T = 2*SecondaryStripTotal_T2 + SecondaryStripTotal_TF;   %Total moles of T stripped out of the secondary salt
SecondaryStripActivity = SecondaryStripTotal_T*29263.83;  %Tritium activity stripped out of the secondary salt
PrimaryPermeatorTactivity = Permeator_total_p_store*29263.8;  %Tritium activity (Ci) removed from primary coolant via permeator 
SecondaryPermeatorTactivity = Permeator_total_s_store*29263.8;  %Tritium activity (Ci) removed from secondary coolant via permeator

OutputData(:,1) = x;  %Put days in first column
OutputData(:,2) = x*24;  %Hours
OutputData(:,3) = MT1_Total;  %Put moles of T in primary
OutputData(:,4) = MT1_Total*29263.83;  %Circulating T activity in Primary (Ci)
OutputData(:,5) = MT1_T2;  %Moles T2 in primary
OutputData(:,6) = MT1_TF;  %Moles TF in primary
OutputData(:,7) = MT2;  %moles of T in secondary in 
OutputData(:,8) = MT2*29263.83;  %Circulating activity in secondary (Ci) 
OutputData(:,9) = MT3;  %moles of T released to power cycle 
OutputData(:,10) = MT3*29263.83;  %Ci T released to power cycle 
OutputData(:,11) = ReleaseRate_Ci;  %Release rate due to T [Ci/d] 
OutputData(:,12) = CoreGraphiteCumulativeT_bank;  %Total moles of T atoms adsorbed on core graphite
OutputData(:,13) = HX1_T; %Moles T dissolved in HX1 tube walls
OutputData(:,14) = HX2_T; %Moles T dissolved in HX2 tube walls
OutputData(:,15) = PP_TF_coolantbank;  %Partial pressure [atm] of TF above primary coolant
OutputData(:,16) = PP_T2_coolantbank;  %Partial pressure [atm] of T2 above primary coolant
OutputData(:,17) = Global_Redox;    %Global redox in primary after each time step [kJ/mol F2]
OutputData(:,18) = CumulativeT_bank;    %Total moles of T atoms produced
OutputData(:,19) = TritiumBalance;  %Total T summed up throughout the system
OutputData(:,20) = Ratio;  %Ratio of tritium balance to cumulativeT_bank
OutputData(:,21) = Cr_coolant_conc;  %mol Cr/m3
    dens_flibe = 2415.6-0.49072*T_avg;  %Flibe density from Janz correlation in Sohal 2010 [kg/m^3].  Uses T [K] from loop_parameters input file (average coolant Temp).
OutputData(:,22) = Cr_coolant_conc*(1E6)*(Cr_MM)/(1000*dens_flibe);  %ppm Cr in salt vs time;
OutputData(:,23) = CoreGraphiteCumulativeT_bank*29263.8;  %Ci T absorbed on core graphite
OutputData(:,24) = bed_TF_solubility;       %solubility limit of TF (cm3 STP/g) on the graphite bed
OutputData(:,25) = bed_TF_concentration;    %concentration of TF (cm3 STP/g) on the graphite bed
OutputData(:,26) = bed_T2_solubility;       %solubility limit of T2 (cm3 STP/g) on the graphite bed
OutputData(:,27) = bed_T2_concentration;    %concentration of T2 (cm3 STP/g) on the graphite bed
OutputData(:,28) = Tinventorycore_track*29263.83;    %Instantaneous T activity (Ci) on the core graphite

BedCumulativeT = 2*T2onbedbank + TFonbedbank;  %Total Moles T absorbed on graphite bed external from the core
Tinventorycore = 2*mtg_T2 + mtg_TF;
Tinventorybed = 2*mtgbed_T2 + mtgbed_TF;
Tinventorybed2 = 2*mtgbed2_T2;  %Current inventory of moles T absorbed on secondary packed bed of graphite

%Calculate Cr profile in the metal throughout the loop
if Corrosionflag == 2
    for j = 1:1:n_axial
        T = Temp_Profile (j,1);
        D_Cr = (2.84E-5)*exp(-90.6/(R_univ*T/1000));  %[m2/s] Cr Grain Boundary Diffusion coefficient in 316L from Mizouchi, 2004 eqn 4.  Dividing this by 3.3 might get it near the Hast N thing?
        D_Cr_bulk = (3E-32)*exp(0.0283*T);  %[m2/s] Cr Bulk Diffusion coefficient in 316L from fit to data in Mizouchi 2004 Figure 2
        % D_Cr_bulk = 0.27*exp(-246/(0.008314*T))*(1/10000);  %[m2/s] Cr vol diffusion in 310 stainless, Williams, 1987
        % D_Cr_bulk = (1/10000)*(3.06*exp(-67536/(1.987*T)));  %[m2/s] bulk Cr diffusion in 304 from Daruvala, 1979
        % D_Cr_bulk = (1.745E-30)*exp((2.863E-2)*T); %Cr "overall diffusion coefficient" for Hastelloy N from Evans, 1961 ORNL-2982
        for n = 0:1:200
            slice_bulk = (n+1)*5E-7;  %Depth in pipe wall (m)
            depth_bulk(1, n+2) = slice_bulk; %m
            Cr_profile_bulk(j,n+2) = Conc_bulk(j,1)*(erf(slice_bulk/(2*sqrt(D_Cr_bulk*corrosion_time(j,1)))));
        end
        for n = 0:1:200
            slice_gb = (n+1)*3E-3;
            depth_gb(1,n+2) = slice_gb;  %m
            Cr_profile_gb(j,n+2) = Conc_bulk(j,1)*(erf(slice_gb/(2*sqrt(D_Cr*corrosion_time(j,1)))));   %Array of Cr profile in metal down a grain boundary now assuming that at the very surface, Cr concentration is 0
        end
    end
    %Corrosion Output
    FinalGramsDep_persa = GRAMSCR_pptpersa(:,Days*Hoursperday/Hour_Fraction);  %mg Cr/cm2 deposited through loop at end of simulation
    
    GRAMSCR_persa_new = GRAMSCR_pptpersa';  %mg Cr/cm2 deposited as a function of time
    Crcorroded_persa_new = (-1*Crcorroded_persa)';  %mg/Cr lost per cm2 as function of loop location and time
else
end

%Save output once more.  See input file for the name given to the file 
clear Restart
clear Restartfilename
% clear Savefilename
clear Daysnew
clear Days

if exist('output_dir')
    save(strcat(output_dir,'/',Savefilename))
else
    save(Savefilename)
end 

%close all;

%{
if Redoxflag == 1  %All tritium is assumed to be in the form of T2. Meaning, corrosion will not occur.  This is incorrect, but provides an upper bound for the maximum amount of T2 that might be expected to be released to the power cycle.
    figure; plot(x,MT1(:,1)); xlabel('Time (days)'); ylabel('Moles T2'); title('Moles of T2 in Primary with PHX of 316 L');
    figure; plot(x,MT2(:,1)); xlabel('Time (days)'); ylabel('Moles T2'); title('Moles of T2 in Secondary with SHX 316 SS');
    figure; plot(x,MT3(:,1)); xlabel('Time (days)'); ylabel('Moles T'); title('Moles of T Escaped to Power Cycle with SHX 316 SS');
    figure; plot(x,MT3(:,1)*29263.83); xlabel('Time (days)'); ylabel('Ci'); title('Activity of T Escaped to Power Cycle with SHX 316 SS');
    figure; plot(x,CoreGraphiteCumulativeT_bank(:,1)); xlabel('Time (days)'); ylabel('Moles T'); title('Moles of T adsorbed on core graphite');
    figure; plot(x,SystemTotalT(1,:)); xlabel('Time (days)'); ylabel('Moles T'); title('Total Moles of T atoms Produced');
    
elseif Redoxflag == 2  %Tritium is born as TF and may become T2 if corrosion occurs or if a redox potential is imposed.

%Tritium in coolant output
    figure; plot(x,MT1_Total(:,1)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Moles of T atoms in Primary');
    figure; plot(x,MT2(:,1)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Moles of T atoms in Secondary');
    figure; plot(x,MT3(:,1)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Moles of T atoms Escaped to Power Cycle');
    figure; plot(x,HX1_T(:,1)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Moles of T atoms dissolved in HX1 Tube Walls');
    figure; plot(x,HX2_T(:,1)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Moles of T atoms dissolved in HX2 Tube Walls');
    figure; plot(x,TritiumBalance(:,1)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Overall Tritium Balance');
    figure; plot(x,CumulativeT_bank(:,1)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Total Tritium Produced in the Calculation');
    figure; plot(x,Ratio(:,1)); xlabel('Time (days)'); ylabel('Ratio of System T to Cumulative T'); title('Ratio of Tritium Balance');
    %figure; plot(x,SystemTotalT2(1,:)); xlabel('Time (days)'); ylabel('Moles T atoms'); title('Total Moles of T atoms Produced');
    %figure; plot(x,MT1_TF(:,1)); xlabel('Time (days)'); ylabel('Moles T+ ions'); title('Moles of T+ in Primary');
    %figure; plot(x,MT1_T2(:,1)); xlabel('Time (days)'); ylabel('Moles T2'); title('Moles of T2 in Primary');
    figure; plot(x,PP_TF_coolantbank); xlabel('Time (days)'); ylabel('Partial Pressure of TF above flibe (atm)');
    figure; plot(x,PP_T2_coolantbank); xlabel('Time (days)'); ylabel('Partial Pressure of T2 above flibe (atm)');
    %figure; plot(x,PP_T2_HX_primarybank); xlabel('Time (days)'); ylabel('Partial Pressure of TF above primary HX metal (atm)');
    %figure; plot(x,PP_T2_HX_secondarybank); xlabel('Time (days)'); ylabel('Partial Pressure of T2 above secondary HX metal (atm)');
%Corrosion/deposition output
    figure; plot(GRAMSCR_pptpersa); xlabel('Axial Segment'); ylabel('mg Cr/cm^2'); title('mg Cr deposited per cm^2 in each axial segment of the reactor');
    figure; plot(x,Cr_coolant_conc); xlabel('Time (days)'); ylabel('Concentration (mol Cr/m3)'); title('Concentration of dissolved Cr in primary coolant');
    figure; plot(x,Crcorroded_persa(Core_mesh+Hot_mesh+1,:)); xlabel('Time (days)'); ylabel('Cr Lost (mg Cr/cm^2)'); title('Cr Lost from entrance to HX'); 
%Gas Stripping Output
    if GasStrippingFlag == 2 || GasStrippingFlag == 4
        figure; plot(x, PrimaryStripTotal_T2); xlabel('Time (days)'); ylabel('Total amount of T_2 extracted from the primary into stripping gas [mol T_2]');
        figure; plot(x, PrimaryStripCoolantExit_TF); xlabel('Time (days)'); ylabel('Concentration of TF in the primary coolant exiting the stripper [mol TF/mol flibe]');
        figure; plot(x, PrimaryStripCoolantExit_T2); xlabel('Time (days)'); ylabel('Concentration of T2 in the primary coolant exiting the stripper [mol TF/mol flibe]');
    else
    end
    if GasStrippingFlag == 3 || GasStrippingFlag == 4
        figure; plot(x, SecondaryStripCoolantExit_T2); xlabel('Time (days)'); ylabel('Concentration of TF in the primary coolant exiting the stripper [mol TF/mol flibe]');
        figure; plot(x, SecondaryStripTotal_T2); xlabel('Time (days)'); ylabel('Total amount of T_2 extracted from the secondary into stripping gas [mol T_2]');
        figure; plot(x, SecondaryStripCoolantExit_TF); xlabel('Time (days)'); ylabel('Concentration of TF in the secondary coolant exiting the stripper [mol TF/mol flibe]');
    else
    end
%Permeation Window Output
    if PermeationFlag_primary == 2
        figure; plot(x, 2*Permeator_total_p_store); xlabel('Time (days)'); ylabel('(mole T)'); title('Total Amount of T extracted from primary coolant via permeator')
        figure; plot(x, 2*29263.83*Permeator_total_p_store); xlabel('Time (days)'); ylabel('(Ci)'); title('Total Activity of T extracted from primary coolant via permeator')
        figure; plot(x, C_T2_permeatorexit_p_store); xlabel('Time (days)'); ylabel('(mole T_2/m^3 flibe)'); title('Concentration of T_2 in Primary Coolant Exiting Permeator'); 
    else
    end
    if PermeationFlag_secondary == 2
        figure; plot(x, Permeator_total_s_store); xlabel('Time (days)'); ylabel('(moles T_2)'); title('Total Amount of T_2 extracted from secondary coolant via permeator')
        figure; plot(x, C_T2_permeatorexit_s_store); xlabel('Time (days)'); ylabel('(mole T_2/mol Coolant)'); title('Concentration of T_2 in Secondary Coolant Exiting Permeator');
        figure; plot(x, J_permeator_s_store); xlabel('Time (days)'); ylabel('mole T_2/s'); title('Rate of T_2 permeation in secondary permeator');
    else
    end
%T Release Rate and Global redox

    figure; plot(x,ReleaseRate_Ci(:,1)); xlabel('Time (days)'); ylabel('Tritium Release Rate (Ci/d)'); title('Release Rate from Power Cycle');
    figure; plot(x, Global_Redox); xlabel('Time (days)'); ylabel('Coolant Redox Potential (kJ/mol F_2)'); title('Coolant redox potential exiting the HX after each time step');

end

%}
    
if Feedbackflag == 3 && Corrosionflag == 1
    disp('Warning with Feedbackflag = 3 and Corrosionflag = 1 all tritium will be TF.')
    disp('Changing Corrosionflag to 2 turns on corrosion and allows T2 to be generated from corrosion of Cr by TF')
    disp('Changing Feedbackflag to 2 fixes the redox potential and assures that T2 and TF exist in relative quatities consistent with the chosen redox potential.')
else
end

toc