    function [Birth] = TritiumProductionCalculation(Vol_1, Core_coolant_vol, T_in, T_out, flux, t, Li7_enrichment)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the tritium birth rate due to neutron transmutation in flibe
% as function of the average system temperature, the one-group flux,
% and the salt composition (initial Li-7 enrichment).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T = (T_in+T_out)/2;  %Coolant temp [K]

Flibe_MM = 32.8931;     %Molar mass of flibe [g/mol] from doing 0.67*MM_LiF + 0.33*MM_BeF2 
dens_flibe = 2415.6-0.49072*T;          %Flibe density from Janz correlation in Sohal 2010 [kg/m^3]
mole_dens_flibe = dens_flibe*(1000/Flibe_MM);  %Molar density of flibe [mol flibe/m3]

Li7_MM = 7.01600455;  %[g/mol]
Li6_MM = 6.015122795;  %[g/mol]

Li7_molefrac = (Li7_enrichment/Li7_MM)/((Li7_enrichment/Li7_MM)+((100-Li7_enrichment)/Li6_MM));  %Mole fraction of lithium in flibe that is Li-7
Li6_molefrac = ((100-Li7_enrichment)/Li6_MM)/((Li7_enrichment/Li7_MM)+((100-Li7_enrichment)/Li6_MM));  %Mole fraction of lithium in flibe that is Li-6
mole_dens_Li_flibe = 0.67*mole_dens_flibe;  %Molar density of total lithium in flibe [mol Li/m3]
N_Li7 = (Li7_molefrac*mole_dens_Li_flibe*6.022E23)*10^-6;  %Number density of Li-7 atoms in flibe [atoms Li-7/cm3]
N_Li6 = (Li6_molefrac*mole_dens_Li_flibe*6.022E23)*10^-6;  %Number density of Li-6 atoms in flibe [atoms Li-6/cm3]
N_Be9 = (0.33*mole_dens_flibe*6.022E23)*10^-6;  %Number density of Be-9 in flibe [atoms Be-9/cm3]

sig_T_Li7 = (1E-3)*10^(-24);  %(n,T) cross section in Li-7 [cm2]
sig_T_Li6 = 148.026E-24;  %(n,T) cross section in Li-6 [cm2]
sig_abs_Li6 = 148.032E-24;  %(n,abs) cross section in Li-6 [cm2]
sig_alpha_Be9 = (3.63E-3)*10^-24;  %(n,alpha) in Be-9 [cm2]

Birth = (flux*sig_T_Li7*N_Li7+flux*sig_T_Li6*(N_Li6*exp((-Core_coolant_vol/Vol_1)*flux*sig_abs_Li6*t)+((flux*sig_alpha_Be9*N_Be9) ...
    /(flux*sig_abs_Li6))*(1-exp((-Core_coolant_vol/Vol_1)*flux*sig_abs_Li6*t))))*(Core_coolant_vol*10^6)/(6.022E23);  %[mol T/s] Equation comes from Cisneros, 2013 Thesis

