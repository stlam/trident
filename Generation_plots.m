
t_i = 0;
t_f = 50*365*24*60*60;
t = linspace(t_i, t_f);
gen_rate = zeros(length(t));

vol_list = linspace(2, 7.2);
gen_list = zeros(length(vol_list),1)

for i = 1:length(vol_list)
    gen_list(i) = TritiumProductionCalculation(46.82, vol_list(i), 873.15, 973.15, 3.41E14, 0, 99.995)*3*9650*1000/236*60*60*24;
end
plot(vol_list, gen_list)