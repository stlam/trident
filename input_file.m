function [Density_metal, MM, Wtfrac, Lattice_param, T_out, T_in, Rx_power, qo, T_avg, Days, Elements, Corrosionflag, T_uptake, Redoxflag, Feedbackflag, Oxideflag, ...
          Kernel_d, Buffer_t, IPyC_t, SiC_t, OPyC_t, TRISOperPebble, Pebble_radius, Core_height, Core_mesh, CentralRef_radius, ...
          OuterRef_outradius, OuterRef_inradius, Fuelzone_innerradius, Fuelzone_outerradius, pipe_d, pipe_l, Hot_mesh, pipe_d2, pipe_l2, Cold_mesh, ...
          Hx_mesh, Hx_tube_od, Thick, A1, A2, C_Cr_initial_ppm, pipe_thick1, pipe_thick2, pipe_zone1, pipe_zone2, slices1, slices2, slice_thick1, ... 
          slice_thick2, depth_inwall, flux, Tritiumproductionflag, GBflag, GasStrippingFlag, NStages_p, NStages_s, G_s, G_p, Hour_Fraction, Birth_User, ... 
          StrippingFlowFraction_p, StrippingFlowFraction_s, PermeationFlag_primary, WindowArea_p, WindowThick_p, Vac_p, PermeationFlag_secondary, ...
          WindowArea_s, WindowThick_s, Vac_s, PermElements, Permp_tube_od, Perms_tube_od, Tritiumcapturebedflag, Bed_vessel_radius, Bed_vessel_length, ...
          Particle_radius, Particle_density, Bed_packingfraction, Bed_frac_rep, Restart, Restartfilename, Savefilename, Loops, PRFinput, PF, TubeNumber, ...
          Hx1tubes, Hx2tubes, CoreGeometryAdjust, N_CoreFuelPebbles, N_CoreGrapPebbles, CoreRefuelFrac, NumPermptubes_opt, NumberofPermeatorTubes, ...
          Ratio_TF_T2, Surf_area_gb, Li7_enrichment, Tritiumcapturebedflag_s, Bed_frac_rep_s, Bed_vessel_radius_s, Bed_surface_area_s, Particle_radius_s, ... 
          Particle_density_s, Bed_packingfraction_s] = input_file
            
Restart = 1;  %If restarting a previous calculation, turn ON Restart = 2
              %If Restart = 1, restart is NOT used.  A fresh caclulation is performed
if Restart == 1
    Restartfilename = 'nothing';  %Only provide a file name to restart a calculation if Restart = 2
elseif Restart == 2
    clear all
    Restart = 2;
    Restartfilename = fullfile(cd, 'restart_file.mat');  %Provide a file name and directory to restart from if Restart = 2 
end

Savefilename = fullfile(cd, 'output_file.mat');  %Provide a file name and directory for saving the current run
      
%Reactor Temperature, time, time step, calculation model, and output options:
T_in = 873.15;   %Core inlet temperature [K]
T_out = 973.15;  %Core outlet temperature [K]
T_avg = 923.15;      %Core average temp in Kelvin. Used for the initial calculation of flibe density
Rx_power = 236;      %Reactor power [MWt]
qo = 79.7221;        %Axial peak linear heat generation rate calculated from equation in Ch. 14 of Todreas and Kazimi if core height and power are known [MWt]

Ratio_TF_T2 = 9.2E-5;  %Nominal = 9.2E-5 ratio of P_TF/Sqrt(P_T2) calculated at 650 C for fluorine potential of -700.5 kJ/mol F2 
Li7_enrichment = 99.995;  %wt % Li-7 enrichment in flibe.  Baseline is 99.995
Loops = 1;  %Number of coolant loops to simulate (options are 1 or 2).  Currently, any secondary (intermediate loop uses the salt flinak)

Days = 1; %Total number of days of simulation

Hour_Fraction = 0.5;   %Record and store calculation results every fraction of an hour.
                       %For example, if you want to record ouput every hour, Hour_Fraction = 1. If you want to store output after every 30 minutes, Hour_fraction = 0.5
                       %This is useful for capturing behavior which occurs
                       %quickly.  Do not use less than 1 for long simulations.
      
%Calculation options
Elements = 6;     %Number of meshpoints for solving diffusion in HX.  When running 
                    %finite difference, the number of Elements needs to be related
                    %to DT via the Fourier Number calculated lower down in
                    %the code
                    
T_uptake = 2;      %Turn off/on tritium uptake on core graphite
                    %1 = off
                    %2 = on
CoreRefuelFrac = (1/30)/86400;  %Fraction of the core pebbles to "refuel" per second
                                %Set = 0 if not simulating core refueling
                     
CoreGeometryAdjust = 2;   %Option to correct the pebble graphite surface areas in the
                          %core to account for non-uniform geometries.  TRIDENT
                          %only models concentric cylinders in the core, but there
                          %may be chutes and other geometries in a real core
                          %1 = off
                          %2 = on
                          %TRIDENT only uses the number of pebbles below if CoreGeometryAdjust = 2
                          %Otherwise, TRIDENT calculates the number of pebbles in the core based on the core geometry,
                          %the pebble size, and the pebble packing fraction in the core
N_CoreFuelPebbles = 470000;  %Number of fuel pebbles in the core from Table 2-1 of Mk1 PB-FHR Report
N_CoreGrapPebbles = 218000;  %Number of graphite-only pebbles in the core from Table 2-1 of Mk1 PB-FHR Report
                    
Redoxflag = 2;     %Turn off/on redox control
                    %1 = off - everything is T2, there is no TF and no corrosion
                    %2 = on, the user may specify a fixed redox condition as a ratio via "Ratio_TF_T2" or  
                    
Feedbackflag = 2;  %Turn off/on redox feedback
                    %1 = off, calculates a fixed T2 and T+ generation rate based on an initially specified redox potential, then
                    %  applies this generation rate for the entire calculation.  When T2 diffusion occurs, this will change the overall redox
                    %  state in the coolant, but the T2 and T+ generation rate remains constant.  Corrosion reactions do not alter the the T2 and T+ generation rate 
                    %2 = on, Fixed redox: the generation rate of atoms T per second in the reactor remains constant, but the portion of this
                    %  which is generated at T+ and the portion generated as T2 is varied in order to maintain a fixed redox
                    %  potential in the coolant.  Any corrosion reactions are also taken into account if Corrosionflag == 2 below
                    %3 = pseudo feedback, all T is produced as T+, corrosion reactions produce T2, redox potential is allowed to
                    %  drift based on buildup of T+, consumption of T+ by corrosion, generation of T2 by corrosion and diffusion
                    %  of T2.  Redox state is calculated, but not controlled
                    
Oxideflag = 2;     %Turn off/on oxide layer permeation reduction on air side of HX
                    %1 = off
                    %2 = on
PRFinput = 10;     %Permeation reduction factor due to an oxide layer on the air-facing side of the heat exchanger.  Only used if Oxideflag = 2
  
                      
Corrosionflag = 1;  %Turn off/on corrosion of structural metals 
                      %1 = off, no corrosion
                      %2 = on, corrosion is considered 
GBflag = 2;         %Turn off/on corrosion surface area adjustment based on GB. Only meaningful if Corrosionflag = 2
                      %1 = off, use whole surface area
                      %2 = on, adjust active surface area for corrosion to
                      %only the surface area of the grain boundaries
if GBflag ==2
    GB_diameter = 31.8E-6;   %Grain boundary diameter for 316 from azom.com (spacing between grain boundaries) [m]
    % GB_diameter = 10E-6;  %304 L stainless fine grain size
%     GB_diameter = 23E-6;  %23E-6 304 L stainless from kestenbach, 1976 springer
    %     GB_diameter = 60E-6;   %Grain boundary diameter (spacing between grain boundaries) for Hastelloy X  88 micron from Lippold 2013 [m]
    %     GB_diameter = 24E-6;   %Grain boundary diameter (spacing between grain boundaries) for Hastelloy X. 24 micron from Abuzaid.  88 micron from Lippold 2013 [m]
    GB_width = 10E-9;      %Grain boundary width [m]  For 10 Ni atomic distances 2.48E-9 m
    
    %     GB_width = 0.01E-6;      %Grain boundary width [m]  For Hastelloy N, grain width is about 0.5 nm?
    %     Surf_area_gb = (4*(GB_diameter*GB_width)+GB_width^2)/(2*GB_diameter+GB_width)^2;  %Multiplier with units of m2 GB surface per m2 of metal surface.  Typically around 3E-4
    %
    Circ_radius = (2*GB_diameter+sqrt(3)*GB_width/2)/2;
    Surf_area_gb = ((3*GB_diameter*GB_width)+(sqrt(3)/4)*GB_width^2)/(pi*(Circ_radius)^2); %Multiplier with units of m2 GB surface per m2 of metal surface.
    
else
    Surf_area_gb = 1;  %Surface area not adjusted for grain boundaries
end
                      
%%% Metal material properties
% Initial elements in 316 L SS, atom densities, surface atoms, etc.
Density_metal = 8000;  %[kg/m^3] density of 316L SS from azom.com
%     Cr       Fe      Ni      C       Mn      P       S       Si      Mo     N      Molar Mass [g/mol]     
MM = [51.9961; 55.845; 58.693; 12.011; 54.938; 30.974; 32.065; 28.086; 95.94; 14.007];  
%  Note that the Fe wt fraction is set to 0.00 initially.  Iron is assumed to make up the balance of SS, and is calculated separately after all the other elements are specified
%         Cr    Fe    Ni    C       Mn    P        S       Si      Mo     N     Weight fraction from aksteel.com for 316L SS
Wtfrac = [0.18; 0.00; 0.12; 0.0003; 0.02; 0.00045; 0.0003; 0.0075; 0.025; 0.001];
Wtfrac(2,1) = 1-sum(Wtfrac); %Iron is the balance of the 316 composition

Lattice_param = 0.359E-9;  %[m] lattice parameter for austenite in 316 L stainless steel


C_Cr_initial_ppm = 25; %Initial Cr concentration [ppm] in the salt after processing 
pipe_zone1 = 0.000254; %[m] Thickness over which corrosion calcs are done for TF transport limited mode
pipe_zone2 = 0.000254; %[m] Thickness over which corrosion calcs are done for Cr diffusion limited mode
slices1 = 10;  %number of slices at which to calc Cr concentration distribution
slices2 = 10;  %number of slices at which to calc Cr concentration for Cr diff limited mode
slice_thick1 = pipe_zone1/slices1;  %[m] thickness of each slice of the pipe through-wall thickness for TF-limited case
slice_thick2 = pipe_zone2/slices2;  %[m] thickness of each slice of the pipe through-wall thickness for Cr diffusion-limited case
depth_inwall = (0:pipe_zone1/slices1:pipe_zone1);  %[m] depth of each slice in the pipe wall 

Tritiumproductionflag = 3;  %Turn off/on variable tritium production rate with time
                            %Tritium production rate in primary loop.  Moles of T atoms per second
                            %1 = use BOL tritium generation rate, time variation is OFF, BOL generation rate is used for entire calculation
                            %2 = use EOL tritium generation rate, time variation is OFF, equilibrium production rate is used
                            %3 = variation is ON, tritium production varies
                            %as Li-6 is consumed and Be-9 is transmuted to Li-6 based on a model from Cisneros, 2013
                            %4 = user specifies the tritium production rate in units of [molt T/sec]
                            if Tritiumproductionflag == 4
                                Birth_User = (7.44486E-7);  %[Mole T/sec] for 900 MWt PB-FHR based on AHTR estimates (7.44486E-7) used in ICAPP 2014 Paper
                            else
                                Birth_User = 0;  %Birth_User not used.  Use Tritiumproduction flag 1, 2, or 3 to specify birth rate.
                            end
                          
flux = 3.41E14;  %flux in coolant for tritium calculation (n/cm2-s).  From Cisneros, 2013

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Primary Graphite bed for capture of tritium %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tritiumcapturebedflag = 1;  %1 = tritium capture on separate graphite bed is turned OFF
%2 = tritium capture on separate graphite bed is turned ON
%Model options for graphite bed
Bed_frac_rep = 1/30;      %Fraction of bed to be replaced aka replacement rate [fraction of bed graphite/d]. If this is 0, then the bed is not replaced online
Bed_frac_rep = Bed_frac_rep*(1/86400);  %Fraction of bed being replaced per second
%Input variables for graphite absorption bed
Particle_radius = 0.015;  %[m] radius of particle/pebble assuming a spherical particle
Bed_packingfraction = 0.60;  %Packing fraction in the graphite bed. Fraction of bed volume occupied by pebbles
Bed_vessel_radius = 1.2; %[m]
Particle_density = 1.77E6;  %From Toyo Tanso density for IG-110U graphite [g/m^3];
Bed_vessel_length = 3.6;  %[m] Length of the bed vessel

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Secondary Graphite bed for capture of tritium %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tritiumcapturebedflag_s = 1;  %1 = tritium capture on separate graphite bed is turned OFF
%2 = tritium capture on separate graphite bed is turned ON
%Model options for graphite bed
Bed_frac_rep_s = 1/30;      %Fraction of bed to be replaced aka replacement rate [fraction of bed graphite/d]. If this is 0, then the bed is not replaced online
Bed_frac_rep_s = Bed_frac_rep_s*(1/86400);  %Fraction of bed being replaced per second
%Input variables for graphite absorption bed
Bed_vessel_radius_s = 1.2; %[m]
Bed_surface_area_s = 1945.3;  %[m2] total bed surface area
Particle_radius_s = 0.021;  %[m] radius of particle/pebble assuming a spherical particle
Particle_density_s = 0.45E6;  %From CalgonCarbon OVC 4x8 carbon [g/m^3];
Bed_packingfraction_s = 0.60;  %Packing fraction in the graphite bed. Fraction of bed volume occupied by pebbles

%%% Gas Stripping Options
GasStrippingFlag = 1;  % Option for including counter current gas strippers at various points in the system
                       % If stripping is used, strippers are located just before the heat exchangers 
                       % 1 = no gas strippers
                       % 2 = gas stripping in primary coolant system only
                       % 3 = gas stripping in secondary coolant system only
                       % 4 = gas stripping in both primary and secondary system
StrippingFlowFraction_p = 0.5;  %Put value from 0 to 1.  Specifies how much of the primary flow rate is diverted into the gas stripper
                             %1 means that the full primary coolant flow is going through the stripper 
                             %Anything < 1 means that only a fraction of the coolant flow is being diverted to the stripper
StrippingFlowFraction_s = 0.5; %Put value from 0 to 1.  Specifies how much of the secondary coolant flow is diverted into the gas stripper 
NStages_p = 10;  %Number of stages in primary gas stripper
NStages_s = 10;  %Number of stages in secondary gas stripper
Gas_hrflowrate_p = 20000;  %Stripping gas flow rate [L/hr] at STP in primary
G_p = (Gas_hrflowrate_p*0.987/(0.08206*273.15))*(1/3600); %Use IGL to conver stripping gas flow rate in primary [mole/s]
Gas_hrflowrate_s = 20000;  %Stripping gas flow rate [L/hr] at STP in secondary
G_s = (Gas_hrflowrate_s*0.987/(0.08206*273.15))*(1/3600); %Use IGL to conver stripping gas flow rate in secondary [mole/s];     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Permeation Window Options %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%TRIDENT assumes that the flow area in the permeators is equal to the flow
%area of the pipe flowing into the permeator
NumPermptubes_opt = 2;  %Option to specify the number of tubes in the permeator or to have TRIDENT calculate the number of tubes based on other input
                        %1 = off
                        %2 = on, user specifies number of tubes
NumberofPermeatorTubes = 2*13680; %Number of tubes to put in the permeator
PermElements = 6;  %Number of radial finite difference elements in the primary and/or secondary permeator(s)
%Primary System
%%%%%%%%********NOTE: need to tell TRIDENT main program what the diffusion
%%%%%%%%coefficient is for the permeator so that it can calculate an
%%%%%%%%appropriate DT size (around line 680 in main program)
PermeationFlag_primary = 1;     %1 = off
                                %2 = on;  Permeation window is turned on in the primary system.
WindowArea_p = 12000;   %Surface area of primary system permeation window (m^2) 
WindowThick_p = 8.89E-4;  %(Hx thickness 8.89E-4 m)Thickness of primary system permeation window tube walls (m)
Permp_tube_od = 0.00635;  %Primary permeator tube outer diameter [m]
Vac_p = 1E-6; %NOT USED -- Pressure on outside of permeation window (side opposite the coolant flow) [Pa]
%Secondary System
PermeationFlag_secondary = 1;   %1 = off
                                %2 = on - permeation window is turned on in the secondary system
WindowArea_s = 10082;   %Surface area of secondary system permeation window (m^2)
WindowThick_s = 8.89E-4;  %Thickness of secondary system permeation window (m)
Perms_tube_od = 0.00635; %Secondary permeator tube outer diameter [m]
Vac_s = 1E-6;  %NOT USED -- Pressure on outside of permeation window [Pa]

%Fuel pebble and TRISO particle properties  %From PB-FHR Mk1 Report 
Kernel_d = 400E-6;      %Fuel kernel diameter [m]
Buffer_t = 100E-6;      %Buffer thickness [m]
IPyC_t = 35E-6;         %IPyC thickness [m]
SiC_t = 35E-6;          %SiC layer thickness [m]
OPyC_t = 35E-6;         %OPyC thickness [m]
TRISOperPebble = 4730;    %Number of TRISO particles per pebble
Pebble_radius = 1.5/100;  %Pebble radius [m]  3cm diameter pebble
PF = 0.60;           %Pebble packing fraction

%%% Reactor Core geometry Mk1 Report p. 31 and 38
Core_height = 4.65;           %236 MWt core effective height based on Figure 2-17 in p.61 of PB-FHR Mk1 Report [m]
Core_mesh = 10;               %Number of axial core divisions for polythermal loop calculations
CentralRef_radius = 0.35;      %Central reflector radius [m]
OuterRef_outradius = 1.69;     %Outer reflector outer radius [m] Based on Table 1-5
OuterRef_inradius = 1.25;     %Outer refelctor inner radius [m] 
Fuelzone_innerradius = 0.35;  %Fuel zone inner radius [m]
Fuelzone_outerradius = 1.05;  %Fuel zone outer radius [m]   

%Reactor Hot Leg Pipe Parameters from p. 89 of Mk1 PB-FHR Report where the
%four manifold pipes are combined into a single pipe having equivalent
%inner cross sectional area
pipe_thick1 = 0.02;  %Pipe wall thickness [m]
pipe_d = 0.79196; %Pipe inner diameter [m]
pipe_l = 29.74;  %Pipe length [m]
Hot_mesh = 10;  %number of axial divisions for polythermal loop calculations

%Reactor Cold Leg pipe parameters
pipe_thick2 = 0.02;  %Pipe wall thickness [m]
pipe_d2 = 0.494975;  %Pipe inner diameter [m]
pipe_l2 = 35.443;   %Pipe length [m]
Cold_mesh = 10;  %number of axial divisions for polythermal loop calculations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Heat exchanger properties, coolant volumes, from MK1-PB-FHR paper %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TubeNumber = 1;  %TubeNumber = 1: input number of tubes in the HX1 and HX2 if known
                 %TubeNumber = 0: number of HX tubes is calculated in
                 %TRIDENT assuming the total flow cross sectional area in
                 %the HX matches that in the hot leg pipe
Hx1tubes = 2*13680;  %Number of HX1 tubes (from Table 3-2 in Mk1 Report)
Hx2tubes = 2*13680;  %Number of Hx2 tubes

Hx_mesh = 10;  %number of axial mesh points for the primary HX for polythermal loop calculations
Hx_tube_od = 0.00635; %[m] Heat exchanger tube outer diameter from MK1-PB-FHR paper
Thick = 8.89E-4; %Heat exchanger tube wall thickness from Mk1-PB-FHR [m] 
%1.6E-3;  %Heat exchanger tube wall thickness [m] from ICAPP 2014 report 
A1 = 2*5041;  %Primary heat exchanger surface area from MK1-PB-FHR paper [m^2]
A2 = 2*5041;  %Secondary heat exchanger surface area from MK1-PB-FHR paper[m^2] 

end